<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ShopWindow extends CI_Controller
{
    public $user;
    public $auth_key;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('business_model');
        $this->load->model('users_model');
        $this->load->model('response_model');
        $this->load->model('coupons_model');
        $this->load->model('category_model');
        $this->load->model("auth_model");
        $this->load->model("db_shopwindow");
        $this->auth_key = $this->input->post('auth_key');
        $this->user = $this->auth_model->authenticateUser($this->auth_key);
        if (!empty($this->user)) {
            $this->lang->load('message', $this->user['language']);
        } else {
            $this->lang->load('message', 'en');
        }
    }

    public function createShopWindow()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'title',
            'title',
            'required',
            array(
                'required' => $this->lang->line('req_title'),
            )
        );
        $this->form_validation->set_rules(
            'start_date',
            'start date',
            'required',
            array(
                'required' => $this->lang->line('req_start_date'),
            )
        );
        $this->form_validation->set_rules(
            'expire_date',
            'expire date',
            'required',
            array(
                'required' => $this->lang->line('req_expire_date'),
            )
        );
        $this->form_validation->set_rules(
            'price',
            'price',
            'required',
            array(
                'required' => $this->lang->line('req_price'),
                'numeric' => $this->lang->line('req_numeric'),
            )
        );
        $this->form_validation->set_rules(
            'description',
            'description',
            'required',
            array(
                'required' => $this->lang->line('req_description'),
            )
        );
         $this->form_validation->set_rules(
            'business_id',
            'business id',
            'required',
            array(
                'required' => $this->lang->line('req_business_id'),
            )
        );
        if (!isset($_FILES['image'])) {
            echo json_encode(array('status' => '500', 'message' => $this->lang->line('req_img'), 'data' => (object) []));
        } else {
            if ($this->form_validation->run() == false) {

                $error = $this->form_validation->error_array();
                $message = reset($error);
                echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
                exit();
            } else {
                    // $active_id = $this->business_model->getBusinessById($this->user['id']);
                    // if (empty($active_id) || $active_id->is_activated == 0) {
                    //     return $this->response_model->apiresponse('500', $this->lang->line('business_deactive'));
                    // }
                    $active_id = $this->db_shopwindow->getBusinessById($this->input->post('business_id'));
                    if (empty($active_id) || $active_id->is_activated == 0) {
                        return $this->response_model->apiresponse('500', $this->lang->line('business_deactive'));
                    }
                    $response = $this->users_model->saveImage("image", './uploads/shopwindow/');
                    if ($response['error'] == true) {
                        echo json_encode(['status' => '500', 'message' => $this->lang->line('req_img'), 'data' => (object) []]);
                        exit;
                    }
                    $image = $response['image'];
                    date_default_timezone_set("UTC");
                    $data = array(
                        'title' => $this->input->post('title'),
                        'description' => $this->input->post('description'),
                        'price' => $this->input->post('price'),
                        'start_date' => date('Y-m-d H:i:s', $this->input->post('start_date')),
                        'expiration_date' => date('Y-m-d H:i:s', $this->input->post('expire_date')),
                        'business_profile_id' => $this->input->post('business_id'),
                        'image' => $image,
                        "shouldOrder" => (isset($_REQUEST['should_order'])?$_REQUEST['should_order']:0),
                        "city" => $this->user['city']
                    );
                    $date = date('Y-m-d H:i:s');
                    if ($data['expiration_date'] <= $date) {
                        return $this->response_model->apiresponse('500', $this->lang->line('expire_date_valid'));
                    }
                    $res=$this->db_shopwindow->createShopWindow($data);
                    echo json_encode(['status' => '200', 'message' => $this->lang->line('create_window'), 'data' => ['window_details' => $res]]);
                    exit();
            }
        }
    }
    public function getShopWindows(){
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {
             $User = $this->users_model->getUserByUserId($this->user['id']);
             
             $data =  $this->db_shopwindow->getAllShopWindow($User['city']);
              foreach($data as &$val){
                 $val['business_profile'] = $this->db_shopwindow->getBusinessProfileById($val['business_profile_id']); 
             }
             echo json_encode(['status' => '200', 'message' => $this->lang->line('create_window'), 'data' => ['window_details' => $data]]);
            }
    }
    public function getShopWindowDetails(){
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'window_id',
            'Shop Window',
            'required',
            array(
                'required' => $this->lang->line('req_shop_window_id'),
            )
        );
        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {
            $data =  $this->db_shopwindow->getShopWindowById($this->input->post('window_id'));
            // $business = $this->db_shopwindow->getBusinessDetails($data);
            $business = $this->db_shopwindow->getBusinessProfileById($data['business_profile_id']);
            if($business){
                if(intval($business->city) != 0){
                    $business->city = $this->category_model->getCitiesById($business->city);
                }  
            }
            
            echo json_encode(['status' => '200', 'message' => $this->lang->line('create_window'), 'data' => ['window_details' => $data,"business_details" => $business]]);
        }


    }
    
    
    
    public function deleteShopWindow()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'window_id',
            'window id',
            'required',
            array(
                'required' => $this->lang->line('req_window_id'),
            )
        );

       
        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'user_id' => $this->user['id'],
                    'window_id' => $this->input->post('window_id'),
                );
                $window = $this->db_shopwindow->getWindowById($data['window_id']);
                if (empty($window->id)) {
                    return $this->response_model->apiresponse('500', $this->lang->line('not_found_window'));
                }
               
                $deletewindow = $this->db_shopwindow->deleteWindowById($data,$window);
                if ($deletewindow == 1) {
                    return $this->response_model->apiresponse('200', $this->lang->line('success_delete_window'));
                } else {
                    return $this->response_model->apiresponse('500', $this->lang->line('not_delete_window'));
                }
            }
        }
    }
 public function filterShopWindow()
        {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'business_name' => $this->input->post('business_name'),
                    'user_id' => $this->user['id'],
                    'city_id' => $this->input->post('city_id'),
                );
                $window = $this->db_shopwindow->getWindowByFilter($data);
                foreach($window as &$val){
                 $val->business_profile = $this->db_shopwindow->getBusinessProfileById($val->business_profile_id); 
                }
                if (empty($window)) {
                    echo json_encode(['status' => '200', 'message' => $this->lang->line('not_found_window'), "data"=>['window_details' => $window]]);
                } else {
                    echo json_encode(['status' => '200', 'message' => '', 'data' => ['window_details' => $window]]);
                }
            }
        }
    }
}