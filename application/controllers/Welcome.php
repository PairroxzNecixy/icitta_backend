<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function getIssuesId()
	{
		$this->db->select('users.id,oauth.user_id');
		$this->db->from('oauth');
		$this->db->join("users", "oauth.user_id=users.id", "LEFT");
		$users = $this->db->get()->result_array();
		$ids  = "";
		foreach($users as $user){
		    $ids.=$user["user_id"].",";
		    
		}
		echo $ids;
	}
	 
	public function index()
	{
		$this->load->view('welcome_message');
	}
}
