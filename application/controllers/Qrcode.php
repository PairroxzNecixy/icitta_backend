<?php
class Qrcode extends CI_Controller
{
    public $user;
    public $auth_key;
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('Qrcode_model');
        $this->load->model('response_model');
        $this->load->model('coupons_model');
        $this->load->model("auth_model");
        $this->auth_key = $this->input->post('auth_key');
        $this->user = $this->auth_model->authenticateUser($this->auth_key);
        if (!empty($this->user)) 
        {
            $this->lang->load('message', $this->user['language']);
        } else 
        {
            $this->lang->load('message', 'en');
        }
    }

    public function scanCode()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        $this->form_validation->set_rules(
            'QR_code',
            'QR_code',
            'required',
            array(
                'required' => $this->lang->line('req_QR_code')
            )
        );

        if ($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            return $this->response_model->apiresponse('500', $message);
            
        } else {
            if (empty($this->user)) 
            {
                return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
            } else {
                $data = array(
                    'user_id' => $this->user['id'],
                    'QR_code'   =>  $this->input->post('QR_code')
                );
                $countscan = $this->Qrcode_model->getCouponsByQrcode($data['QR_code']);
               
                if(!empty($countscan))
                {
                    $scan = $this->Qrcode_model->updateCouponForScan($countscan->scan_count, $data);
                }
                $coupons = $this->Qrcode_model->getCouponByuser($data);
                
                if(!empty($coupons))
                {
                    
                    if($data['QR_code'] == $coupons->QR_code)
                    {
                        $verified = $this->Qrcode_model->updateCouponByQRcode($coupons, $data);
                    }else{
                        return $this->response_model->apiresponse('500', $this->lang->line('not_QRcode_auth'));
                    }
                    
                    $data = array('scan_count'=>$scan->scan_count, 'verified_count'=>$verified->verified_count);
                    return $this->response_model->apiresponse('200', $this->lang->line('QRcode_auth'), $data );
                }else{
                    return $this->response_model->apiresponse('500', $this->lang->line('not_QRcode_auth'));
                }

            }
        }
    }
}