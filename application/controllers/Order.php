<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Order extends CI_Controller
{
    public $user;
    public $auth_key;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('business_model');
        $this->load->model('users_model');
        $this->load->model('response_model');
        $this->load->model('coupons_model');
        $this->load->model("auth_model");
        $this->load->model("db_shopwindow");
        $this->load->model("category_model");
        $this->load->model("db_orders");
        $this->load->model("notification_model");
        $this->auth_key = $this->input->post('auth_key');
        $this->user = $this->auth_model->authenticateUser($this->auth_key);
        if (!empty($this->user)) {
            $this->lang->load('message', $this->user['language']);
        } else {
            $this->lang->load('message', 'en');
        }
    }

    public function createOrder()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'name',
            'Name',
            'required',
            array(
                'required' => $this->lang->line('req_name'),
            )
        );
        $this->form_validation->set_rules(
            'address',
            'Address',
            'required',
            array(
                'required' => $this->lang->line('req_address'),
            )
        );
        $this->form_validation->set_rules(
            'city',
            'City',
            'required',
            array(
                'required' => $this->lang->line('req_city'),
            )
        );
        $this->form_validation->set_rules(
            'phone_number',
            'Phone Number',
            'required',
            array(
                'required' => $this->lang->line('req_phone_number')
            )
        );
        $this->form_validation->set_rules(
            'description',
            'Description',
            'required',
            array(
                'required' => $this->lang->line('req_description'),
            )
        );
        // $this->form_validation->set_rules(
        //     'expected_delivery',
        //     'Expected Delivery',
        //     'required',
        //     array(
        //         'required' => $this->lang->line('req_expected_delivery'),
        //     )
        // );
        $this->form_validation->set_rules(
            'business_id',
            'Business Id',
            'required',
            array(
                'required' => $this->lang->line('req_business_id'),
            )
        );
       
       
            if ($this->form_validation->run() == false) {

                $error = $this->form_validation->error_array();
                $message = reset($error);
                echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
                exit();
            } else {
                   $active_id = $this->business_model->getBusinessById($this->input->post('business_id'));
                    if (empty($active_id) || $active_id->is_activated == 0) {
                        return $this->response_model->apiresponse('500', $this->lang->line('business_deactive'));
                    }
                    date_default_timezone_set("UTC");
                    $date = date('Y-m-d H:i:s');
                    $data = array(
                        'name' => $this->input->post('name'),
                        'description' => $this->input->post('description'),
                        'address' => $this->input->post('address'),
                        'city' => $this->input->post('city'),
                        // 'expected_delivery' => date('Y-m-d H:i:s', $this->input->post('expected_delivery')),
                        'business_id' => $this->input->post('business_id'),
                        'phone_number' => $this->input->post('phone_number'),
                        "created_date" => $date,
                        "state" => 'requested',
                        "user_id" => $this->user['id']
                    );
                    
                    
                    if(isset($_REQUEST['coupon_id'])){
                      $data['coupon_id'] = $this->input->post('coupon_id');
                    }else{
                        $data['coupon_id'] = 0;
                    }
                    if(isset($_REQUEST['show_window_id'])){
                      $data['show_window_id'] = $this->input->post('show_window_id');
                    }else{
                        $data['show_window_id'] = 0;
                    }
                   
                    $date2 = strtotime($date);
                    $date2 = strtotime("+7 day", $date2);
                    $data['expected_delivery'] = date('Y-m-d H:i:s', $date2);
                    if ($data['expected_delivery'] <= $date) {
                        return $this->response_model->apiresponse('500', $this->lang->line('expected_delivery_date_valid'));
                    }
                    $res=$this->db_orders->createOrders($data);
                    $orderId = $this->db->insert_id();
                    if($res){
                        $userid = $this->db->select("user_id")->from("business_profiles")->where(["user_id"=> $data['business_id']])->get()->row();
                        $playerId   = $this->notification_model->getPlayerIdByUsersId($userid->user_id);
                        $player_ids = array_column($playerId, 'player_id');
                        // print_r($player_ids);
                        // die;
                        $content = array(
                            "en"=>"New Order received",
                            "it"=>"Nuovo ordine ricevuto"
                        );
                        $data = array(
                            "orderId"=>$orderId
                        );
                        $player_ids = array_values((array)array_diff($player_ids, array('')));
                        $this->notification_model->sendNotification($player_ids,$content,$data);
                    }
                    
                    
                    echo json_encode(['status' => '200', 'message' => $this->lang->line('create_order'), 'data' => ['order_details' => $res]]);
                    exit();
            }
        
    }
    public function getOrders(){
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        // $this->form_validation->set_rules(
        //     'for_business',
        //     'For Business',
        //     'required',
        //     array(
        //         'required' => $this->lang->line('req_for_business'),
        //     )
        // );
        $this->form_validation->set_rules(
            'user_id',
            'user id',
            'required',
            array(
                'required' => $this->lang->line('req_for_business'),
            )
        );
        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {
            $for_business = $this->input->post('for_business')?$this->input->post('for_business'):0;
             $data =  $this->db_orders->getAllOrders($this->user['id'],$for_business);
            //  $data['cuponDetails'] = [];   
            //   $data['windowDetails'] = [];
            if($data){
                    foreach ($data as &$dataVal) {
                     
                            if($dataVal['coupon_id'] != '' && $dataVal['coupon_id'] != 0){
                                $dataVal['cuponDetails'] = $this->coupons_model->getCouponByCouponId($dataVal['coupon_id']);
                            }else{
                                $dataVal['cuponDetails'] = [];   
                            }
                            if($dataVal['show_window_id'] != '' && $dataVal['show_window_id'] != 0){
                                $dataVal['windowDetails'] = $this->db_shopwindow->getShopWindowById($dataVal['show_window_id']);
                            }else{
                                $dataVal['windowDetails'] = [];
                            }
                        }
                    
            }
             
             echo json_encode(['status' => '200', 'message' => $this->lang->line('get_all_order'), 'data' => ['Orders' => $data]]);
            }
    }
    public function getOrderDetails(){
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'order_id',
            'Order Id',
            'required',
            array(
                'required' => $this->lang->line('req_order_id'),
            )
        );
        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {
            $data =  $this->db_orders->getOrderById($this->input->post('order_id'));
            
            if($data){
                if($data['coupon_id'] != '' && $data['coupon_id'] != 0){
                    $data['cuponDetails'] = $this->coupons_model->getCouponByCouponId($data['coupon_id']);
                }else{
                    $data['cuponDetails'] = [];   
                }
                if($data['show_window_id'] != '' && $data['show_window_id'] != 0){
                    $data['windowDetails'] = $this->db_shopwindow->getShopWindowById($data['show_window_id']);
                }else{
                    $data['windowDetails'] = [];
                }
            }
            
            
            $business = $this->db_orders->getBusinessDetails($data);
            if(intval($business->city) != 0){
                $business->city = $this->category_model->getCitiesById($business->city);
            }
            
             echo json_encode(['status' => '200', 'message' => $this->lang->line('get_order_details'), 'data' => ['order_details' => $data,"business_details" => $business]]);
        }
    }
    public function updateOrderDetails(){
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'order_id',
            'Order Id',
            'required',
            array(
                'required' => $this->lang->line('req_order_id'),
            )
        );
        $this->form_validation->set_rules(
            'new_order_state',
            'New Order State',
            'required',
            array(
                'required' => $this->lang->line('req_new_order_state'),
            )
        );
        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {
            $data =  $this->db_orders->updateOrderById($this->input->post('order_id'),$this->input->post('new_order_state'));
            if($data){
                $userid = $this->db->select("user_id")->from("orders")->where(["id"=> $this->input->post('order_id')])->get()->row();
                $playerId   = $this->notification_model->getPlayerIdByUsersId($userid->user_id);
                $player_ids = array_column($playerId, 'player_id');
               
                $content = array(
                    "en"=>"Your Order is:".$this->input->post('new_order_state'),
                    "it"=>"Il tuo ordine è:" . $this->input->post('new_order_state')
                );
                $data = array(
                    "orderId"=>$this->input->post('order_id')
                );
                $player_ids = array_values((array)array_diff($player_ids, array('')));
                $this->notification_model->sendNotification($player_ids,$content,$data);
            }
             echo json_encode(['status' => '200', 'message' =>  $this->lang->line('get_update_success'), 'data' => ['order_details' => $data]]);
        }
    }
    
    
     public function deleteOrder()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'order_id',
            'order id',
            'required',
            array(
                'required' => $this->lang->line('req_order_id'),
            )
        );
        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {
            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'user_id' => $this->user['id'],
                    'order_id' => $this->input->post('order_id'),
                );
                $order = $this->db_orders->getOrderById2($data['order_id']);
                // print_r($order);
                // print_r($data);
                // die;
                if (empty($order->id)) {
                    return $this->response_model->apiresponse('500', $this->lang->line('not_found_order'));
                }
                $deleteorder = $this->db_orders->deleteOrderById($data);
                if ($deleteorder == 1) {
                    return $this->response_model->apiresponse('200', $this->lang->line('success_delete_order'));
                } else {
                    return $this->response_model->apiresponse('500', $this->lang->line('not_delete_order'));
                }
            }
        }
    }
}