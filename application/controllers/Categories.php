<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories extends CI_Controller
{
    public $user;
    public $auth_key;
    public $language;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('response_model');
        $this->load->model('category_model');
        $this->load->model("auth_model");
        $this->auth_key = $this->input->post('auth_key');
        $this->user = $this->auth_model->authenticateUser($this->auth_key);
        $header = $this->input->request_headers();
        if(empty($header))
        {
            echo json_encode(['status'=>'500', 'message'=>'please select header value', 'data'=>(object)[]]);
            exit;
        }
        if(!isset($header['Lang'])|| empty($header['Lang']))
        {
            $this->language = 'en';
        }else{

            if ($header['Lang'] == 'en') 
            {
                $this->language = $header['Lang'];
            } elseif ($header['Lang'] == 'it') 
            {
                $this->language = $header['Lang'];
            } else {
                $this->language = 'en';
            }
        }
        
        $this->lang->load('message', $this->language);
    }
    
    public function getCategories()
    {
        if($this->language == 'it'){
            $categories = $this->category_model->getcategoriesByItalian();
        }else{
            $categories = $this->category_model->getcategoriesByEnglish();
        }
        if (!empty($categories)) {
            echo json_encode(['status' => '200', 'message' => '', 'data' => ['categories' => $categories]]);
            exit();
        } else {
            echo json_encode(['status' => '500', 'message' => $this->lang->line('not_categories'), 'data' => (object)[]]);
            exit();
        }
    }

    public function getcities()
    {
        $cities = $this->category_model->getCities();
        if (!empty($cities)) {
            echo json_encode(['status' => '200', 'message' => '', 'data' => ['cities' => $cities]]);
            exit();
        } else {
            echo json_encode(['status' => '500', 'message' => $this->lang->line('no_city'), 'data' => (object) []]);
            exit();
        }
    }

    public function updateplayerid()
    {
        log_message("error","NOTI PLAYER ID".$this->input->post('player_id'));
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        $this->form_validation->set_rules(
            'player_id',
            'player_id',
            'required',
            array(
                'required' => $this->lang->line('req_player_id')
            )
        );
        if($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            return $this->response_model->apiresponse('500', $message);
        }
        if(empty($this->user)) {
            return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
        }
        $player_id = $this->input->post('player_id');
        $res = $this->category_model->updatePlayeridByUserId($this->auth_key, $player_id);
        
       // return $this->response_model->apiresponse('200',);
        echo json_encode(['status' => '200', 'message' => $this->lang->line('success_updation'), 'data' => ['player_id' => $player_id]]);
    }
}
