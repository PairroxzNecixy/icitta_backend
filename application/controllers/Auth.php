<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public $user;
    public $auth_key;
    public function __construct()
    {

        parent::__construct();
        $this->load->model("response_model");
        $this->load->model("users_model");
        $this->load->model("auth_model");
        $this->auth_key = $this->input->post('auth_key');
        $this->user = $this->auth_model->authenticateUser($this->auth_key);
        if (!empty($this->user)) {
            $this->lang->load('message', $this->user['language']);
        } else {
            $this->lang->load('message', 'en');
        }
    }

    public function logout()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );

        if ($this->form_validation->run() == FALSE) {

            $error = $this->form_validation->error_array();
            $message = reset($error);

            return $this->response_model->apiresponse('200', $message);
        } else {

            if (empty($this->user)) {
                return $this->response_model->apiresponse('200', $this->lang->line('not_auth'));
            } else {

                if ($this->auth_model->logout($this->auth_key)) {
                    return $this->response_model->apiresponse('200', $this->lang->line('logout'));
                }
            }
        }
    }

    public function changePassword()
    {

        $this->form_validation->set_rules(
            'old_password',
            'old password',
            'required',
            array(
                'required' => $this->lang->line('req_pass')
            )
        );
        $this->form_validation->set_rules(
            'new_password',
            'new password',
            'required',
            array(
                'required' => $this->lang->line('req_new_pass')
            )
        );
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );

        if ($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            return $this->response_model->apiresponse('200', $message);

        } else {

            if (!empty($this->user)) {
                $data = array(

                    'user_id' => $this->user['id'],
                    'old_password' => $this->input->post('old_password'),
                    'new_password' => $this->input->post('new_password')

                );

                if (password_verify($data['old_password'], $this->user['password'])) {
                    $updatepass = $this->auth_model->changePass($data);
                    if ($updatepass) {

                        return $this->response_model->apiresponse('200', $this->lang->line('success_update'));
                    } else {

                        return $this->response_model->apiresponse('500', $this->lang->line('error_update'));
                    }
                } else {
                    return $this->response_model->apiresponse('500', $this->lang->line('not_match_pass'));
                }
            } else {
                return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
            }
        }
    }

    public function getUserProfile()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );

        if (empty($this->user)) {
            return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
            } else {
            $user = $this->users_model->getuserdetails($this->user['id']);
            if ($user['user_type_id'] == 1) {
                $data_profile = $this->users_model->getBusinessProfiles($this->user['id']);
            } elseif ($user['user_type_id'] == 2) {
                $data_profile  = $this->users_model->getUsersProfiles($this->user['id']);
            } else {
                $data_profile = $this->lang->line('not_exist_data');
            }

            $data = $data_profile;
            $message = '';
            return $this->response_model->apiresponse('200', $message, $data);
        }
    }

    public function updateuser()
    {
        $this->form_validation->set_rules(
            'name',
            'name',
            'required',
            array(
                'required' => $this->lang->line('req_name')
            )
        );
        $this->form_validation->set_rules(
            'mobile',
            'mobile',
            'required|is_natural',
            array(
                'required'      =>  $this->lang->line('req_mobile'),
                'is_natural'    =>  $this->lang->line('req_numeric') 
            )
        );
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        if ($this->form_validation->run() == FALSE) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            return $this->response_model->apiresponse('500', $message);
            
        } else {

            $data = array(
                'name'      =>  $this->input->post('name')
            );
            $userdata = array(
                'mobile'    =>  $this->input->post('mobile')
            );

            if (empty($this->user)) {

                return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
            } else {

                $user = $this->users_model->getuserdetails($this->user['id']);
                if ($user['user_type_id'] == 1) {
                    $userupdate = $this->auth_model->updateBusinessById($this->user, $data, $userdata);
                } elseif ($user['user_type_id'] == 2) {
                    $userupdate = $this->auth_model->updateUserById($this->user, $data, $userdata);
                } else {
                    $userupdate = $this->lang->line('not_exist_data');
                }
                
                if ($userupdate) {
                    return $this->response_model->apiresponse('200', $this->lang->line('success_updation'),$userupdate);
                } else {
                    return $this->response_model->apiresponse('500', $this->lang->line('unsuccess_updation'));
                }
            }
        }
    }

    public function updateUserImage()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        if ($this->form_validation->run() == FALSE) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            return $this->response_model->apiresponse('500', $message);
            
        } else {

            if (empty($this->user)) {
                return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
                } else {

                $config['upload_path']   = './uploads/userProfile';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|JPG|JPEG|PNG';
                $config['encrypt_name']  =  TRUE;
                $config['file_ext_tolower'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    return $this->response_model->apiresponse('500', $this->lang->line('req_img'));
                    
                } else {

                    $upload = $this->upload->data();
                    $image = $upload['file_name'];

                    $data = array(
                        'user_id' => $this->user['id'],
                        'image' => $image
                    );
                    $user = $this->users_model->getuserdetails($this->user['id']);
                    if ($user['user_type_id'] == 1) {
                        $imageupdate = $this->auth_model->updateBusinessImage($data);
                    } elseif ($user['user_type_id'] == 2) {
                        $imageupdate = $this->auth_model->updateuserImage($data);
                    } else {
                        $imageupdate = $this->lang->line('not_exist_data');
                    }
                    if ($imageupdate) {
                        return $this->response_model->apiresponse('200', $this->lang->line('success_updation'));
                    } else {
                        return $this->response_model->apiresponse('500', $this->lang->line('unsuccess_updation'));
                    }
                }
            }
        }
    }

    public function forgotpassword()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        $this->form_validation->set_rules(
            'email',
            'email',
            'required',
            array(
                'required' => $this->lang->line('req_email')
            )
        );

        if ($this->form_validation->run() == FALSE) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            
            return $this->response_model->apiresponse('500', $message);
        } else {

            if (empty($this->user)) {

                return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
            } else {
                $data = array(

                    'user_id'   =>  $this->user['id'],
                    'email'     =>  $this->input->post('email')

                );
                $user = $this->auth_model->emailVerification($data);
                if (empty($user)) {
                    return $this->response_model->apiresponse('500', $this->lang->line('not_vailid_email'));
                } else {

                    $maildata = array(
                        'code'  =>  $this->users_model->genauthkey(),
                        'link'  =>  'localhost/icitta/auth/forgotPasswordPage',
                        'email' =>  $data['email']
                    );
                    $requestdata = array(
                        'user_id'   =>  $data['user_id'],
                        'code'      =>  $maildata['code'],
                        'link'      =>  $maildata['link']
                    );
                    $this->auth_model->saveForgotPasswordRequest($requestdata);

                    $sendmail = $this->auth_model->sendmail($maildata);
                    if ($sendmail == 0) {
                        return $this->response_model->apiresponse('500', $this->lang->line('not_send_email'));
                    }
                }
            }
        }
    }

    public function updateAddress()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        $this->form_validation->set_rules(
            'lat',
            'lat',
            'required',
            array(
                'required' => $this->lang->line('req_lat')
            )
        );
        $this->form_validation->set_rules(
            'lng',
            'lng',
            'required',
            array(
                'required' => $this->lang->line('req_lng')
            )
        );

        if ($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            
            return $this->response_model->apiresponse('500', $message);
        } else {
            if (empty($this->user)) {

                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'lat'       =>  $this->input->post('lat'),
                    'lng'       =>  $this->input->post('lng'),
                    'city'      =>  $this->input->post('city'),
                    'address'   =>  $this->input->post('address'),
                    'user_type' =>  $this->user['user_type_id'],
                    'user_id'   =>  $this->user['id']
                );
                $updateaddress = $this->auth_model->updateAddress($data);
                if (!empty($updateaddress)) {
                    return $this->response_model->apiresponse('200', $this->lang->line('success_updation'), $updateaddress);
                } else {
                    return $this->response_model->apiresponse('500', $this->lang->line('unsuccess_updation'));                    
                    
                }
            }
        }
    }
    public function updatelanguage()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        $this->form_validation->set_rules(
            'Lang',
            'Lang',
            'required',
            array(
                'required' => $this->lang->line('req_language')
            )
        );

        if ($this->form_validation->run() == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object)[]));
            exit();

        } else {
            if (empty($this->user)) 
            {
                
                echo json_encode(['status' => '401', 'message' => $this->lang->line('not_auth'), 'data' => (object)[]]);
            
            } else {
                $lang =  $this->input->post('Lang');
                if ($lang == 'en') 
                {
                    $lang = 'en';
                } elseif ($lang == 'it') 
                {
                    $lang = 'it';
                } else {
                    $lang = 'en';
                }
                
                $data = array(
                    'language'  => $lang,
                    'user_id'   =>  $this->user['id']
                );
                $this->auth_model->updatelaguage($data);
                // if ($updatelang == TRUE) 
                // {
                    echo json_encode(['status' => '200', 'message' => $this->lang->line('success_updation'), 'data' => (object) []]);
                // } else {
                //     echo json_encode(['status' => '500', 'message' => $this->lang->line('same_lang_update'), 'data' => (object) []]);
                // }
            }
        }
    }
}
