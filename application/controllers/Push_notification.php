<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Push_notification extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("notification_model");
        $this->load->model("response_model");
        $this->load->model("coupons_model");
        $this->load->model("auth_model");
        $this->auth_key = $this->input->post('auth_key');
        $this->user = $this->auth_model->authenticateUser($this->auth_key);
        if (!empty($this->user)) {
            $this->lang->load('message', $this->user['language']);
        } else {
            $this->lang->load('message', 'en');
        }
    }

    /*$response = sendMessage();
        $return["allresponses"] = $response;
        $return = json_encode( $return);
        
        print("\n\nJSON received:\n");
        print($return);
        print("\n");*/

    public function setnotification()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        $this->form_validation->set_rules(
            'business_id',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_business_id')
            )
        );
        if ($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            return $this->response_model->apiresponse('500', $message);
        }
        if (empty($this->user)) {
            return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
        }
        //$coupon = $this->coupons_model->getCouponIdByCouponId($coupon_id);
        $data = array(
            'user_id'       =>  $this->user['id'],
            'business_id'   =>  $this->input->post('business_id')
        );
        $rows = $this->coupons_model->getDataByBusinessIdAndUserId($data);
        
        if(!empty($rows)) {
            if($rows->is_notification_enabled == 0)
            {
                $is_enabled = 1;
            }else{
                $is_enabled = 0;
            }
            $notificationData   = $this->notification_model->updateEnabledNotification($data,$is_enabled);
            $rows               = $this->coupons_model->getDataByBusinessIdAndUserId($data);
            $enabled            = $rows->is_notification_enabled;
            if($notificationData->is_notification_enabled == 1)
            {
                echo json_encode(['status'=>'200', 'message'=>$this->lang->line('notification_active'),'data'=>['is_notification_enabled'=>$enabled]]);
            }else{
                echo json_encode(['status'=>'200', 'message'=>$this->lang->line('notification_deactive'),'data'=>['is_notification_enabled'=>$enabled]]);
            }
        }else{
            
            $this->coupons_model->insertbusinessIdAndUserId($data);
            $rows               = $this->coupons_model->getDataByBusinessIdAndUserId($data);
            $enabled            = $rows->is_notification_enabled;
            echo json_encode(['status'=>'200', 'message'=>$this->lang->line('notification_active'),'data'=>['is_notification_enabled'=>$enabled]]);
            //return $this->response_model->apiresponse('200', $this->lang->line('notification_active'));
        }
    }

    public function getnotificationsdetails()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        if ($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            return $this->response_model->apiresponse('500', $message);
        }
        if(empty($this->user)) {
            return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
        }
        // if($this->user['user_type_id'] == 2)
        // {
        //     $userdata  = $this->notification_model->getUserIdByBusinessId($this->user['id']);
        // }
        // if($this->user['user_type_id'] == 1)
        // {
            $userdata   = $this->notification_model->getBusinessIdByBusinessId($this->user['id']);
        // }
        
       //$businessData   = $this->notification_model->getBusinessIdByBusinessId($this->user['id']);
       
        if(!empty($userdata))
        {
            foreach($userdata as &$row)
            { 
                if($row->name == NULL)
                {
                    $row->name = '';
                }
            }
            
            echo json_encode(['status'=>'200', 'message'=>$this->lang->line('data_available'),'data'=>['notification_list'=>$userdata]]);   
        }else{
            $notification = array();
            echo json_encode(['status'=>'200', 'message'=>$this->lang->line('data_not'), 'data' => ['notification_list'=>$notification]]);
            //return $this->response_model->apiresponse('500', $this->lang->line('data_not'));
        }
    }
}
