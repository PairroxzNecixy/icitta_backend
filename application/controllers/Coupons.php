<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coupons extends CI_Controller
{
    public $user;
    public $auth_key;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('business_model');
        $this->load->model('users_model');
        $this->load->model('response_model');
        $this->load->model('coupons_model');
        $this->load->model('category_model');
        $this->load->model("auth_model");
        $this->load->library('phpqrcode/qrlib');
        $this->auth_key = $this->input->post('auth_key');
        $this->user = $this->auth_model->authenticateUser($this->auth_key);
        if (!empty($this->user)) {
            $this->lang->load('message', $this->user['language']);
        } else {
            $this->lang->load('message', 'en');
        }
    }

    public function createCoupons()
    {
        // display_errors();
        // log_message('error', json_encode($_POST));
        // error_log(json_encode($_POST));
        error_log(json_encode($_REQUEST));

        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'name',
            'name',
            'required',
            array(
                'required' => $this->lang->line('req_name'),
            )
        );
        // $this->form_validation->set_rules(
        //     'value',
        //     'value',
        //     'required',
        //     array(

        //         'required' => $this->lang->line('req_value'),

        //     )
        // );

        $this->form_validation->set_rules(
            'apply_date',
            'apply date',
            'required',
            array(
                'required' => $this->lang->line('req_apply_date'),
            )
        );
        $this->form_validation->set_rules(
            'expire_date',
            'expire date',
            'required',
            array(
                'required' => $this->lang->line('req_expire_date'),
            )
        );
        $this->form_validation->set_rules(
            'coupon_type',
            'coupon type',
            'required|numeric',
            array(
                'required' => $this->lang->line('req_coupon_type'),
                'numeric' => $this->lang->line('req_numeric'),
            )
        );
        $this->form_validation->set_rules(
            'description',
            'description',
            'required',
            array(
                'required' => $this->lang->line('req_description'),
            )
        );
        if (!isset($_FILES['image'])) {
            echo json_encode(array('status' => '500', 'message' => $this->lang->line('req_img'), 'data' => (object) []));
        } else {

            if ($this->form_validation->run() == false) {

                $error = $this->form_validation->error_array();
                $message = reset($error);

                echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));

                exit();
            } else {

                if (empty($this->user)) {

                    echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
                } else {
                    $active_id = $this->business_model->getBusinessById($this->user['id']);

                    if (empty($active_id) || $active_id->is_activated == 0) {
                        return $this->response_model->apiresponse('500', $this->lang->line('business_deactive'));
                    }
                    // $user_id = $this->user['id'];

                    // if (!is_dir('uploads/coupons/'.$user_id))
                    // {
                    //     mkdir('./uploads/coupons/'.$user_id, 0755, TRUE);
                    // }
                    //     $url = './uploads/coupons/'.$user_id;

                    $response = $this->users_model->saveImage("image", './uploads/coupons/');

                    if ($response['error'] == true) {
                        echo json_encode(['status' => '500', 'message' => $this->lang->line('req_img'), 'data' => (object) []]);
                        exit;
                    }

                    $image = $response['image'];

                    if ($this->input->post('category') == null) {
                        $category = $this->coupons_model->getcategoryIdByBusiness($this->user['id']);
                        $cat = $category->category_id;
                    } else {
                        $cat = $this->input->post('category');
                    }
                    date_default_timezone_set("UTC");
                    $data = array(

                        'name' => $this->input->post('name'),
                        'value' => $this->input->post('value'),
                        'category_id' => $cat,
                        'apply_date' => date('Y-m-d H:i:s', $this->input->post('apply_date')),
                        'expire_date' => date('Y-m-d H:i:s', $this->input->post('expire_date')),
                        'release_date' => ($this->input->post('release_date')=="")?(date('Y-m-d H:i:s', 0)):date('Y-m-d H:i:s', $this->input->post('release_date')),
                        'coupon_type_id' => $this->input->post('coupon_type'),
                        'user_id' => $this->user['id'],
                        'image' => $image,
                        'description' => $this->input->post('description'),
                        'city' => $this->user['city'],
                        "shoudOrder" => (isset($_REQUEST['shoudOrder'])?$_REQUEST['shoudOrder']:0)

                    );
                    
                    if ($data["coupon_type_id"] == 1) {
                        $data['PriceFrom'] = isset($_REQUEST['price_from']) ? $this->input->post('price_from') : 0;
                        $data['PriceTo'] = isset($_REQUEST['price_to']) ? $this->input->post('price_to') : 0;
                        $data['discount'] = isset($_REQUEST['value']) ? $this->input->post('value') : 0;
                    } else {
                        $data['FixPrice'] = isset($_REQUEST['fix_price']) ? $this->input->post('fix_price') : 0;
                        if($data["coupon_type_id"] == 2){
                            $data['package'] = isset($_REQUEST['package']) ? $this->input->post('package') : 0;
                        }else{
                            $data['tribute'] = isset($_REQUEST['tribute']) ? $this->input->post('tribute') : 0;
                        }
                    }
           
           
                    //  print_r($data['expire_date']);
             
                    $date = date('Y-m-d H:i:s');
                    // print_r($date);
                    if ($data['expire_date'] <= $date) {
                        return $this->response_model->apiresponse('500', $this->lang->line('expire_date_valid'));
                    }
                    else if ($data['expire_date'] <= $data['release_date']) {
                        return $this->response_model->apiresponse('500', $this->lang->line('release_date_valid'));
                    }

                    $countcategories = $this->coupons_model->getcategories();
                    $countCouponType = $this->coupons_model->getcoupontype();

                    if ($data['category_id'] == 0) {
                        echo json_encode(['status' => '500', 'message' => $this->lang->line('category_not_exist'), 'data' => (object) []]);
                    } else if ($data['coupon_type_id'] == 0) {
                        echo json_encode(['status' => '500', 'message' => $this->lang->line('couponType_not_exist'), 'data' => (object) []]);
                    } else {

                        $validuser = $this->coupons_model->validForCreateCouponUser($this->user['id']);
                        if ($validuser == true) {
                            $coupon = $this->coupons_model->createCoupons($data);

                            $QRcode = $coupon->id . '_' . rand(2, 200000) . '_' . $this->user['id'];
                            $image_path = FCPATH . 'uploads/QR_images/';

                            $QR_image = 'image_' . $coupon->id . '.png';

                            $QR_data = array(
                                'id' => $coupon->id,
                                'QR_code' => $QRcode,
                                'QR_image' => $QR_image,
                            );

                            if (!empty($coupon)) {
                                /*$notificationData = $this->coupons_model->getUsersForSendNotification($this->user['id']);
                                foreach ($notificationData as $row)
                                {
                                echo $row->user_id.',';
                                }*/

                                QRcode::png($QRcode, $image_path . $QR_image, QR_ECLEVEL_L, 40);
                                $this->coupons_model->updateCoupons($QR_data);
                                echo json_encode(['status' => '200', 'message' => $this->lang->line('create_coupon'), 'data' => ['coupon_details' => $coupon]]);
                                exit();
                            } else {

                                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_create_coupon'), 'data' => (object) []]);
                            }
                        } else {
                            echo json_encode(['status' => '500', 'message' => $this->lang->line('limit_over'), 'data' => (object) []]);
                        }

                    }
                }
            }
        }
    }

    public function getcoupons()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'user_id' => $this->user['id'],
                );
                $coupons = $this->coupons_model->getcoupondefault($data);

                if (!empty($coupons)) {
                    echo json_encode(['status' => '200', 'message' => '', 'data' => ['coupon_details' => $coupons]]);
                    exit();
                } else {
                    echo json_encode(['status' => '500', 'message' => $this->lang->line('not_coupons'), (object) []]);
                    exit();
                }
                echo json_encode(['status' => '200', 'message' => '', 'data' => ['coupon_details' => $coupons]]);
                exit();
            }
        }
    }
    public function getCouponDetails()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'coupon_id',
            'coupon id',
            'required',
            array(
                'required' => $this->lang->line('req_coupon_id'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $coupon_id = $this->input->post('coupon_id');
                $data = array(
                    'coupon_id' => $coupon_id,
                    'user_id' => $this->user['id'],
                );
                $business_id = $this->coupons_model->getCouponIdByCouponId($coupon_id);
                $noti_data = array(
                    'user_id' => $this->user['id'],
                    'business_id' => $business_id->user_id,
                );
                $notification_enabled = $this->coupons_model->getenabledStatus($noti_data);

                $coupons = $this->coupons_model->getCouponDetails($data);
                $business = $this->coupons_model->getBusinessDetails($data);
                if(intval($business->city) != 0){
                    $business->city = $this->category_model->getCitiesById($business->city);
                }
                if (!empty($coupons) && !empty($business)) {
                    if (!empty($notification_enabled)) {
                        $business->is_notification_enabled = $notification_enabled->is_notification_enabled;
                    } else {
                        $business->is_notification_enabled = 0;
                    }
                    echo json_encode([
                        'status' => '200', 'message' => '',
                        'data' => ['coupon_details' => $coupons, 'business_details' => $business],
                    ]);
                } else {
                    echo json_encode(['status' => '500', 'message' => $this->lang->line('data_not'), 'data' => (object) []]);
                }
            }
        }
    }

    public function favoritecoupon()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'coupon_id',
            'coupon id',
            'required',
            array(
                'required' => $this->lang->line('req_coupon_id'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {

                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {

                $data = array(
                    'user_id' => $this->user['id'],
                    'coupon_id' => $this->input->post('coupon_id'),
                );
                $coupon = $this->coupons_model->getCouponIdByCouponId($data['coupon_id']);

                if (empty($coupon->id)) {
                    echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_found_coupon'), 'data' => (object) []));
                    exit;
                } else {
                    /*$notificationdata = array(
                    'user_id'       =>  $this->user['id'],
                    'business_id'   =>  $coupon->user_id
                    );*/
                    $couponfav = $this->coupons_model->makeFavoriteCoupon($data);
                    $couponsave = $this->coupons_model->saveCouponstatus($data);

                    if (empty($couponsave)) {
                        $statussave = 0;
                    } else {
                        $statussave = 1;
                    }

                    if ($couponfav == 1) {
                        /*$rows = $this->coupons_model->getDataByBusinessIdAndUserId($notificationdata);
                        if (empty($rows)) {
                        $this->coupons_model->insertbusinessIdAndUserId($notificationdata);
                        }*/
                        echo json_encode([
                            'status' => '200', 'message' => $this->lang->line('success_favorite'),
                            'data' => ['favourite' => 1, 'save' => $statussave],

                        ]);
                    } else {

                        echo json_encode([
                            'status' => '200', 'message' => $this->lang->line('success_unfavorite'),
                            'data' => ['favourite' => 0, 'save' => $statussave],

                        ]);
                    }
                }
            }
        }
    }

    public function savecoupon()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'coupon_id',
            'coupon id',
            'required',
            array(
                'required' => $this->lang->line('req_coupon_id'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {

                $data = array(
                    'user_id' => $this->user['id'],
                    'coupon_id' => $this->input->post('coupon_id'),
                );
                $coupon = $this->coupons_model->getCouponIdByCouponId($data['coupon_id']);

                if (empty($coupon->id)) {
                    echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_found_coupon'), 'data' => (object) []));
                    exit;
                } else {
                    $couponsave = $this->coupons_model->makeSavecoupon($data);
                    $couponfav = $this->coupons_model->FavoriteCouponstatus($data);

                    if (empty($couponfav)) {
                        $statusfav = 0;
                    } else {
                        $statusfav = 1;
                    }

                    if ($couponsave == 1) {
                        echo json_encode([
                            'status' => '200', 'message' => $this->lang->line('success_savecoupon'),
                            'data' => ['favourite' => $statusfav, 'save' => 1],

                        ]);
                    } else {

                        echo json_encode([
                            'status' => '200', 'message' => $this->lang->line('success_unsavecoupon'),
                            'data' => ['favourite' => $statusfav, 'save' => 0],
                        ]);
                    }
                }
            }
        }
    }

    public function getFavoritecoupon()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'user_id' => $this->user['id'],
                );
                $favorite_coupon = $this->coupons_model->getFavoritecoupon($data);
                if (!empty($favorite_coupon)) {

                    echo json_encode([
                        'status' => '200', 'message' => $this->lang->line('availeble_fav'),
                        'data' => ['favorite_coupon' => $favorite_coupon],
                    ]);
                } else {

                    echo json_encode([
                        'status' => '200', 'message' => $this->lang->line('not_availeble_fav'),
                        'data' => ['favorite_coupon' => $favorite_coupon],
                    ]);
                }
            }
        }
    }

    public function getsavecoupon()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {

                $data = array(
                    'user_id' => $this->user['id'],
                );

                $save_coupon = $this->coupons_model->getsavecoupondetails($data);
                if (!empty($save_coupon)) {

                    echo json_encode([
                        'status' => '200', 'message' => $this->lang->line('availeble_savecoupon'),
                        'data' => ['save_coupon' => $save_coupon],
                    ]);
                } else {

                    echo json_encode([
                        'status' => '500', 'message' => $this->lang->line('not_availeble_savecoupon'),
                        'data' => (object) [],
                    ]);
                }
            }
        }
    }

    public function filtercoupons()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );

        
        
        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'business_name' => $this->input->post('business_name'),
                    // 'category_id'   => explode(',', $this->input->post('category_id')),
                    // 'coupon_type' => $this->input->post('coupon_type'),
                    'coupon_type' =>explode(',', $this->input->post('coupon_type')),
                    'user_id' => $this->user['id'],
                    'user_city' => $this->user['city'], // PCR
                );
                $category_id = explode(',', $this->input->post('category_id'));

                $banner_coupons = $this->coupons_model->getbannerCoupons();
                $coupons = $this->coupons_model->getcouponsbyfilter($data, $category_id);
              

                $shuffle_val = $this->input->post('shuffle_val');
 log_message("error","SHUFFLEVAL:".$shuffle_val);
                if ($shuffle_val == "") {
                    $shuffle_val = 0;
                }

                if ($shuffle_val == 0) {
                    $shuffle_val = rand(1, 20);
                }
log_message("error","SHUFFLEVALGEN:".$shuffle_val);
                $this->seoShuffle($coupons, $shuffle_val);

                if (empty($coupons)) {
                    echo json_encode(['status' => '200', 'message' => $this->lang->line('not_coupons_available'), 'data' => ['shuffle_val' => $shuffle_val, 'coupon_details' => $coupons, 'banner_coupons' => $banner_coupons]]);
                } else {
                    echo json_encode(['status' => '200', 'message' => '', 'data' => ['shuffle_val' => $shuffle_val, 'coupon_details' => $coupons, 'banner_coupons' => $banner_coupons]]);
                }
            }
        }
    }
    public function seoShuffle(&$items, $shuffle_val)
    {
        mt_srand($shuffle_val);
        for ($i = count($items) - 1; $i > 0; $i--) {
            $j = @mt_rand(0, $i);
            $tmp = $items[$i];
            $items[$i] = $items[$j];
            $items[$j] = $tmp;
        }
    }

    public function filterBusiness()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'business_name' => $this->input->post('business_name'),
                    'user_id' => $this->user['id'],
                    'city' => $this->user['city'], // PCR
                );

                $category_id = explode(',', $this->input->post('category_id'));

            //   $city_id = isset($_REQUEST['city_id'])?$this->input->post('city_id'):'';
                $business = $this->coupons_model->getbusinessbyfilter($data, $category_id);
                shuffle($business);
                foreach($business as $key => &$val){
                    if(intval($val->city) != 0){
                        $val->city = $this->category_model->getCitiesById($val->city);
                    }
                }
                shuffle($business);
                if (empty($business)) {
                    echo json_encode(['status' => '200', 'message' => $this->lang->line('data_not'), 'data' => ['business_details' => $business]]);
                } else {
                    echo json_encode(['status' => '200', 'message' => '', 'data' => ['business_details' => $business]]);
                }
            }
        }
    }
    public function deletecoupon()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );
        $this->form_validation->set_rules(
            'coupon_id',
            'coupon id',
            'required',
            array(
                'required' => $this->lang->line('req_coupon_id'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'user_id' => $this->user['id'],
                    'coupon_id' => $this->input->post('coupon_id'),
                );
                $coupon = $this->coupons_model->getCouponIdByCouponId($data['coupon_id']);
                if (empty($coupon->id)) {
                    return $this->response_model->apiresponse('500', $this->lang->line('not_found_coupon'));
                }
                $deletecoupon = $this->coupons_model->deleteCouponByCouponId($data);
                if ($deletecoupon == 1) {
                    return $this->response_model->apiresponse('200', $this->lang->line('success_delete_coupon'));
                } else {
                    return $this->response_model->apiresponse('500', $this->lang->line('not_delete_coupon'));
                }
            }
        }
    }
}