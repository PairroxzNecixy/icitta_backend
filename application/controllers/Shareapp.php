<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shareapp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    // public function get_icitta()
    // {
    //     $os = $this->getOS($_SERVER['HTTP_USER_AGENT']);
    //     if ($os == "iPhone" || $os == "Mac OS" || $os == "Safari") {
    //         $redirect_to = "https://itunes.apple.com/us/app/instaconnect-snap-pics-video/id1477961493?ls=1&mt=8";
    //     } else {
    //         $redirect_to = "https://play.google.com/store/apps/details?id=com.pairroxz.icita";
    //     }

    //     redirect($redirect_to);
    //     die();

    //     // $this->load->view('get_explorii', ["os" => $os]);
    // }
    public function get_icitta()
    {
        
        $coupon_id = $this->input->get('coupon_id');

        $os = $this->getOS($_SERVER['HTTP_USER_AGENT']);
        $type = 0;
        
        if ($os == "iPhone" || $os == "Mac OS" || $os == "Safari") {
            $link = "com.icitta.apple://?couponId=$coupon_id";
            $type=1;
        } else {
            $link = "com.pairroxz.icita://icita?coupon_id=$coupon_id";
            $type=0;
        }

         if ($os == "iPhone" || $os == "Mac OS" || $os == "Safari") {
             $redirect_to = "https://itunes.apple.com/us/app/instaconnect-snap-pics-video/id1477961493?ls=1&mt=8";
         } else {
             $redirect_to = "https://play.google.com/store/apps/details?id=com.pairroxz.icita";
         }
        $this->load->view('shareapp', ["redirect_to" => $redirect_to,"link"=>$link,"type"=>$type]);
        //->load->view('shareapp', ["redirect_to" => $redirect_to,"link"=>$link]);
    }

    public function getOS($userAgent)
    {
        // Create list of operating systems with operating system name as array key
        $oses = array(
            'iPhone' => '(iPhone)',
            'Windows 3.11' => 'Win16',
            'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
            'Windows 98' => '(Windows 98)|(Win98)',
            'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
            'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
            'Windows 2003' => '(Windows NT 5.2)',
            'Windows Vista' => '(Windows NT 6.0)|(Windows Vista)',
            'Windows 7' => '(Windows NT 6.1)|(Windows 7)',
            'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
            'Windows ME' => 'Windows ME',
            'Open BSD' => 'OpenBSD',
            'Sun OS' => 'SunOS',
            'Linux' => '(Linux)|(X11)',
            'Safari' => '(Safari)',
            'Mac OS' => '(Mac_PowerPC)|(Macintosh)',
            'QNX' => 'QNX',
            'BeOS' => 'BeOS',
            'OS/2' => 'OS/2',
            'Search Bot' => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp/cat)|(msnbot)|(ia_archiver)',
        );

        // Loop through $oses array
        foreach ($oses as $os => $preg_pattern) {
            // Use regular expressions to check operating system type
            if (preg_match('@' . $preg_pattern . '@', $userAgent)) {
                // Operating system was matched so return $oses key
                return $os;
            }
        }

        // Cannot find operating system so return Unknown

        return 'n/a';
    }

}
