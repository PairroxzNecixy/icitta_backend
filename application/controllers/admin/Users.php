<?php 
class Users extends BASE_Controller{
    public function __construct()
    {
        parent::__construct();        
        $this->load->model('db_users');
        $this->load->model('db_coupons');
    }

    public function index()
    {
		$this->data['users'] = $this->db_users->getUsers();
        $this->load->view("user/allusers",$this->data);
	}
	
	public function generalUsers()
	{
        $this->data['users'] = $this->db_users->getGeneralUsers();
        $this->load->view("user/generalUsers",$this->data);
	}

	public function businessUsers()
	{
        $this->data['users'] = $this->db_users->getBusinessUsers();
        $this->load->view("user/businessUsers",$this->data);
	}

	public function sponsorUsers()
	{
        $this->data['users'] = $this->db_users->getSponsorUsers();
        $this->load->view("user/sponsorUsers",$this->data);
	}

	public function userDetail($id = null)
	{
		if(empty($id)){
			$this->session->set_flashdata('error', "Invalid user id.");
			return $this->redirect_back();
		}
		
		$this->data['user'] = $this->db_users->getUserById($id);

		if(empty($this->data['user'])){
			$this->session->set_flashdata('error', "User not found.");
			return $this->redirect_back();
		}

		// die("<pre>".var_dump($this->data));
		$this->data['coupons'] =  $this->db_coupons->getCouponsByUserId($id);

		if(trim($this->data['user']['user_type_slug']) == "business"){
			$this->data['business'] =  $this->db_users->getBusinessByUserId($id);
			return $this->load->view("user/businessDetail", $this->data);

		}elseif(trim($this->data['user']['user_type_slug']) == "user"){
			return $this->load->view("user/userDetail", $this->data);
		}elseif(trim($this->data['user']['user_type_slug']) == "sponsor"){
			$this->data['sponsor'] =  $this->db_users->getSponsorByUserId($id);
			return $this->load->view("user/sponsorDetail", $this->data);
		}else{
			$this->session->set_flashdata('error', "Some error has occured, Usertype not valid.");
			return $this->redirect_back();
		}

		$this->load->view("user/userDetail", $this->data);
	}



	public function approveUser()
	{
		$user_id = $this->input->post('id');

		if ($this->db_users->approveUser($user_id)) {
			die(json_encode(["error" => false, "msg" => "User approved successfully."]));
		} else {
			die(json_encode(["error" => true, "msg" => "Some error has occured. while processing your request."]));
		}
	}

	public function unApproveUser()
	{
		$user_id = $this->input->post('id');

		if ($this->db_users->unApproveUser($user_id)) {
			die(json_encode(["error" => false, "msg" => "User Un-Approved successfully."]));
		} else {
			die(json_encode(["error" => true, "msg" => "Some error has occured. while processing your request."]));
		}
	}
	public function deleteUser()
    {
        $user_id = $this->input->post('id');
        $user_type = $this->input->post('user_type');

        if ($this->db_users->deleteUser($user_id, $user_type)) {
            die(json_encode(["error" => false, "msg" => "User deleted successfully."]));
        } else {
            die(json_encode(["error" => true, "msg" => "Some error has occured. while processing your request."]));
        }
    }
}
