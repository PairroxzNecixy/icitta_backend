<?php 
class DetailPage extends BASE_Controller{
    public function __construct()
    {
        parent::__construct("");
        
        $this->load->model('db_categories');

        $this->data['DetailPage'] = $this->db_categories->getCategories();
        
    }

    public function index()
    {
        $this->load->view("detail_page");
    }

}
?>