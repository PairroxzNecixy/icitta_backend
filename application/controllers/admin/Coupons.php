<?php
class Coupons extends BASE_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('db_coupons');
		$this->load->model('notification_model');
	}

	public function index()
	{
		$this->data['coupons'] = $this->db_coupons->getCoupons();
        // print_r(json_encode($this->data));
        // die;
		$this->load->view("coupon/allcoupons", $this->data);
		// echo '<pre />';
		// print_r($this->data["Coupons"]);
	}

	public function approveCoupon()
	{
		$coupon_id = $this->input->post('id');

		if ($this->db_coupons->approveCoupon($coupon_id)) {
			
			$this->notification_model->sendAppprovedNotification($coupon_id);

			die(json_encode(["error" => false, "msg" => "Coupon approved successfully."]));
		} else {
			die(json_encode(["error" => true, "msg" => "Some error has occured. while processing your request."]));
		}
	}

	public function unApproveCoupon()
	{
		$coupon_id = $this->input->post('id');

		if ($this->db_coupons->unApproveCoupon($coupon_id)) {
			die(json_encode(["error" => false, "msg" => "Coupon Un-Approved successfully."]));
		} else {
			die(json_encode(["error" => true, "msg" => "Some error has occured. while processing your request."]));
		}
	}

	public function showCouponDetails($id = null)
	{
		if(empty($id)){
			$this->session->set_flashdata('error', "Invalid coupon id.");
			return $this->redirect_back();
		}
		
		$this->data['coupon'] = $this->db_coupons->getCouponById($id);

		if(empty($this->data['coupon'])){
			$this->session->set_flashdata('error', "Coupon not found.");
			return $this->redirect_back();
		}

		$this->load->view("coupon/coupon_detail", $this->data);
	}

	public function createCoupon()
	{
        $this->load->view("coupon/createcoupon");
	}

	public function saveCoupon()
	{
		$this->form_validation->set_rules('business_name', 'Business name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('link', 'Link address', 'trim|required|xss_clean');
        $this->form_validation->set_rules('priority', 'Priority', 'trim|required|is_natural_no_zero|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			return $this->redirect_back();
		}

		$this->load->model("upload_model");

		$path = "./uploads/bannerImage/";
		if (is_dir($path) === false) {
			mkdir($path, '0777', true);
		}

		$upload = $this->upload_model->upload_image("image", $path);
		if ($upload['error']) {
			$this->session->set_flashdata('error', $upload['error']);
			return $this->redirect_back();
		}

		$data = [
			'business_name'	=> $this->input->post("business_name"),
			'link'			=> $this->input->post("link"),
			'image'			=> $upload['data']['file_name'],
            'priority'      => $this->input->post("priority"),
		];

		if($this->db_coupons->add_new_banner_coupon($data)){
			$this->session->set_flashdata('success', "Coupon created successfully.");
			return $this->redirect_back();
		}else{
			$this->session->set_flashdata('error', "Some error has occured while creating coupon to database.");
			return $this->redirect_back();
		}
	}
	public function edit_coupon($id = null)
	{
		if (empty($id)) {
			$this->session->set_flashdata('error', "Invalid coupon id.");
			return $this->redirect_back();
		}

		$this->data['coupon'] = $this->db_coupons->getCouponById($id);
		$this->data["cuponTypeArray"] = $this->db_coupons->getCouponTypes();

		if (empty($this->data['coupon'])) {
			$this->session->set_flashdata('error', "Coupon not found.");
			return $this->redirect_back();
		}

		$this->load->view("coupon/edit_coupon_b", $this->data);
	}

	public function update_coupon()
	{
		$this->form_validation->set_rules('id', 'ID', 'trim|required|xss_clean');
		$this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('expire_date', 'expire_date', 'trim|required|xss_clean');
        $this->form_validation->set_rules('CuponType', 'CuponType', 'trim|required|xss_clean');
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			return $this->redirect_back();
		}

		$this->load->model("upload_model");

		$path = "./uploads/coupons/";
		if (is_dir($path) === false) {
			mkdir($path, '0777', true);
		}

		$value = $this->input->post("value");


		$data = [
			'name'			=> $this->input->post("name"),
			'description'	=> $this->input->post("description"),
			'expire_date'	=> date("Y-m-d H:i:s", strtotime($this->input->post("expire_date"))),
			"coupon_type_id" => $this->input->post("CuponType")
		];

		$id = $this->input->post("id");

		$upload = $this->upload_model->upload_image("image", $path);
		if ($upload['error']) { } else {
			$data['image'] = $upload['data']['file_name'];
		}

		if(!empty($value)){
			$data['value'] = $value;
		}else{
		    $data['value'] = '';
		}



		if ($this->db_coupons->update_coupon($id, $data)) {
			$this->session->set_flashdata('success', "Coupon updated successfully.");
			return $this->redirect_back();
		} else {
			$this->session->set_flashdata('error', "Some error has occured while updating coupon to database.");
			return $this->redirect_back();
		}
	}


	public function deleteCoupon()
	{
		$coupon_id = $this->input->post('id');

        if ($this->db_coupons->deleteCoupon($coupon_id)) {
            die(json_encode(["error" => false, "msg" => "Coupon deleted successfully."]));
        } else {
            die(json_encode(["error" => true, "msg" => "Some error has occured. while processing your request."]));
        }
	}



}
