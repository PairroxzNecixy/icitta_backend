<?php
class Categories extends BASE_Controller
{
	public function __construct()
	{
		parent::__construct("");

		$this->load->model('db_categories');
	}

	public function index()
	{
		$this->data['categories'] = $this->db_categories->getCategories();
		$this->load->view("category", $this->data);
	}

	public function create()
	{
		$this->form_validation->set_rules('name', 'Category name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('name_italian', 'Nome della categoria', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			return $this->redirect_back();
		}

		$this->load->model("upload_model");

		$path = "./uploads/categories/";
		if (is_dir($path) === false) {
			mkdir($path, '0777', true);
		}

		$upload = $this->upload_model->upload_image("image", $path);
		if ($upload['error']) {
			$this->session->set_flashdata('error', $upload['error']);
			return $this->redirect_back();
		}

		$data = [
			'name'	=> $this->input->post("name"),
			'name_italian'	=> $this->input->post("name_italian"),
			'image'	=> $upload['data']['file_name'],
		];

		if($this->db_categories->add_new($data)){
			$this->session->set_flashdata('success', "Category added successfully.");
			return $this->redirect_back();
		}else{
			$this->session->set_flashdata('error', "Some error has occured while saving category to database.");
			return $this->redirect_back();
		}
	}
}
