<?php 
class ForgetPassword extends BASE_Controller{
    public function __construct()
    {
        parent::__construct("");
        
        $this->load->model('db_categories');

        $this->data['ForgetPassword'] = $this->db_categories->getCategories();
        
    }

    public function index()
    {
        $this->load->view("forget_password");
    }

}
?>