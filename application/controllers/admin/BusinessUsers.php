<?php 
class BusinessUsers extends BASE_Controller{
    public function __construct()
    {
        parent::__construct("");
        
        $this->load->model('db_users');

        $this->data['BusinessUsers'] = $this->db_users->getUsers();
        
    }

    public function index()
    {
        $this->load->view("user/business",$this->data);
        // echo '<pre />';
        // print_r($this->data["BusinessUsers"]);
    }

}
?>