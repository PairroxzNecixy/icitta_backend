<?php
class BusinessCoupon extends BASE_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('db_coupons');
	}

	public function index()
	{
		$this->data['businessCoupon'] = $this->db_coupons->getCoupons();
		$this->load->view("coupon/businesscoupon", $this->data);
	}
}
