<?php
class SponsoredList extends BASE_Controller
{
    public function __construct()
    {
        parent::__construct("");

        $this->load->model('db_coupons');
    }

    public function index()
    {
        $this->data['sponsoredList'] = $this->db_coupons->getSponsorCoupons();

        $this->load->view("coupon/sponsoredlist", $this->data);
        // echo '<pre />';
        // print_r($this->data["SponsoredList"]);
    }

    public function deleteSponsoredCoupon()
    {
        $coupon_id = $this->input->post('id');

        if ($this->db_coupons->deleteSponsoredCoupon($coupon_id)) {
            die(json_encode(["error" => false, "msg" => "Coupon deleted successfully."]));
        } else {
            die(json_encode(["error" => true, "msg" => "Some error has occured. while processing your request."]));
        }
    }

    public function edit_coupon($id)
    {
		$this->data['coupon'] = $this->db_coupons->getSponsorCouponById($id);
        
        $this->load->view("coupon/edit_coupon", $this->data);
    }

    public function updateCoupon()
    {
        $this->form_validation->set_rules('id', 'Coupon Id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('business_name', 'Business name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('link', 'Link address', 'trim|required|xss_clean');
        $this->form_validation->set_rules('priority', 'Priority', 'trim|required|is_natural_no_zero|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            return $this->redirect_back();
        }

        $coupon_id = $this->input->post("id");
		$coupon = $this->db_coupons->getSponsorCouponById($coupon_id);


        $this->load->model("upload_model");

        $path = "./uploads/bannerImage/";
        if (is_dir($path) === false) {
            mkdir($path, '0777', true);
        }

        $upload = $this->upload_model->upload_image("image", $path);
        if ($upload['error']) {
            $image = $coupon->image;
        }else{
            $image = $upload['data']['file_name'];
        }

        $data = [
            'business_name' => $this->input->post("business_name"),
            'link'          => $this->input->post("link"),
            'image'         => $image,
            'priority'      => $this->input->post("priority"),
        ];

        if ($this->db_coupons->update_new_banner_coupon($coupon_id, $data)) {
            $this->session->set_flashdata('success', "Coupon updated successfully.");
            return redirect("admin/sponsoredList");
        } else {
            $this->session->set_flashdata('error', "Some error has occured while updating coupon to database.");
            return $this->redirect_back();
        }
    }
}
