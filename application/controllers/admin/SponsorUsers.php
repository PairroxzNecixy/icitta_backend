<?php 
class SponsorUsers extends BASE_Controller{
    public function __construct()
    {
        parent::__construct("");
        
        $this->load->model('db_users');

        $this->data['SponsorUsers'] = $this->db_users->getUsers();
        
    }

    public function index()
    {
        $this->load->view("user/sponsor",$this->data);
    }

}
?>