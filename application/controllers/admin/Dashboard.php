<?php 
class Dashboard extends BASE_Controller{
    public function __construct()
    {
		parent::__construct();
		
		if(empty($this->session->userdata('logged_in'))){
			redirect('admin/login');
		}
        
        $this->load->model('db_dashboard');
        $this->load->model('db_users');
        $this->load->model('db_coupons');
        
        $this->data['Users'] = $this->db_users->getUsers();
        
    }
    public function index()
    {
        // die("<pre>".json_encode($this->session));
        
        $this->data['coupons']                  = $this->db_coupons->getRecentCoupons();
		$this->data['total_coupons']            = $this->db_coupons->total();
		$this->data['total_percentage_coupons'] = $this->db_coupons->total(COUPON_PERCENTAGE_TYPE);
		$this->data['total_promotion_coupons']  = $this->db_coupons->total(COUPON_PROMOTION_TYPE);
		$this->data['total_box_coupons']        = $this->db_coupons->total(COUPON_BOX_TYPE);
		$this->data['total_sponsor_coupons']    = $this->db_coupons->total(COUPON_SPONSOR_TYPE);

		$this->data['total_users']              = $this->db_users->total();
		$this->data['total_general_users']      = $this->db_users->total(USER_TYPE_USER);
		$this->data['total_business_users']     = $this->db_users->total(USER_TYPE_BUSINESS);
		$this->data['total_sponsor_users']      = $this->db_users->total(USER_TYPE_SPONSOR);
		
		$this->data['total_verified_coupons']   = $this->db_coupons->totalVerifiedCoupons();
        $this->data['totalNoOfTimesVerifiedCoupons']   = $this->db_coupons->totalNoOfTimesVerifiedCoupons();

        $this->load->view("dashboard",$this->data);
        // echo '<pre />';
        // print_r($this->data["Coupons"]);
    }

}
?>
