<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends BASE_Controller
{
	public function __construct()
	{
		parent::__construct();


		$this->load->model('db_dashboard');
		$this->load->model('db_users');
		$this->load->model('db_coupons');


		$this->data['Users'] = $this->db_users->getUsers();
		$this->data['Coupons'] = $this->db_coupons->getCoupons();
	}
	public function index()
	{
		$this->load->view("login");
	}

	public function login()
	{

		$this->form_validation->set_rules('username', 'Username', 'trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			return $this->redirect_back();
		}

		$email = $this->input->post("username");
		$password = $this->input->post("password");

		$user = $this->db_users->getAdminByEmail($email);
		if (empty($user)) {
			$this->session->set_flashdata('error', "Email is not registered. please provide a registered email address.");
			return $this->redirect_back();
		}

		if (password_verify($password, $user->password)) {
			unset($user->password);

			$this->session->set_userdata('logged_in', true);
			$this->session->set_userdata('user', $user);
			$this->session->set_flashdata('success', "Welcome back ".$user->name."!");
			redirect("admin/dashboard");
		}

		$this->session->set_flashdata('error', "Invalid credentials, please provide valid credentials.");
		return $this->redirect_back();
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin/login');
	}
}
