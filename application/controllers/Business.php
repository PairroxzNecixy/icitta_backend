<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Business extends CI_Controller {

    public $user;
    public $auth_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model("response_model");
        $this->load->model("auth_model");
        $this->load->model('business_model');
        $this->auth_key = $this->input->post('auth_key');
        $this->user = $this->auth_model->authenticateUser($this->auth_key);
        if (!empty($this->user)) {
            $this->lang->load('message', $this->user['language']);
        } else {
            $this->lang->load('message', 'en');
        }
    }

    public function updatebusinessprofile()
    {
        $this->form_validation->set_rules(
            'name',
            'name',
            'required',
            array(
                'required' => $this->lang->line('req_name')
            )
        );
        
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );

        if ($this->form_validation->run() == FALSE) 
        {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object)[]));
            exit();

        } else {

            if (empty($this->user)) 
            {

                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object)[]]);

            } else {

                $data = array(

                    'name'      =>  $this->input->post('name'),
                    'city'      =>  $this->input->post('city'),
                    'address'   =>  $this->input->post('address'),
                    'lat'       =>  $this->input->post('lat'),
                    'lng'       =>  $this->input->post('lng'),
                    'user_id'   =>  $this->user['id']
                );
                
                $businessupdate = $this->business_model->updateBusinessById($data);
                if ($businessupdate) 
                {

                    echo json_encode(['status' => '200', 'message' => $this->lang->line('success_updation'), 'data' => (object)[]]);
                
                } else {
                    
                    echo json_encode(['status' => '500', 'message' => $this->lang->line('unsuccess_updation'), 'data' => (object)[]]);
               
                }
            }
        }
    }

    public function getBusinessProfile()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );

        $business_user_id = $this->input->post("business_id");

        if ($this->form_validation->run() == FALSE) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data'  => (object)[]));
            exit();
        } else {

            if (empty($this->user) && empty($business_user_id))
            {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data'  => (object)[]]);
            } else {

                if(!empty($business_user_id))
                {
                    $data = array('user_id' => $business_user_id);    
                }else{
                    $data = array('user_id' => $this->user['id']);                    
                }

                $businessProfile = $this->business_model->getBusinessProfileByUserId($data);
                $coupons = $this->business_model->getcouponByUserId($data);
                 $window = $this->business_model->getShopWindowUserId($businessProfile);
                //  print_r($this->db->last_query());
                //  die;
                if(intval($businessProfile->city) != 0){
                    $businessProfile->city = $this->category_model->getCitiesById($businessProfile->city);
                }
                if(empty($businessProfile))
                {
                    echo json_encode(['status' => '500', 'message' => $this->lang->line('data_not'), 'data' => (object)[]]);
                }else{
                    echo json_encode(['status' => '200', 'message' => '', 'data' => ['coupon_details' => $coupons, 'business_profile' => $businessProfile,"Business_Window" =>  $window  ] ]);
                }
            }
        }
    }
    public function updatebusiness()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );
        
        $this->form_validation->set_rules(
            'name',
            'name',
            'required',
            array(
                'required' => $this->lang->line('req_name')
            )
        );

        if ($this->form_validation->run() == FALSE) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            return $this->response_model->apiresponse('500', $message);
            //echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object)[]));
            exit();
        } else {
            if (empty($this->user)) {

                //echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object)[]]);
                return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
            } else {
                
                if ($this->user['user_type_id'] != 1) {
                    return $this->response_model->apiresponse('500', $this->lang->line('only_business'));
                }

                $config['upload_path']      =   './uploads/userProfile';
                $config['allowed_types']    =   'gif|jpg|png|jpeg|JPG|JPEG|PNG';
                $config['encrypt_name']     =   TRUE;
                $config['file_ext_tolower'] =   TRUE;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('banner_image')) 
                {
                    $image = $this->business_model->getImageName($this->user['id']);
                    $banner_image = $image->banner_image;
                   
                } else {
                    $upload = $this->upload->data();
                    $banner_image = $upload['file_name'];
                }

                    $data = array(
                        'user_id'       =>  $this->user['id'],
                        'name'          =>  $this->input->post('name'),
                        'about_us'      =>  $this->input->post('about_us'),
                        'banner_image'  =>  $banner_image
                    );

                    $updatebusiness = $this->business_model->updateBusiness($data);
                    $businessprofile = $this->business_model->getBusinessProfileByUserId($data);
                    if(intval($businessprofile->city) != 0){
                    $businessprofile->city = $this->category_model->getCitiesById($businessprofile->city);
                   }
                    if ($updatebusiness == TRUE) {
                        
                        echo json_encode(['status' => '200', 'message' => $this->lang->line('success_updation'), 'data' => ['business_profile'=>$businessprofile]]);
                    } else {
                        echo json_encode(['status' => '500', 'message' => $this->lang->line('unsuccess_updation'), 'data' => (object) []]);
                    }
            }
        }
    }

    public function updateSocialLinks()
    {
       
        
        $this->form_validation->set_rules(
            'auth_key',
            'auth key',
            'required', 
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );

        if ($this->form_validation->run() == FALSE) 
        {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object)[]));
            exit();

        } else {

            if (empty($this->user)) 
            {

                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object)[]]);

            } else {

                $data = array(

                    'facebook_link' =>  $this->input->post('facebook_link'),
                    'insta_link'    =>  $this->input->post('insta_link'),
                    'website_link'  =>  $this->input->post('website_link'),
                    'user_id'       =>  $this->user['id']
                );
                
                $socialupdate = $this->business_model->updateSocialLinks($data);
                if ($socialupdate) 
                {
                    $businessprofile = $this->business_model->getBusinessProfileByUserId($data);
                    if(intval($businessprofile->city) != 0){
                    $businessprofile->city = $this->category_model->getCitiesById($businessprofile->city);
                    }
                    echo json_encode(['status' => '200', 'message' => $this->lang->line('success_updation'), 'data' => ['business_profile'=>$businessprofile]]);
                
                } else {
                    
                    echo json_encode(['status' => '500', 'message' => $this->lang->line('unsuccess_updation'), 'data' => (object)[]]);
               
                }
            }
        }
    }

    public function getBusinessdetails()
    {
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key')
            )
        );

        if ($this->form_validation->run() == FALSE) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data'  => (object)[]));
            exit();
        } else {
           
            if (empty($this->user))
            {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data'  => (object)[]]);
            } else {
                $business = $this->business_model->getBusinessDetailsNotification($this->user['id']);
               
                if(!empty($business))
                {
                    $data = array(
                        'user_id' => $this->user['id']
                    );
                    $businessProfile = $this->business_model->getBusinessProfileByUserId($data);
                    if(intval($businessProfile->city) != 0){
                    $businessProfile->city = $this->category_model->getCitiesById($businessProfile->city);
                }
                    echo json_encode(['status' => '200', 'message' => '', 'data'  => ['business_profiles'=>$businessProfile, 'users'=>$business]]);
                }else{
                    echo json_encode(['status' => '500', 'message' => $this->lang->line('data_not'), 'data'  => (object)[]]);
                }
            }
        }
    }

}
?>