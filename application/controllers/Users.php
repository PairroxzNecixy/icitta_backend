<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    public $language;
    public function __construct()
    {
        parent::__construct();
        $this->load->model("auth_model");
        $this->load->model("users_model");
        $this->load->model("response_model");
        $this->load->model("oauth");
        $header = $this->input->request_headers();
        if (empty($header)) {
            echo json_encode(['status' => '500', 'message' => 'please select header value', 'data' => (object) []]);
            exit;
        }
        if (!isset($header['Lang']) || empty($header['Lang'])) {
            $this->language = 'en';
        } else {

            if ($header['Lang'] == 'en') {
                $this->language = $header['Lang'];
            } elseif ($header['Lang'] == 'it') {
                $this->language = $header['Lang'];
            } else {
                $this->language = 'en';
            }
        }

        $this->lang->load('message', $this->language);
    }

    public function addUser()
    {
        // error_log('regitration called');
        $this->form_validation->set_rules(
            'name',
            'Username',
            'required',
            array(
                'required' => $this->lang->line('req_name'),
            )
        );

        $this->form_validation->set_rules(
            'email',
            'Email',
            'required|valid_email|is_unique[users.email]',
            array(
                'required' => $this->lang->line('req_email'),
                'valid_email' => $this->lang->line('valid_email'),
                'is_unique' => $this->lang->line('unique_email'),
            )
        );

        // $this->form_validation->set_rules(
        //     'password',
        //     'password',
        //     'required',
        //     array(
        //         'required' => $this->lang->line('req_pass'),
        //     )
        // );

        $this->form_validation->set_rules(
            'mobile',
            'mobile',
            'required|numeric|min_length[10]|max_length[10]',
            array(
                'required' => $this->lang->line('req_mobile'),
                'numeric' => $this->lang->line('req_num'),
                'min_length' => $this->lang->line('min_mob_len'),
                'max_length' => $this->lang->line('max_mob_len'),
            )
        );
        $this->form_validation->set_rules(
            'user_type',
            'user type',
            'required',
            array(
                'required' => $this->lang->line('req_user_type'),
            )
        );
        $this->form_validation->set_rules(
            'device_id',
            'device id',
            'required',
            array(
                'required' => $this->lang->line('req_device_id'),
            )
        );
        $this->form_validation->set_rules(
            'device_type',
            'device type',
            'required',
            array(
                'required' => $this->lang->line('req_device_type'),
            )
        );
        $this->form_validation->set_rules(
            'device_type',
            'device type',
            'required',
            array(
                'required' => $this->lang->line('req_player_key'),
            )
        );

        $social_provider = $this->input->post('social_provider');
        $social_id_token = $this->input->post('social_id_token');
        if ((!empty($social_provider) && empty($social_id_token)) || (!empty($social_id_token) && empty($social_provider))) {
            return $this->response_model->apiresponse('500', $this->lang->line('required_social_id_token'));
        }

        if ((empty($social_provider) && empty($social_id_token)) && empty($this->input->post('password'))) {
            return $this->response_model->apiresponse('500', $this->lang->line('req_pass'));
        }

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            //echo json_encode(array('status' => '500', 'message' => $message, 'data' => ));
            return $this->response_model->apiresponse('500', $message);
        } else {
            $user_type = $this->input->post('user_type');
            $lat = $this->input->post('lat') . "";
            $lng = $this->input->post('lng') . "";

            if ($user_type == 1 && !$this->isValidCity($lng, $lat)) {
                return $this->response_model->apiresponse('500', $this->lang->line('req_invalid_city'));

            }
            if ($user_type == 2 && !(strtolower($this->input->post('city')) === strtolower("carmagnola") || 
                                     strtolower($this->input->post('city')) === strtolower("carignano") ||
                                     strtolower($this->input->post('city')) === strtolower("racconigi") ||
                                     strtolower($this->input->post('city')) === strtolower("villastellone") ||
                                     strtolower($this->input->post('city')) === strtolower("sommariva del bosco ") ||
                                     strtolower($this->input->post('city')) === strtolower("santena") ||
                                     strtolower($this->input->post('city')) === strtolower("vinovo") ||
                                     strtolower($this->input->post('city')) === strtolower("trofarello") ||
                                     strtolower($this->input->post('city')) === strtolower("savigliano") ||
                                     strtolower($this->input->post('city')) === strtolower("bra") ||
                                     strtolower($this->input->post('city')) === strtolower("alba") 
                                            
                                    )) {
                return $this->response_model->apiresponse('500', $this->lang->line('req_invalid_city'));
            }

            $aboutus = $this->input->post('about_us');
            $config['upload_path'] = './uploads/userProfile';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('banner_image')) {
                $banner_image = 'default.jpg';
                //echo json_encode($this->upload->display_errors());
            } else {
                $upload = $this->upload->data();
                $banner_image = $upload['file_name'];
            }

            $config['upload_path'] = './uploads/userProfile';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['encrypt_name'] = true;
            $config['file_ext_tolower'] = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                $image = 'default.jpg';
                //echo json_encode($this->upload->display_errors());
            } else {
                $upload = $this->upload->data();
                $image = $upload['file_name'];
            }
            $player_id = $this->input->post('player_id');
            if (empty($player_id)) {
                $player_id = '';
            }
            $usertypecount = $this->users_model->countUserType();
            $lat = $this->input->post('lat');
            $lng = $this->input->post('lng');
            $user_type = $this->input->post('user_type');
            if ($user_type == 3) {
                return $this->response_model->apiresponse('500', $this->lang->line('not_for_registration'));
            }
            if ($user_type == 0 || $user_type > $usertypecount) {

                return $this->response_model->apiresponse('500', $this->lang->line('user_type'));
            }
            if ($user_type == 1) {
                $category_id = $this->input->post('category_id');

                $countcategories = $this->users_model->getcategories();

                if (!isset($aboutus)) {

                    return $this->response_model->apiresponse('500', $this->lang->line('aboutus_set'));
                } elseif (empty($aboutus)) {

                    return $this->response_model->apiresponse('500', $this->lang->line('empty_aboutus'));
                } elseif ($category_id == 0) { ///extra condition || $category_id > $countcategories

                    return $this->response_model->apiresponse('500', $this->lang->line('category_not_exist'));
                }
            }

            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'country_code' => '',
                'mobile' => $this->input->post('mobile'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'user_type_id' => $this->input->post('user_type'),
                'city' => $this->input->post('city'),
                'language' => $this->language,
                'player_id' => $player_id,
            );

            if (empty($this->users_model->getUserbyemail($data))) {
                $user_id = $this->users_model->insertUser($data);

                $lat = $this->input->post('lat') . "";
                $lng = $this->input->post('lng') . "";

                if ($user_type == 1) {

                    $business_data = array(
                        'name' => $this->input->post('name'),
                        'user_id' => $user_id,
                        'lat' => $lat,
                        'lng' => $lng,
                        'city' => $this->input->post('city'),
                        'address' => $this->input->post('address'),
                        'about_us' => $aboutus,
                        'banner_image' => $banner_image,
                        'category_id' => $this->input->post('category_id'),
                        'image' => $image,
                    );
                    $this->users_model->insertBusinessprofiles($business_data);
                } elseif ($user_type == 2) {
                    $user_data = array(
                        'name' => $this->input->post('name'),
                        'user_id' => $user_id,
                        'lat' => $lat,
                        'lng' => $lng,
                        'city' => $this->input->post('city'),
                        'address' => $this->input->post('address'),
                        'image' => $image,
                    );
                    $this->users_model->insertUserprofiles($user_data);
                } elseif ($user_type == 3) {
                    $sponsor_data = array(
                        'name' => $this->input->post('name'),
                        'user_id' => $user_id,
                        'lat' => $lat,
                        'lng' => $lng,
                        'city' => $this->input->post('city'),
                        'address' => $this->input->post('address'),
                        'category_id' => $this->input->post('category_id'),
                        'image' => $image,
                    );
                    $this->users_model->insertSponsorprofiles($sponsor_data);
                }
                $user = $this->users_model->getuserdetails($user_id);
                if ($user['user_type_id'] == 1) {
                    $data_profile = $this->users_model->getBusinessProfiles($user_id);
                } elseif ($user['user_type_id'] == 2) {
                    $data_profile = $this->users_model->getUsersProfiles($user_id);
                } else {
                    $data_profile = 'registration failed';
                }

                if (empty($user_id)) {

                    return $this->response_model->apiresponse('500', $this->lang->line('registration_faild'));
                } else {

                    if (!empty($social_provider)) {
                        $d = [
                            'provider' => $social_provider,
                            'user_id' => $user_id,
                            'access_token' => $this->input->post('social_access_token') . "",
                            'id_token' => $social_id_token,
                            'expires_in' => date("Y-m-d H:i:s", strtotime("+2 month")),
                            'token_type' => "",
                            'refresh_token' => "",
                            'email' => $this->input->post('email') . "",
                            'name' => "",
                            'picture' => "",
                            'locale' => $this->input->post('locale') . "",
                            'link' => $this->input->post('social_link') . "",
                        ];

                        $this->oauth->save($d);
                    }

                    $authdata = array(
                        'user_id' => $user_id,
                        'device_id' => $this->input->post('device_id'),
                        'device_type' => $this->input->post('device_type'),
                        'auth_key' => $this->users_model->genauthkey(),
                        'player_id' => $player_id,
                    );

                    $this->users_model->insertAuth($authdata);

                    $data = ['id' => $user_id, 'auth_key' => $authdata['auth_key'], 'player_id' => $user['player_id'], 'user_type' => $user['user_type_id'], 'user_profile' => $data_profile];
                    return $this->response_model->apiresponse('200', $this->lang->line('registration_success'), $data);
                }
            } else {

                return $this->response_model->apiresponse('500', $this->lang->line('registration_already'));
            }
        }
    }

    public function socialLogin()
    {
        log_message("error","social_login_test: ".json_encode($_POST));
        $this->form_validation->set_rules('device_id', 'Device ID', 'required|xss_clean');
        $this->form_validation->set_rules('device_type', 'Device type', 'required|xss_clean');
        $this->form_validation->set_rules('social_provider', 'social provider', 'trim|required|max_length[128]|xss_clean');
        $this->form_validation->set_rules('social_id_token', 'Social Id Token', 'required|xss_clean');

        if ($this->form_validation->run() == false) {
            $validation_array = $this->form_validation->error_array();
            $msg = array_values($validation_array)[0];
            die(json_encode(array('status' => '500', 'message' => $msg, 'data' => (object) [])));
        }

        $data = [
            "provider" => $this->input->post('social_provider'),
            "id_token" => $this->input->post('social_id_token'),
        ];

        $oAuth = $this->oauth->ex_oauth($data);

        if (empty($oAuth)) {
            $response = [
                "registration_required" => true,
            ];
            die(json_encode(array('status' => '200', 'message' => $this->lang->line('required_social_registration'), 'data' => $response)));
        }

        $data = array(
            'device_id' => $this->input->post('device_id'),
            'device_type' => $this->input->post('device_type'),
        );

        $user = $this->users_model->getUserByUserId($oAuth->user_id);
        $user_id = $user['id'];

        $user_type = $this->users_model->getUserType($user)["id"];
        if (!empty($user)) {

            if ($user['user_type_id'] == 1) {
                $data_profile = $this->users_model->getBusinessProfiles($user_id);
            } elseif ($user['user_type_id'] == 2) {
                $data_profile = $this->users_model->getUsersProfiles($user_id);
            } else {
                $data_profile = $this->lang->line('user_type_login');
            }

            $auth_key = $this->users_model->genauthkey();
            $player_id = $this->input->post('player_id');
            if (empty($player_id)) {
                $player_id = '';
            }

            $authData = array(
                'user_id' => $user['id'],
                'auth_key' => $auth_key,
                'device_id' => $data['device_id'],
                'device_type' => $data['device_type'],
                'player_id' => $player_id,
            );
            $userdata = array(
                'user_id' => $user_id,
                'language' => $this->language,
            );
            $playerdata = array(
                'user_id' => $user_id,
                'player_id' => $player_id,
            );

            $authusers = $this->users_model->getAuthUserId($authData);
            $this->auth_model->updatelaguage($userdata);
            $this->auth_model->updateplayerId($playerdata);

            if (empty(count($authusers))) {

                if ($this->users_model->insertAuthdata($authData)) {

                    echo json_encode(array('status' => '200', 'message' => $this->lang->line('login'), 'data' => ['auth_key' => $auth_key, "registration_required" => false, 'user_type' => $user_type, 'user_profile' => $data_profile]));
                    exit();
                } else {
                    echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_login'), 'data' => (object) []));
                    exit();
                }
            } else {
                if ($this->users_model->updateAuthdata($authData)) {

                    echo json_encode(array('status' => '200', 'message' => $this->lang->line('login'), 'data' => ['auth_key' => $auth_key, "registration_required" => false, 'user_type' => $user_type, 'user_profile' => $data_profile]));
                    exit();
                } else {

                    echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_login'), 'data' => (object) []));
                    exit();
                }
            }

        } else {
            echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_email'), 'data' => (object) []));
            exit();
        }

    }
    public function userLogin()
    {
        $this->form_validation->set_rules(
            'email',
            'Email',
            'required',
            array(
                'required' => $this->lang->line('req_email'),
            )
        );
        $this->form_validation->set_rules(
            'password',
            'password',
            'required',
            array(
                'required' => $this->lang->line('req_pass'),
            )
        );
        $this->form_validation->set_rules(
            'device_id',
            'device id',
            'required',
            array(
                'required' => $this->lang->line('req_device_id'),
            )
        );
        $this->form_validation->set_rules(
            'device_type',
            'device type',
            'required',
            array(
                'required' => $this->lang->line('req_device_type'),
            )
        );
        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            $data = array(

                'email' => $this->input->post('email'),
                'password' => $this->input->post('password'),
                'device_id' => $this->input->post('device_id'),
                'device_type' => $this->input->post('device_type'),

            );

            $user = $this->users_model->getUserbyemail($data);
            $user_id = $user['id'];
            $userType = $this->users_model->getUserType($user);

            if ($user['user_type_id'] == 1) {
                $data_profile = $this->users_model->getBusinessProfiles($user_id);
            } elseif ($user['user_type_id'] == 2) {
                $data_profile = $this->users_model->getUsersProfiles($user_id);
            } else {
                $data_profile = $this->lang->line('user_type_login');
            }

            if (!empty($user)) {
                if (password_verify($data['password'], $user['password'])) {

                    $this->session->set_userdata(['name' => $user['name'], 'email' => $user['email']]);

                    $auth_key = $this->users_model->genauthkey();
                    $player_id = $this->input->post('player_id');
                    if (empty($player_id)) {
                        $player_id = '';
                    }
                    $authData = array(
                        'user_id' => $user['id'],
                        'auth_key' => $auth_key,
                        'device_id' => $data['device_id'],
                        'device_type' => $data['device_type'],
                        'player_id' => $player_id,
                    );
                    $userdata = array(
                        'user_id' => $user_id,
                        'language' => $this->language,
                    );
                    $playerdata = array(
                        'user_id' => $user_id,
                        'player_id' => $this->input->post('player_id'),
                    );

                    $authusers = $this->users_model->getAuthUserId($authData);
                    $this->auth_model->updatelaguage($userdata);
                    $this->auth_model->updateplayerId($playerdata);
                    if (empty(count($authusers))) {

                        if ($this->users_model->insertAuthdata($authData)) {

                            echo json_encode(array('status' => '200', 'message' => $this->lang->line('login'), 'data' => ['auth_key' => $auth_key, 'user_type' => $userType['id'], 'user_profile' => $data_profile]));
                            exit();
                        } else {
                            echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_login'), 'data' => (object) []));
                            exit();
                        }
                    } else {
                        if ($this->users_model->updateAuthdata($authData)) {

                            echo json_encode(array('status' => '200', 'message' => $this->lang->line('login'), 'data' => ['auth_key' => $auth_key, 'user_type' => $userType['id'], 'user_profile' => $data_profile]));
                            exit();
                        } else {

                            echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_login'), 'data' => (object) []));
                            exit();
                        }
                    }
                } else {

                    echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_pass'), 'data' => (object) []));
                    exit();
                }
            } else {
                echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_email'), 'data' => (object) []));
                exit();
            }
        }
    }

    public function loginToFacebook()
    {
        $this->form_validation->set_rules(
            'access_token',
            'token id',
            'required',
            array(
                'required' => 'access_token is required',
            )
        );
        $this->form_validation->set_rules(
            'token_key',
            'token key',
            'required',
            array(
                'required' => 'token key is required',
            )
        );

        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => []));
            exit();
        } else {

            $data = array(

                'user_id' => $this->input->post('user_id'),
                'access_token' => $this->input->post('access_token'),
                'token_key' => $this->input->post('token_key'),

            );
            $user_id = $data['user_id'];
            $fbloginuser = $this->users_model->getFBLoginUser($user_id);

            if (empty($fbloginuser)) {
                if ($this->users_model->insertFBdata($data)) {

                    echo json_encode(array('status' => '200', 'message' => $this->lang->line('login'), 'data' => []));
                } else {

                    echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_login'), 'data' => []));
                }
            } else {

                if ($this->users_model->updateFBdata($data)) {

                    echo json_encode(array('status' => '200', 'message' => $this->lang->line('login'), 'data' => []));
                } else {

                    echo json_encode(array('status' => '500', 'message' => $this->lang->line('not_login'), 'data' => []));
                }
            }
        }
    }

    public function getUserTypes()
    {
        $userTypes = $this->users_model->getUserTypeAll();
        if (!empty($userTypes)) {
            echo json_encode(['status' => '200', 'message' => '', 'data' => ['user_type' => $userTypes]]);
        } else {
            echo json_encode(['status' => '500', 'message' => 'user types not avaiable', 'data' => (object) []]);
        }
    }
    public function forgotpassword()
    {
        $this->form_validation->set_rules(
            'email',
            'token key',
            'required|valid_email',
            array(
                'required' => 'email id is required',
                'valid_email' => 'please mention valid email',
            )
        );

        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            $inputs = array(
                'email' => $this->input->post('email'),
            );

            $user = $this->users_model->getUserbyemail($inputs);

            if (empty($user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_email'), 'data' => (object) []]);
            } else {

                $data = array(
                    'code' => $this->users_model->genauthkey(),
                    'link' => base_url() . 'users/formforgotpassword',
                    'user_id' => $user['id'],
                    'email' => $inputs['email'],
                );

                $sendemail = $this->users_model->sendemail($data);
                // echo json_encode($sendemail) ;
                // exit;
                // if($sendemail == FALSE)
                // {
                $this->users_model->insertForgotQuery($data);
                echo json_encode(['status' => '200', 'message' => $this->lang->line('send_email'), 'data' => (object) []]);
                // }else{
                //     echo json_encode(['status' => '500', 'message' => 'sorry email was not send', 'data'=> []]);
                // }
            }
        }
    }
     public function updatepassword()
    {
        $message="";
        $shouldRedirect = false;
        $data = array(
            'code' => $this->input->post('code'),
            'password' => $this->input->post('password'),
            'confirm_password' => $this->input->post('confirm_password'),
        );
        $this->form_validation->set_rules(
            'password',
            'password',
            'required|min_length[6]',
            array(
                'required' => $this->lang->line('req_pass'),
                'min_length' => $this->lang->line('min_pass_len'),
            )
        );
        $this->form_validation->set_rules(
            'confirm_password',
            'confirm_password',
            'required',
            array(
                'required' => $this->lang->line('req_confirm_pass'),
            )
        );
        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            $message = reset($error);
            $shouldRedirect = true;
            // echo $message;
            // exit();
        } else {

            if (isset($_REQUEST['submit'])) {


                if ($data['password'] == $data['confirm_password']) {
                    $requestdata = $this->users_model->getRequestDetails($data['code']);

                    if (!empty($requestdata)) {
                        $updatedata = array(
                            'password' => password_hash($data['password'], PASSWORD_DEFAULT),
                            'user_id' => $requestdata['user_id'],
                        );
                        $updatepass = $this->users_model->updatepass($updatedata);
                        if ($updatepass == 1) {
                            $this->users_model->deleterequestquery($updatedata);
                             $message = 'Complimenti! La tua password è stata aggiornata con successo.';
                            $shouldRedirect = false;
                            //echo 'password update successfully';
                        } else {
                            $message= 'Scusate! impossibile aggiornare la password.';
                            $shouldRedirect = true;
                            // echo 'password not update';
                        }
                    } else {
                        $message = 'Scusate! la tua richiesta non esiste';
                        $shouldRedirect = true;
                        // echo 'sorry! your request not exist';
                    }
                } else {
                    $message= 'la nuova password e la conferma della password non corrispondono';
                    $shouldRedirect = true;
                    // echo 'new password and confirm password do not match';
                }
                
            }
        }
        $this->session->set_flashdata('message', $message."");
        if($shouldRedirect==true || $shouldRedirect =='true'){
            //redirect to some function
            redirect("users/formforgotpassword?code=".$data["code"]);
        }else{
            $this->session->set_flashdata('info',$message);
            $this->load->view('info');
            
        }
    }
    
    

    
    
    public function updateCity() {
        
        
        $this->user = $this->auth_model->authenticateUser($this->input->post('auth_key'));
        
       
   
        $this->form_validation->set_rules(
            'auth_key',
            'Auth key',
            'required',
            array(
                'required' => $this->lang->line('req_auth_key'),
            )
        );

        if ($this->form_validation->run() == false) {

            $error = $this->form_validation->error_array();
            $message = reset($error);
            echo json_encode(array('status' => '500', 'message' => $message, 'data' => (object) []));
            exit();
        } else {

            if (empty($this->user)) {
                echo json_encode(['status' => '500', 'message' => $this->lang->line('not_auth'), 'data' => (object) []]);
            } else {
                $data = array(
                    'city' => $this->input->post('city'),
                    'user_id' => $this->user['id'],
                );
               $response = $this->users_model->updateCity($data);
                
             if ($response!==false) 
                 echo json_encode(array('status' => '200', 'message' => 'ok', 'data' => json_decode($response)));
                else echo json_encode(array('status' => '500', 'message' => 'a problem occourred', 'data' => []));
            }
        }
}
 
    
    public function formforgotpassword()
    {
        $data = array(
            'code' => $_GET['code'],
        );

        $requestdata = $this->users_model->getRequestDetails($data['code']);

        if (!empty($requestdata)) {
            $this->load->view('Forgotpassword', $data);
        } else {
            $this->session->set_flashdata('info',"La tua richiesta è scaduta! per favore riprova."); 
            $this->load->view('info'); 
            //echo 'page not found';
        }
    }

    public function isValidCity($longitude_x, $latitude_y)
    {
        $PolyArray = [
            [
                7.82301664352417,
                44.86134450185469,
            ],
            [
                7.818017005920409,
                44.8654813478996,
            ],
            [
                7.8103673458099365,
                44.868408899379595,
            ],
            [
                7.811450958251952,
                44.87101695619395,
            ],
            [
                7.808275222778319,
                44.87305465595256,
            ],
            [
                7.812695503234863,
                44.878559125925065,
            ],
            [
                7.815957069396973,
                44.880322877836484,
            ],
            [
                7.814540863037109,
                44.884671207605415,
            ],
            [
                7.81278133392334,
                44.889262443828734,
            ],
            [
                7.79982089996338,
                44.89391411735795,
            ],
            [
                7.79853343963623,
                44.894582954508536,
            ],
            [
                7.797203063964844,
                44.894704560427456,
            ],
            [
                7.7959585189819345,
                44.894582954508536,
            ],
            [
                7.794370651245117,
                44.895190981531584,
            ],
            [
                7.793812751770019,
                44.895981407050456,
            ],
            [
                7.792525291442871,
                44.896407016291256,
            ],
            [
                7.791881561279296,
                44.89677182170468,
            ],
            [
                7.7878475189208975,
                44.89707582444798,
            ],
            [
                7.787944078445435,
                44.897683825112935,
            ],
            [
                7.7886736392974845,
                44.900480545368865,
            ],
            [
                7.789188623428345,
                44.902874369053066,
            ],
            [
                7.789853811264038,
                44.90540487428456,
            ],
            [
                7.789837718009949,
                44.905511259451956,
            ],
            [
                7.789773344993591,
                44.905798118475715,
            ],
            [
                7.7876222133636475,
                44.90821641612365,
            ],
            [
                7.784167528152466,
                44.91542321350875,
            ],
            [
                7.783110737800598,
                44.91419428482786,
            ],
            [
                7.782762050628663,
                44.913884674230516,
            ],
            [
                7.782627940177917,
                44.91376500833797,
            ],
            [
                7.782115638256073,
                44.913392712856556,
            ],
            [
                7.779961824417113,
                44.91136214046026,
            ],
            [
                7.774994373321534,
                44.90713360885984,
            ],
            [
                7.773631811141967,
                44.90599758913852,
            ],
            [
                7.773444056510925,
                44.90568983411175,
            ],
            [
                7.773256301879882,
                44.90574682590769,
            ],
            [
                7.772912979125977,
                44.90587600710262,
            ],
            [
                7.772516012191772,
                44.90579431902779,
            ],
            [
                7.7722692489624015,
                44.90578292068256,
            ],
            [
                7.769860625267029,
                44.90722099406232,
            ],
            [
                7.769351005554198,
                44.908178423231604,
            ],
            [
                7.76931881904602,
                44.90872551845398,
            ],
            [
                7.768905758857726,
                44.90901046181991,
            ],
            [
                7.768203020095824,
                44.90902185952518,
            ],
            [
                7.767237424850464,
                44.90807774194627,
            ],
            [
                7.7664434909820566,
                44.90788207781192,
            ],
            [
                7.765638828277588,
                44.9079580639626,
            ],
            [
                7.765456438064576,
                44.90834179248915,
            ],
            [
                7.765982151031493,
                44.909382785689296,
            ],
            [
                7.765778303146361,
                44.909572745918,
            ],
            [
                7.764667868614196,
                44.90999445538175,
            ],
            [
                7.764313817024231,
                44.91055672985368,
            ],
            [
                7.76358425617218,
                44.91070869498294,
            ],
            [
                7.76258647441864,
                44.91043135831954,
            ],
            [
                7.762135863304138,
                44.9098310908215,
            ],
            [
                7.761953473091125,
                44.90977410307496,
            ],
            [
                7.760918140411377,
                44.910457952304185,
            ],
            [
                7.760236859321595,
                44.91056432811968,
            ],
            [
                7.759711146354675,
                44.91054153331865,
            ],
            [
                7.7595072984695435,
                44.910777079160034,
            ],
            [
                7.759684324264526,
                44.911327948729955,
            ],
            [
                7.760478258132934,
                44.911753444369765,
            ],
            [
                7.760800123214721,
                44.91225491732972,
            ],
            [
                7.760875225067139,
                44.91258543120642,
            ],
            [
                7.760741114616394,
                44.91268040610677,
            ],
            [
                7.760625779628754,
                44.91267090862382,
            ],
            [
                7.760564088821412,
                44.91261392369301,
            ],
            [
                7.760456800460815,
                44.912575933707764,
            ],
            [
                7.760314643383025,
                44.912574034207836,
            ],
            [
                7.760137617588043,
                44.91268990358818,
            ],
            [
                7.760000824928284,
                44.912750687432,
            ],
            [
                7.759676277637482,
                44.912756385914065,
            ],
            [
                7.75933027267456,
                44.912714597032505,
            ],
            [
                7.758313715457916,
                44.91267850661031,
            ],
            [
                7.757407128810883,
                44.91338701443757,
            ],
            [
                7.756921648979187,
                44.91336991917729,
            ],
            [
                7.756401300430297,
                44.91325785012278,
            ],
            [
                7.756379842758179,
                44.913103992251315,
            ],
            [
                7.756758034229279,
                44.912877953396766,
            ],
            [
                7.756752669811249,
                44.91270130056379,
            ],
            [
                7.75649517774582,
                44.91247715962858,
            ],
            [
                7.756036520004272,
                44.91239548093486,
            ],
            [
                7.755422294139861,
                44.91246386310496,
            ],
            [
                7.755003869533538,
                44.91224162075467,
            ],
            [
                7.753965854644775,
                44.912197931986455,
            ],
            [
                7.753719091415405,
                44.91247905913171,
            ],
            [
                7.7539122104644775,
                44.9132730459396,
            ],
            [
                7.7536922693252555,
                44.91356936356424,
            ],
            [
                7.753086090087891,
                44.91361115182427,
            ],
            [
                7.7526891231536865,
                44.913223659520305,
            ],
            [
                7.752560377120972,
                44.90962213547462,
            ],
            [
                7.752270698547363,
                44.90643832186307,
            ],
            [
                7.751787900924683,
                44.90422322579446,
            ],
            [
                7.751546502113342,
                44.90425742175119,
            ],
            [
                7.7501195669174185,
                44.90403324788655,
            ],
            [
                7.748681902885436,
                44.90401045049543,
            ],
            [
                7.747507095336914,
                44.90443979984365,
            ],
            [
                7.74658441543579,
                44.90349750680474,
            ],
            [
                7.737534642219543,
                44.899241797472975,
            ],
            [
                7.739991545677185,
                44.897091024542945,
            ],
            [
                7.7319931983947745,
                44.89353409278412,
            ],
            [
                7.722058296203613,
                44.89069142932727,
            ],
            [
                7.713346481323242,
                44.88619912656871,
            ],
            [
                7.712767124176025,
                44.88769659980514,
            ],
            [
                7.711275815963744,
                44.887628188172066,
            ],
            [
                7.707241773605346,
                44.88815267527967,
            ],
            [
                7.706694602966309,
                44.88917503132946,
            ],
            [
                7.706431746482849,
                44.89047670371976,
            ],
            [
                7.706442475318909,
                44.890746535769836,
            ],
            [
                7.70655244588852,
                44.89100116485331,
            ],
            [
                7.706836760044097,
                44.89129949749458,
            ],
            [
                7.707008421421051,
                44.89138310636483,
            ],
            [
                7.706887722015381,
                44.89140210836384,
            ],
            [
                7.70638346672058,
                44.8918695555628,
            ],
            [
                7.705881893634795,
                44.89261062260426,
            ],
            [
                7.705450057983398,
                44.893064758969665,
            ],
            [
                7.7053213119506845,
                44.89310466199354,
            ],
            [
                7.7051952481269845,
                44.893068559258836,
            ],
            [
                7.7048519253730765,
                44.89295455047413,
            ],
            [
                7.704304754734039,
                44.89285764282942,
            ],
            [
                7.704092860221863,
                44.89284244161541,
            ],
            [
                7.703950703144073,
                44.89288044464286,
            ],
            [
                7.703433036804199,
                44.893193968660995,
            ],
            [
                7.702000737190247,
                44.89376020770812,
            ],
            [
                7.701657414436339,
                44.89412122969342,
            ],
            [
                7.70152598619461,
                44.89421813520888,
            ],
            [
                7.701491117477416,
                44.8943378417967,
            ],
            [
                7.701362371444701,
                44.89448794970547,
            ],
            [
                7.701099514961243,
                44.89458865479174,
            ],
            [
                7.700951993465423,
                44.894478449216535,
            ],
            [
                7.700627446174621,
                44.89437584383597,
            ],
            [
                7.6937878131866455,
                44.89551779340007,
            ],
            [
                7.6910653710365295,
                44.89695992359167,
            ],
            [
                7.691081464290618,
                44.897216425173184,
            ],
            [
                7.691738605499268,
                44.89786432407278,
            ],
            [
                7.691888809204102,
                44.89822532029265,
            ],
            [
                7.691760063171387,
                44.89850461581296,
            ],
            [
                7.691856622695923,
                44.89880480948535,
            ],
            [
                7.692291140556336,
                44.89905560299735,
            ],
            [
                7.691448926925659,
                44.89987447419604,
            ],
            [
                7.691204845905303,
                44.900600238920276,
            ],
            [
                7.690807878971099,
                44.90134119345466,
            ],
            [
                7.690451145172119,
                44.901645172039686,
            ],
            [
                7.687575817108154,
                44.90279837618227,
            ],
            [
                7.684389352798461,
                44.90123290069545,
            ],
            [
                7.683445215225219,
                44.90110370906677,
            ],
            [
                7.682425975799561,
                44.90066293544299,
            ],
            [
                7.682136297225953,
                44.90013856242037,
            ],
            [
                7.682318687438965,
                44.89925699699539,
            ],
            [
                7.683691978454589,
                44.89709482456607,
            ],
            [
                7.685424685478211,
                44.896635019945876,
            ],
            [
                7.688273191452026,
                44.89838301793006,
            ],
            [
                7.688949108123778,
                44.898200620623534,
            ],
            [
                7.690858840942383,
                44.89522898300706,
            ],
            [
                7.693927288055419,
                44.894020523788605,
            ],
            [
                7.697988152503966,
                44.890946058654706,
            ],
            [
                7.698540687561035,
                44.89128809627558,
            ],
            [
                7.699243426322937,
                44.89137170516241,
            ],
            [
                7.700037360191345,
                44.89121208809118,
            ],
            [
                7.700697183609009,
                44.8908320456624,
            ],
            [
                7.705315947532653,
                44.88880827743892,
            ],
            [
                7.706265449523927,
                44.88798924865036,
            ],
            [
                7.70628958940506,
                44.886814843633786,
            ],
            [
                7.704857289791106,
                44.885395263814914,
            ],
            [
                7.703057527542115,
                44.884789033646186,
            ],
            [
                7.701287269592284,
                44.884669307183415,
            ],
            [
                7.699769139289857,
                44.88529074182782,
            ],
            [
                7.699447274208069,
                44.88564991658851,
            ],
            [
                7.69968867301941,
                44.88614401576932,
            ],
            [
                7.700380682945251,
                44.88630934800911,
            ],
            [
                7.701984643936156,
                44.88632265058245,
            ],
            [
                7.701568901538848,
                44.88684714959412,
            ],
            [
                7.701630592346191,
                44.887647191411624,
            ],
            [
                7.700549662113189,
                44.88830659993615,
            ],
            [
                7.700327038764954,
                44.888295198123885,
            ],
            [
                7.700131237506866,
                44.88747806235875,
            ],
            [
                7.700093686580657,
                44.887050486692644,
            ],
            [
                7.69916832447052,
                44.886774936245985,
            ],
            [
                7.6989483833313,
                44.88685285064406,
            ],
            [
                7.697870135307311,
                44.88700677877886,
            ],
            [
                7.696877717971801,
                44.88696117048557,
            ],
            [
                7.696282267570496,
                44.887174008878205,
            ],
            [
                7.695981860160827,
                44.88766619464492,
            ],
            [
                7.696102559566497,
                44.88818688079448,
            ],
            [
                7.697419524192809,
                44.88950377764791,
            ],
            [
                7.695756554603577,
                44.8903721909659,
            ],
            [
                7.695603668689727,
                44.89093085693544,
            ],
            [
                7.6949143409728995,
                44.89130139769753,
            ],
            [
                7.6931494474411,
                44.89171754063041,
            ],
            [
                7.691974639892578,
                44.890493805788694,
            ],
            [
                7.69205778837204,
                44.88936315806536,
            ],
            [
                7.689560651779174,
                44.886556395297966,
            ],
            [
                7.690300941467284,
                44.88342451729211,
            ],
            [
                7.688026428222656,
                44.88506649401471,
            ],
            [
                7.685708999633788,
                44.886313148744655,
            ],
            [
                7.685494422912597,
                44.88810706789495,
            ],
            [
                7.684807777404786,
                44.88974891096302,
            ],
            [
                7.683520317077636,
                44.89066102575013,
            ],
            [
                7.680816650390624,
                44.890965060798116,
            ],
            [
                7.677769660949706,
                44.889688102796235,
            ],
            [
                7.676010131835937,
                44.88792463799442,
            ],
            [
                7.675795555114745,
                44.88576584023889,
            ],
            [
                7.679357528686523,
                44.884884054471,
            ],
            [
                7.682275772094727,
                44.882937996670314,
            ],
            [
                7.686438560485839,
                44.88135677623422,
            ],
            [
                7.686266899108887,
                44.879927558833224,
            ],
            [
                7.688755989074708,
                44.879593056016695,
            ],
            [
                7.691465020179748,
                44.8794182014977,
            ],
            [
                7.696657776832581,
                44.876943572962226,
            ],
            [
                7.696150839328765,
                44.876278332087644,
            ],
            [
                7.692875862121582,
                44.87722107116818,
            ],
            [
                7.6895713806152335,
                44.878559125925065,
            ],
            [
                7.6859235763549805,
                44.87901527384398,
            ],
            [
                7.680687904357909,
                44.87901527384398,
            ],
            [
                7.677769660949706,
                44.87968808542369,
            ],
            [
                7.676366865634917,
                44.87995036577263,
            ],
            [
                7.675001621246338,
                44.880455917274766,
            ],
            [
                7.674352526664733,
                44.88071249246563,
            ],
            [
                7.6739904284477225,
                44.880780912323296,
            ],
            [
                7.672523260116577,
                44.88092345343208,
            ],
            [
                7.672072649002075,
                44.88109070121612,
            ],
            [
                7.671375274658203,
                44.88182810610286,
            ],
            [
                7.667341232299805,
                44.88065737641003,
            ],
            [
                7.665796279907227,
                44.877890102436005,
            ],
            [
                7.664508819580078,
                44.87457528040379,
            ],
            [
                7.6662254333496085,
                44.87065198742504,
            ],
            [
                7.669100761413573,
                44.869070429349485,
            ],
            [
                7.6692214608192435,
                44.86689381502687,
            ],
            [
                7.669240236282349,
                44.866654287375646,
            ],
            [
                7.668921053409576,
                44.86570186994952,
            ],
            [
                7.666000127792358,
                44.86186922856788,
            ],
            [
                7.665506601333618,
                44.86034636718896,
            ],
            [
                7.665072083473205,
                44.859718959399395,
            ],
            [
                7.6643264293670645,
                44.85902880293432,
            ],
            [
                7.663535177707672,
                44.85814660741842,
            ],
            [
                7.66297996044159,
                44.857697899341005,
            ],
            [
                7.663173079490661,
                44.857986898164555,
            ],
            [
                7.663095295429229,
                44.858059147643786,
            ],
            [
                7.663293778896332,
                44.85811618664231,
            ],
            [
                7.663368880748749,
                44.858226461879255,
            ],
            [
                7.662985324859619,
                44.85848503746866,
            ],
            [
                7.662738561630249,
                44.859270263575056,
            ],
            [
                7.662411332130432,
                44.85991668865269,
            ],
            [
                7.662282586097718,
                44.85985965143779,
            ],
            [
                7.66221821308136,
                44.859595378271266,
            ],
            [
                7.662014365196228,
                44.859572563264756,
            ],
            [
                7.662330865859985,
                44.8592398433926,
            ],
            [
                7.662306725978851,
                44.858943245771414,
            ],
            [
                7.662043869495392,
                44.85873410550466,
            ],
            [
                7.660793960094451,
                44.859173299187475,
            ],
            [
                7.659967839717864,
                44.85934631396086,
            ],
            [
                7.659857869148255,
                44.8594584880966,
            ],
            [
                7.6598310470581055,
                44.85946609311485,
            ],
            [
                7.659530639648438,
                44.85920752193114,
            ],
            [
                7.6593080163002005,
                44.858665659434166,
            ],
            [
                7.659254372119904,
                44.85835574981912,
            ],
            [
                7.6583826541900635,
                44.857808175379034,
            ],
            [
                7.657521665096283,
                44.857772050493296,
            ],
            [
                7.656727731227874,
                44.858230264469874,
            ],
            [
                7.656196653842925,
                44.85918280550718,
            ],
            [
                7.655268609523773,
                44.85953263698161,
            ],
            [
                7.654222548007964,
                44.86127415769267,
            ],
            [
                7.6528894901275635,
                44.86356885400068,
            ],
            [
                7.65244960784912,
                44.86428176919396,
            ],
            [
                7.651245296001434,
                44.863544139448976,
            ],
            [
                7.650132179260254,
                44.862852127693714,
            ],
            [
                7.649102210998535,
                44.86214299712842,
            ],
            [
                7.648276090621948,
                44.86097947175535,
            ],
            [
                7.646881341934204,
                44.858979369922366,
            ],
            [
                7.644832134246826,
                44.85589922932069,
            ],
            [
                7.644435167312621,
                44.85260595848456,
            ],
            [
                7.642965316772461,
                44.84865834875107,
            ],
            [
                7.6425790786743155,
                44.84567654508561,
            ],
            [
                7.642021179199219,
                44.84388130313585,
            ],
            [
                7.643544673919678,
                44.84302930432995,
            ],
            [
                7.645443677902222,
                44.843740572059005,
            ],
            [
                7.645765542984009,
                44.843786214608095,
            ],
            [
                7.645905017852783,
                44.84393455264287,
            ],
            [
                7.647069096565246,
                44.84446704500575,
            ],
            [
                7.64798104763031,
                44.84467243360177,
            ],
            [
                7.650008797645569,
                44.84500333591064,
            ],
            [
                7.650834918022156,
                44.84486641104785,
            ],
            [
                7.650931477546691,
                44.844927266582594,
            ],
            [
                7.652320861816405,
                44.84385087482405,
            ],
            [
                7.651269435882568,
                44.842557656715606,
            ],
            [
                7.6502180099487305,
                44.84089925218873,
            ],
            [
                7.650411128997803,
                44.84059495259696,
            ],
            [
                7.64925241470337,
                44.83837351688732,
            ],
            [
                7.650475502014159,
                44.83572594055147,
            ],
            [
                7.651827335357665,
                44.83379343734817,
            ],
            [
                7.652685642242432,
                44.8332912802391,
            ],
            [
                7.65521764755249,
                44.83175434795825,
            ],
            [
                7.654584646224975,
                44.82598668214411,
            ],
            [
                7.654938697814941,
                44.82530943813902,
            ],
            [
                7.654509544372558,
                44.8228134589536,
            ],
            [
                7.655410766601562,
                44.82059894792716,
            ],
            [
                7.661418914794923,
                44.82059894792716,
            ],
            [
                7.6642513275146475,
                44.82193831871335,
            ],
            [
                7.674465179443359,
                44.80918259336078,
            ],
            [
                7.690762281417846,
                44.809742047393506,
            ],
            [
                7.6976823806762695,
                44.80997420019494,
            ],
            [
                7.701619863510133,
                44.81321282461497,
            ],
            [
                7.7071237564086905,
                44.817250384833656,
            ],
            [
                7.70893692970276,
                44.81725799542482,
            ],
            [
                7.710634768009186,
                44.817252287481544,
            ],
            [
                7.711498439311981,
                44.81722374775672,
            ],
            [
                7.71164059638977,
                44.81549992218463,
            ],
            [
                7.712681293487549,
                44.8150584927793,
            ],
            [
                7.714623212814331,
                44.8137494063983,
            ],
            [
                7.715224027633666,
                44.81227284307478,
            ],
            [
                7.717198133468629,
                44.811854222883184,
            ],
            [
                7.717874050140381,
                44.812021671324445,
            ],
            [
                7.7186572551727295,
                44.81293501790038,
            ],
            [
                7.719869613647462,
                44.81472361304534,
            ],
            [
                7.719939351081847,
                44.81477308403756,
            ],
            [
                7.720969319343566,
                44.81358196297344,
            ],
            [
                7.721275091171265,
                44.81297307369382,
            ],
            [
                7.72139847278595,
                44.81294643464105,
            ],
            [
                7.721532583236694,
                44.81292360115744,
            ],
            [
                7.721934914588928,
                44.812729516181925,
            ],
            [
                7.722154855728149,
                44.812497374471015,
            ],
            [
                7.722519636154175,
                44.8121396460707,
            ],
            [
                7.722691297531127,
                44.81205592208208,
            ],
            [
                7.723227739334106,
                44.81193794716459,
            ],
            [
                7.723528146743775,
                44.81199122618947,
            ],
            [
                7.723839282989502,
                44.81209397845561,
            ],
            [
                7.7240216732025155,
                44.8121396460707,
            ],
            [
                7.725132107734681,
                44.812402234155606,
            ],
            [
                7.726575136184692,
                44.81264579304967,
            ],
            [
                7.7272993326187125,
                44.81264959864922,
            ],
            [
                7.727884054183959,
                44.81258870902616,
            ],
            [
                7.728511691093445,
                44.812489763251556,
            ],
            [
                7.729514837265014,
                44.812257620575586,
            ],
            [
                7.731199264526367,
                44.811915113281714,
            ],
            [
                7.732121944427491,
                44.81171721925147,
            ],
            [
                7.733441591262817,
                44.81142037693288,
            ],
            [
                7.734106779098511,
                44.81187705679018,
            ],
            [
                7.735093832015991,
                44.81224239807239,
            ],
            [
                7.7358877658843985,
                44.81293501790038,
            ],
            [
                7.736896276473999,
                44.81344686621946,
            ],
            [
                7.736067473888398,
                44.814192748573184,
            ],
            [
                7.736029922962189,
                44.814251733755846,
            ],
            [
                7.73602455854416,
                44.81430501064319,
            ],
            [
                7.73609161376953,
                44.81435067650745,
            ],
            [
                7.736190855503083,
                44.814381120396874,
            ],
            [
                7.736373245716095,
                44.81440205056152,
            ],
            [
                7.73649662733078,
                44.81440775878692,
            ],
            [
                7.736923098564147,
                44.814474354708224,
            ],
            [
                7.737931609153748,
                44.814504798532354,
            ],
            [
                7.73874431848526,
                44.814592324437186,
            ],
            [
                7.739412188529968,
                44.8146532119448,
            ],
            [
                7.739916443824767,
                44.81471029392481,
            ],
            [
                7.740938365459442,
                44.81482636044326,
            ],
            [
                7.742016613483428,
                44.814955745795,
            ],
            [
                7.7434542775154105,
                44.81509654717149,
            ],
            [
                7.744883894920349,
                44.81524115363272,
            ],
            [
                7.745267450809478,
                44.81523544548978,
            ],
            [
                7.745578587055206,
                44.81527349976521,
            ],
            [
                7.746766805648804,
                44.815423813907536,
            ],
            [
                7.748207151889801,
                44.815596960092215,
            ],
            [
                7.748765051364898,
                44.81564833303601,
            ],
            [
                7.74935245513916,
                44.81575298148366,
            ],
            [
                7.749395370483398,
                44.81577391115039,
            ],
            [
                7.749639451503754,
                44.81577391115039,
            ],
            [
                7.749754786491395,
                44.815758689575325,
            ],
            [
                7.749864757061004,
                44.815724441016776,
            ],
            [
                7.750183939933777,
                44.81553226817196,
            ],
            [
                7.750226855278015,
                44.815494214067314,
            ],
            [
                7.750478982925414,
                44.81539146785937,
            ],
            [
                7.750575542449952,
                44.81534770553001,
            ],
            [
                7.750953733921051,
                44.81529633231838,
            ],
            [
                7.751755714416503,
                44.81530394316745,
            ],
            [
                7.752622067928313,
                44.81534770553001,
            ],
            [
                7.753281891345977,
                44.81538385702186,
            ],
            [
                7.753405272960662,
                44.815410494948715,
            ],
            [
                7.753584980964661,
                44.815591251984515,
            ],
            [
                7.753619849681855,
                44.815745370693854,
            ],
            [
                7.754086554050446,
                44.81581767315629,
            ],
            [
                7.754700779914856,
                44.81584811627126,
            ],
            [
                7.7549636363983145,
                44.815844310882774,
            ],
            [
                7.755658328533172,
                44.815945153592935,
            ],
            [
                7.756197452545165,
                44.81601555311625,
            ],
            [
                7.756369113922118,
                44.816124006267756,
            ],
            [
                7.758160829544067,
                44.81668719830098,
            ],
            [
                7.759276628494262,
                44.81857270997129,
            ],
            [
                7.759239077568053,
                44.81880482722639,
            ],
            [
                7.75984525680542,
                44.81397773755779,
            ],
            [
                7.764050960540771,
                44.81524115363272,
            ],
            [
                7.7657461166381845,
                44.81493671855561,
            ],
            [
                7.763257026672363,
                44.81761569207587,
            ],
            [
                7.763171195983887,
                44.820233659585355,
            ],
            [
                7.76484489440918,
                44.82160347893384,
            ],
            [
                7.767591476440429,
                44.823977755374855,
            ],
            [
                7.768042087554931,
                44.82761507898304,
            ],
            [
                7.777891159057617,
                44.83517815094928,
            ],
            [
                7.777419090270997,
                44.83991597833911,
            ],
            [
                7.7774110436439505,
                44.8400148770482,
            ],
            [
                7.777746319770814,
                44.840342002338026,
            ],
            [
                7.778245210647583,
                44.840768023186875,
            ],
            [
                7.778414189815521,
                44.84079845312697,
            ],
            [
                7.778491973876953,
                44.840944896988866,
            ],
            [
                7.778599262237548,
                44.84145459480068,
            ],
            [
                7.778931856155395,
                44.841736067779046,
            ],
            [
                7.779725790023804,
                44.8417969266206,
            ],
            [
                7.780852317810059,
                44.84208600523999,
            ],
            [
                7.784457206726074,
                44.84187300008212,
            ],
            [
                7.791903018951416,
                44.84057973757517,
            ],
            [
                7.795743942260741,
                44.84016893046839,
            ],
            [
                7.800228595733642,
                44.840807962479936,
            ],
            [
                7.815141677856444,
                44.8432118765634,
            ],
            [
                7.808854579925536,
                44.85495994378777,
            ],
            [
                7.809586822986603,
                44.85480783132886,
            ],
            [
                7.809986472129822,
                44.854817338369315,
            ],
            [
                7.811442911624908,
                44.85480783132886,
            ],
            [
                7.812590897083282,
                44.854399027104584,
            ],
            [
                7.815356254577636,
                44.8532904875576,
            ],
            [
                7.815522551536561,
                44.85300146516364,
            ],
            [
                7.816488146781921,
                44.85265159400923,
            ],
            [
                7.819905281066894,
                44.85688793438273,
            ],
            [
                7.820345163345337,
                44.85728721430717,
            ],
            [
                7.821326851844788,
                44.85896415975632,
            ],
            [
                7.822844982147217,
                44.86111635831383,
            ],
            [
                7.82301664352417,
                44.86134450185469,
            ],
            
            // carignano 
            [7.66721589001639,44.8505254469292],[7.66791498375373,44.8510782301628],[7.664325578271831,44.8537009109566],[7.662841820082161,44.8536790437066],[7.662610463456321,44.8535593072254],[7.662413174377061,44.8533859570163],[7.66227088665541,44.8531772425728],[7.6621976649169,44.8529918423127],[7.66220266636654,44.8527758513095],[7.662317958012791,44.8524936368919],[7.66250483407712,44.852108737149],[7.66292524755703,44.8515780378374],[7.663462411172781,44.8509406787461],[7.66386613954044,44.8504637930316],[7.664548147035281,44.849684090382],[7.66492233208894,44.8492698697914],[7.66721589001639,44.8505254469292],
[7.682662279168011,44.86872231633081],[7.683173120491961,44.8692368242694],[7.679717201685,44.8701196256315],[7.67593407842103,44.872051767023],[7.67565125449889,44.8721880186549],[7.673598729407281,44.8730869028779],[7.67276984107522,44.87209154790381],[7.672641660729091,44.8718334997876],[7.67409190461309,44.8711301862965],[7.67547894182809,44.8709962503759],[7.677741998815231,44.87020479400871],[7.67815944561213,44.8699935783625],[7.67905437434426,44.8695403297905],[7.68047691836605,44.8688141093461],[7.68115468377414,44.86847986205011],[7.68215043526365,44.86823480065],[7.682662279168011,44.86872231633081],
[7.70160336831617,44.8839456585053],[7.702638969255671,44.8840834580489],[7.704879606593931,44.8852026398627],[7.704889540966741,44.8857473747304],[7.70471443087103,44.8862855095193],[7.70450345273389,44.88632362510431],[7.70393066692603,44.886402644179],[7.703690909390671,44.8864359305854],[7.7031458409249,44.886461248233],[7.70196464806379,44.8864433255676],[7.698298186577101,44.88613602691251],[7.696260510148441,44.8859507426931],[7.69587717745787,44.8857708257849],[7.6959437126607,44.8851729513943],[7.69625437623835,44.8846633838356],[7.69668703683646,44.8842182220886],[7.69729147367638,44.8839460570218],[7.69806676576112,44.8839053885115],[7.70160336831617,44.8839456585053],
[7.596610124739921,44.8821723573616],[7.598966763158171,44.8832500339869],[7.598921446093231,44.883389009252],[7.59882109896341,44.8836758425359],[7.59868910461412,44.8839622872719],[7.59859941380633,44.8840872147791],[7.596206415984221,44.8857231924221],[7.59595587422409,44.8856931057772],[7.594121093572411,44.8823262790065],[7.594657867563611,44.8815857190632],[7.596610124739921,44.8821723573616],
[7.666685333176821,44.9334367577827],[7.65510695394616,44.9355874633288],[7.64830800875169,44.93852282993401],[7.64926337583598,44.9366122213055],[7.647056460731861,44.9296005817544],[7.643981732731081,44.9277547151434],[7.639902258227791,44.9259733179041],[7.63932262605021,44.9206867615052],[7.633944982583961,44.9126963145899],[7.625415827931361,44.9071027452439],[7.61854533484256,44.9035855686058],[7.6140266455917,44.9003531047616],[7.61202712113866,44.8979477879231],[7.600128568741331,44.88833692038611],[7.601608444156721,44.8885711026221],[7.60690359503365,44.8893604925876],[7.607553078713911,44.8891928757638],[7.608097791108071,44.8890374810928],[7.60876968401154,44.888793613379],[7.62085348589392,44.8826072891635],[7.62329281588854,44.88013866515641],[7.62625685460195,44.8764610279335],[7.624268671796471,44.8749202478217],[7.62287721524918,44.8738097269421],[7.62017910467396,44.8711755648084],[7.61972720487727,44.87068399278281],[7.61824755751615,44.8614795079476],[7.61978695663117,44.8600082864172],[7.62109633801948,44.8590068742425],[7.62138421109143,44.8587943010029],[7.62788714354793,44.85852155146871],[7.633056919491571,44.8610816299661],[7.64954820503159,44.8537341404166],[7.64997294637871,44.8533115649524],[7.65043843384074,44.8488925504459],[7.6498881046197,44.8487510134583],[7.64937280785589,44.84865039754871],[7.64870921398911,44.8487190638066],[7.64808984779798,44.8488917736543],[7.650787543722841,44.8451698167853],[7.660349406976011,44.8373740709572],[7.66815431664743,44.8414668033854],[7.66830582578308,44.841549589292],[7.668424421980841,44.8416364922149],[7.668739419403581,44.8419687412575],[7.669263625118441,44.842591494485],[7.66934209173273,44.8426869308187],[7.6694093864126,44.842786737946],[7.66945739573034,44.84289982341631],[7.66948680675777,44.8430306963467],[7.669490708384131,44.84335931782801],[7.66948151003225,44.84355275535651],[7.66944638179793,44.843687377255],[7.669332375604641,44.8440191257196],[7.669271466668911,44.8441399439487],[7.66920970163535,44.8442292449152],[7.669159419719261,44.8443006754941],[7.669017017000231,44.8444745562412],[7.6688232026417,44.8446658415861],[7.668637708048,44.8448392194123],[7.668490428768311,44.8449500280328],[7.668352759772311,44.8450384433654],[7.66822736482432,44.8451089975497],[7.66813964849309,44.8451574858197],[7.6626410510206,44.8478298475638],[7.65858606519717,44.852040261539],[7.65679800603611,44.8537971496765],[7.654326078157741,44.8551948712856],[7.65280741748202,44.8556675756946],[7.65139428671201,44.8556508939458],[7.650257307524751,44.8553763995924],[7.64951632944332,44.8560112856153],[7.649168794353971,44.85634475197381],[7.64884474305178,44.8567055013062],[7.64863706390747,44.85697760641991],[7.648521406196681,44.8572553014386],[7.648487727007551,44.8575114615148],[7.648526699317211,44.8578404979617],[7.64858829490377,44.8583183361666],[7.64881799583753,44.8588296708844],[7.6490403126517,44.8591833817311],[7.650624966800941,44.8615966666063],[7.65117617887068,44.8623323457242],[7.65230552634694,44.8626517508202],[7.66153327576773,44.8593054233036],[7.66434572311274,44.85824938236591],[7.667566509781,44.8565285382965],[7.67044583617938,44.8526830897646],[7.6688788723312,44.8510039513892],[7.66914327590718,44.8501698408851],[7.66967575799954,44.8493028449491],[7.67003206666854,44.8488388878936],[7.6707657832697,44.8484198331711],[7.67156504220681,44.8480735528352],[7.67281109931146,44.847660444423],[7.673536414115021,44.8476058582031],[7.674025603533401,44.8476565504573],[7.67755429723821,44.8497589486049],[7.677715473507681,44.8500083721441],[7.67796409940564,44.8508259385664],[7.67800208692795,44.8520866681683],[7.67822172944841,44.8592188452904],[7.67762375075436,44.8592164225634],[7.677121928734071,44.85924661800051],[7.66880370909991,44.8614752373423],[7.663237643762031,44.8637407823498],[7.66256627155784,44.8640027679346],[7.661307728838221,44.8646831473564],[7.660306067290721,44.86531413332071],[7.65901900448829,44.8663046065632],[7.65910701891968,44.8668412619436],[7.66592738930825,44.8779261445588],[7.66604872205326,44.8781031016668],[7.66631191068184,44.8782367055493],[7.66646986147835,44.8783015645502],[7.66657572429795,44.878343310009],[7.667287848928801,44.878585676715],[7.66746008158222,44.8786146929562],[7.66775880707607,44.8786316818008],[7.66806250681776,44.8786217218285],[7.66834080697177,44.8785979617462],[7.66895734258189,44.8785421362667],[7.66963832524985,44.8784195431563],[7.67119802852911,44.8781091307808],[7.67132166560473,44.8780835634293],[7.67485105209651,44.8772153732481],[7.67775493922687,44.8763938444253],[7.68070398209738,44.88091546795431],[7.69137353270163,44.8841349439228],[7.704509979296881,44.8901045456037],[7.70481365163024,44.8906976223751],[7.70486831837974,44.8914003993185],[7.70471295383183,44.8928209544777],[7.70336081407325,44.8973921294859],[7.705740353094781,44.9005113128532],[7.70916942651446,44.899636426799],[7.71209188743611,44.8988097364985],[7.712576381532321,44.8986306591914],[7.712885751040311,44.8985306239202],[7.713182227265311,44.898529464509],[7.71795375101292,44.9013332488456],[7.71951648415446,44.9029351354134],[7.719630839804391,44.9038996309901],[7.719720482598541,44.9047828313236],[7.71933669691312,44.91331240866741],[7.714696693813011,44.9321193872406],[7.71339514840121,44.9322172534225],[7.70956564404923,44.93021611462531],[7.70470194681393,44.92795556578271],[7.70455923069847,44.9279449447255],[7.703917364429841,44.9279781696025],[7.70290294075007,44.9280431671545],[7.702523653350751,44.928074865699],[7.70209594577918,44.9281465214699],[7.70182481935393,44.9282109539499],[7.69855201277341,44.92922242555791],[7.69210800443259,44.93743058745771],[7.68465743024891,44.9339377672552],[7.683536083124741,44.93365028487671],[7.67977937285861,44.9328687506979],[7.67975017746892,44.9328639121579],[7.67782692502174,44.9325761006653],[7.6766941724489,44.9324774599293],[7.6757258850607,44.9324257252093],[7.6753159166894,44.93241196778331],[7.67465235163172,44.9324042675457],[7.666685333176821,44.9334367577827],
        ];
        $vertices_x = [];
        $vertices_y = [];
        foreach ($PolyArray as $Poly) {
            array_push($vertices_x, $Poly[0]);
            array_push($vertices_y, $Poly[1]);
        }
        $points_polygon = count($vertices_x) - 1;

        return true; // torna sempre
        
        
        if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)) {
            return true;
        } else {
            return false;
        }
    }

    public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
    {
        $i = $j = $c = 0;
        for ($i = 0, $j = $points_polygon - 1; $i < $points_polygon; $j = $i++) {
            if ((($vertices_y[$i] > $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
                ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]))) {
                $c = !$c;
            }

        }
        return $c;
    }

}