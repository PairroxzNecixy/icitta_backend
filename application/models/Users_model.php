<?php
class Users_model extends CI_Model
{
    public function getUserbyemail($data)
    {
        $user = $this->db
            ->from('users')
            ->where(['email' => $data['email']])
            ->get();
        return $user->row_array();
    }

    
       public function updateCity($userdata)
    {
    if(       
        $this->db
            ->set('city', $userdata['city'])
            ->where(['user_id' => $userdata['user_id']])
            ->update('user_profiles')) {
           
        if( $this->db
            ->set('city', $userdata['city'])
            ->where(['id' => $userdata['user_id']])
            ->update('users')) return json_encode($this->getUserByUserId($userdata['user_id']));
    
           else return false;
    }
    else return false;    
    }
    
    
    public function getUserByUserId($user_id)
    {
        $user = $this->db
            ->from('users')
            ->where(['id' => $user_id])
            ->get();
        return $user->row_array();
    }

    public function getUserType($user)
    {
        $userType = $this->db->from('user_types')->where('id', $user['user_type_id'])->get();
        return $userType->row_array();
    }
    public function insertAuth($authdata)
    {
        return $this->db->insert('authentication_keys', $authdata);
    }
    public function getlastuserid()
    {
        $lastuserid = $this->db->select('id')->from('users')
            ->order_by('id', 'DESC')
            ->get();
        return $lastuserid->first_row();
    }
    public function genauthkey()
    {
        //microtime().
        $auth_key = rand(00000, 99999) . uniqid();
        return $auth_key;
    }
    public function getAuthUserId($authData)
    {
        $authusers = $this->db
            ->select('user_id')
            ->from('authentication_keys')
            ->where(['user_id' => $authData['user_id'], 'device_id' => $authData['device_id']])
            ->get();
        return $authusers->result();
    }
    public function insertAuthdata($authData)
    {
        $addauthdata = $this->db->insert('authentication_keys', $authData);
        return $addauthdata;
    }
    public function updateAuthdata($authData)
    {
        return $this->db->set('auth_key', $authData['auth_key'])
            ->where(['user_id' => $authData['user_id'], 'device_id' => $authData['device_id']])
            ->update('authentication_keys');
    }

    public function insertUser($data)
    {
        if ($this->db->insert('users', $data)) {
            return $this->db->insert_id();
        } else {

            return false;
        }
    }
    public function getuserdetails($user_id)
    {
        return $this->db->from('users')->where('id', $user_id)->get()->row_array();
    }

    public function getBusinessProfiles($user_id)
    {
        $url = base_url();
        //$business_id = $this->db->select('user_id')->from('business_profiles')->where('user_id', $user_id)->get()->row_array();
        $business_profile = $this->db->select("business_profiles.name,business_profiles.id, CONCAT('" . $url . "uploads/userProfile/', business_profiles.image) as image, CONCAT('" . $url . "uploads/userProfile/', business_profiles.banner_image) as banner_image ,business_profiles.address,
                                    business_profiles.about_us,business_profiles.is_activated,business_profiles.city, users.user_type_id,
                                    business_profiles.lat, business_profiles.lng, users.email, users.mobile")
            ->from('business_profiles')
            ->join('users', 'users.id=business_profiles.user_id', 'left')
            ->where('business_profiles.user_id', $user_id)
            ->get()
            ->first_row();
        return $business_profile;
        exit;
    }
    public function getUsersProfiles($user_id)
    {

        $url = base_url();
        $user_profile = $this->db->select("user_profiles.name,user_profiles.id, CONCAT('" . $url . "uploads/userProfile/', user_profiles.image) as image ,user_profiles.address,
                                user_profiles.city, users.user_type_id,
                                user_profiles.lat, user_profiles.lng, users.email, users.mobile")
            ->from('user_profiles')
            ->join('users', 'users.id=user_profiles.user_id', 'left')
            ->where('user_profiles.user_id', $user_id)
            ->get()
            ->first_row();
        return $user_profile;
    }

    public function getUserTypeAll()
    {
        return $this->db->from('user_types')->get()->result();
    }

    public function insertBusinessprofiles($business)
    {
        $this->db->insert('business_profiles', $business);
    }
    public function insertUserprofiles($user)
    {
        $this->db->insert('user_profiles', $user);
    }
    public function insertSponsorprofiles($sponsor)
    {
        $this->db->insert('sponsor_profiles', $sponsor);
    }

    public function deleteauthUserId($data)
    {
        return $this->db->where(['auth_key' => $data['auth_key']])
            ->delete('authentication_keys');
    }

    public function getFBLoginUser($user_id)
    {
        $id_token = $this->db->select('user_id')
            ->from('social_logins')
            ->where('user_id', $user_id)->get();
        return $id_token->first_row();
    }
    public function insertFBdata($data)
    {
        return $this->db->insert('social_logins', $data);
    }
    public function updateFBdata($data)
    {
        return $this->db->set('token_key', $data['token_key'])
            ->where(['user_id' => $data['user_id']])
            ->update('social_logins');
    }
    public function getUserPassword($data)
    {
        $userpass = $this->db->select('password')
            ->from('users')
            ->where('id', $data['user_id'])
            ->get();
        return $userpass->row_array();
    }
    // public function sendemail($data)
    // {
        
    //     $config['protocol'] = 'smtp'; // 'mail', 'sendmail', or 'smtp'
    //     $config['smtp_host'] = 'pairroxz.com';
    //     $config['smtp_port'] = 465;
    //     $config['smtp_user'] = 'amit.kumar@pairroxz.com';
    //     $config['smtp_pass'] = 'Queenqueen1';
    //     $config['smtp_crypto'] = 'ssl'; //can be 'ssl' or 'tls' for example
    //     $config['mailtype'] = 'html'; //plaintext 'text' mails or 'html'
    //     $config['smtp_timeout'] = '4'; //in seconds
    //     $config['charset'] = 'iso-8859-1';
    //     $config['wordwrap'] = true;

    //     $this->load->library('email', $config);

    //     $this->email->from($config['smtp_user'], $data['email'].'');
    //     $this->email->to($data['email']);

    //     $this->email->subject('Icitta');
    //     $this->email->message('Clicca sul link per resettare la password per account: ' . $data['link'] . '?code=' . $data['code']); 
 


    //     if ($this->email->send()) {
    //         return true;
    //         // $message = $this->email->print_debugger();
    //         // print_r($message);
    //     }
    // }
    public function sendemail($data)
    {
       
        $config['protocol'] = 'mail'; // 'mail', 'sendmail', or 'smtp'
        $config['smtp_host'] = 'n3plcpnl0143.prod.ams3.secureserver.net';
        $config['smtp_port'] = 587;
        $config['smtp_user'] = 'admin@xn--icitt-vqa.com';
        $config['smtp_pass'] = 'icitta_admin';
        $config['smtp_crypto'] = 'tls'; //can be 'ssl' or 'tls' for example
        $config['mailtype'] = 'html'; //plaintext 'text' mails or 'html'
        $config['smtp_timeout'] = '4'; //in seconds
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = true;

        $this->load->library('email', $config);

        $this->email->from($config['smtp_user'], $data['email'].'');
        $this->email->to($data['email']);

        $this->email->subject('Icitta');
        $url = $data['link'] . '?code=' . $data['code'];
        //$this->email->message('Clicca sul link per resettare la password per account: ' . $data['link'] . '?code=' . $data['code']);  
           $this->email->message("Clicca sul link per resettare la password per account: <a href=\"$url\">$url</a>");  

        if ($this->email->send()) {
            return true;
            //$message = $this->email->print_debugger();
            //print_r($message);
        }else{
            return false;
        }
    }

    public function insertForgotQuery($data)
    {
        return $this->db->insert('forgot_password_requests', $data);
    }

    public function getRequestDetails($code)
    {
        $user = $this->db
            ->from('forgot_password_requests')
            ->where(['code' => $code])
            ->get();
        return $user->row_array();
    }
    public function updatepass($userdata)
    {
        return $this->db
            ->set('password', $userdata['password'])
            ->where(['id' => $userdata['user_id']])
            ->update('users');
    }
    public function deleterequestquery($userdata)
    {
        return $this->db
            ->where(['user_id' => $userdata['user_id']])
            ->delete('forgot_password_requests');
    }
    public function countUserType()
    {
        return $this->db->count_all('user_types');
    }
    public function getcategories()
    {
        return $this->db->count_all('categories');
    }
    public function saveImage($_file, $path)
    {
        if ((empty($_FILES[$_file])) || ($_FILES[$_file]['error'])) {
            return ["error" => true, "msg" => "image is required", "image" => ''];
        }
        $file_size = $_FILES[$_file]['size'];

        if ($file_size == 0 || $file_size > 10485760) {
            return ["error" => true, "msg" => "File size problem.", "image" => ''];
            log_message("error", "image not uploaded, File size problem.");
        }

        // if(!exif_imagetype($_FILES[$_file]["tmp_name"])){
        //     return ["error" => TRUE, "msg" => "File is not an image. Please select an image.", "image" => $old_image];
        // }
        $allowed_image_extension = array(
            "png", "PNG",
            "jpg", "JPG",
            "jpeg", "JPEG",
            // "webp", "WEBP",
        );
        $file_extension = pathinfo($_FILES[$_file]["name"], PATHINFO_EXTENSION);
        if (!in_array($file_extension, $allowed_image_extension)) {
            return ["error" => true, "msg" => "Upload valiid images. image type not allowed.", "image" => ''];
        }

        $path_parts = pathinfo($_FILES[$_file]["name"]);
        $extension = $path_parts['extension'];
        $file_name = md5(microtime()) . "." . strtolower($extension);
        $destiny = $path . $file_name;

        if (move_uploaded_file($_FILES[$_file]["tmp_name"], FCPATH . $destiny)) {
            return ["error" => false, "msg" => "", "image" => $file_name];
        } else {
            return ["error" => true, "msg" => "Could not save image.", "image" => ''];
        }
    }
}