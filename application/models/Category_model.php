<?php 
class Category_model extends CI_Model
{
    public function getcategoriesByEnglish()
    {
        $url = base_url();
        return $this->db->select("id, name, CONCAT('".$url."uploads/categories/', categories.image) as image ")
                        ->from('categories')
                        ->order_by('name','ASC')
                        ->get()
                        ->result();
    }

    public function getcategoriesByItalian()
    {
        $url = base_url();
        return $this->db->select("id, name_italian as name, CONCAT('".$url."uploads/categories/', categories.image) as image ")
                        ->from('categories')
                        ->order_by('name_italian','ASC')
                        ->get()
                        ->result();
    }
    public function getCities()
    {
        return $this->db->select('*')
                        ->from('cities')
                        ->order_by('name','ASC')
                        ->get()
                        ->result();
    }
    public function getCitiesById($id)
    {
        return $this->db->select('*')
                        ->from('cities')
                        ->where('id',$id)
                        ->get()
                        ->first_row();
    }
    public function updatePlayeridByUserId($auth_key,$player_id)
    {
       return $this->db->set('player_id', $player_id)
                ->where('auth_key', $auth_key)
                ->update('authentication_keys');
        
    }
}
?>