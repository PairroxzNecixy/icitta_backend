<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Qrcode_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }
    public function getCouponByuser($data)
    {
        
        return $this->db->from('coupons')->where(['user_id'=>$data['user_id'], 'QR_code'=>$data['QR_code']])
                    ->get()->first_row();
    }
    public function updateCouponByQRcode($coupons, $data)
    { 
        $verified_count = $coupons->verified_count+1;
        
        $this->db->set(['verified_count'=> $verified_count])
            ->where(['user_id'=>$data['user_id'], 'QR_code'=>$data['QR_code']])
            ->update('coupons');
        return $this->db->select('verified_count')->from('coupons')
            ->where(['user_id'=>$data['user_id'], 'QR_code'=>$data['QR_code']])
            ->get()
            ->first_row();
    }
    public function updateCouponForScan($count, $data)
    {
        
        $this->db->set(['scan_count'=> $count+1])
            ->where(['QR_code'=>$data['QR_code']])
            ->update('coupons');
        return $this->db->select('scan_count')->from('coupons')
                        ->where(['QR_code'=>$data['QR_code']])
                        ->get()
                        ->first_row();
    }
    public function getCouponsByQrcode($QR_code)
    {
        return $this->db->select('scan_count')->from('coupons')->where('QR_code', $QR_code)
                    ->get()->first_row();
    }
}

/* End of file Response.php */
