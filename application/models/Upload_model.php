<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Upload_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function upload_image($_file, $path)
	{
		$config['upload_path']          = $path;
		$config['allowed_types']        = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';
		$config['encrypt_name']			= true;
		$config['file_ext_tolower']		= true;
		// $config['max_size']             = 2048;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload($_file)) {
			return ['error' => $this->upload->display_errors(), "data" => []];
		} else {
			return ["error" => false, "data" => $this->upload->data()];
		}
	}
}

/* End of file Upload_model.php */
