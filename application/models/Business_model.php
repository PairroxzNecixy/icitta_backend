<?php 
class Business_model extends CI_Model{

    public function updateBusinessById($data)
    {
        return $this->db
            ->where('user_id', $data['user_id'])
            ->update('business_profiles', $data);
    }

    public function updateSocialLinks($data)
    {
        return $this->db
            ->where('user_id', $data['user_id'])
            ->update('business_profiles', $data);
           
            
    }


    public function getBusinessProfileByUserId($business)
    {
        $url = base_url();
        $businessprofile = $this->db->select("business_profiles.id,business_profiles.user_id,business_profiles.name as business_name, CONCAT('".$url."uploads/userProfile/', business_profiles.image) as business_image, CONCAT('".$url."uploads/userProfile/', business_profiles.banner_image) as banner_image,
                business_profiles.city, business_profiles.address,business_profiles.lat,business_profiles.lng , categories.name as category_name, business_profiles.about_us, business_profiles.is_activated,business_profiles.facebook_link,business_profiles.insta_link,business_profiles.website_link,
                users.mobile,users.email")
                ->from('business_profiles') 
                ->join('categories','categories.id=business_profiles.category_id','left')
                ->join('users', 'users.id=business_profiles.user_id', 'left')
                ->where('business_profiles.user_id', $business['user_id'])
                ->get()
                ->first_row();   
           return $businessprofile;
    }

    public function getcouponByUserId($business)
    {
        $savedcoup = $this->db->select('coupon_id')
                        ->from('save_coupons') 
                        ->where(['user_id' => $business['user_id']])
                        ->get()
                        ->result();
        $savecouponid =  array_column($savedcoup, 'coupon_id');

        $favcoup = $this->db->select('coupon_id')
                        ->from('favourite_coupons') 
                        ->where(['user_id' => $business['user_id']])
                        ->get()
                        ->result();
        $favcouponid =  array_column($favcoup, 'coupon_id');
        $url = base_url();
        date_default_timezone_set("UTC");
        $date = date('Y-m-d H:i:s');
        $coupons = $this->db->select("coupons.name, coupons.id, CONCAT('".$url."uploads/coupons/', coupons.image) as image ,coupons.value, UNIX_TIMESTAMP(CONVERT_TZ(coupons.apply_date, '+00:00', @@session.time_zone)) as apply_date, UNIX_TIMESTAMP(CONVERT_TZ(coupons.expire_date, '+00:00', @@session.time_zone)) as expire_date,UNIX_TIMESTAMP(CONVERT_TZ(coupons.release_date, '+00:00', @@session.time_zone)) as release_date, coupons.description, coupon_types.name as coupon_type_name, 
                categories.name as category_name, coupons.is_approved, CONCAT('0') as favourite, CONCAT('0') as save " )
                ->from('coupons') 
                ->join('coupon_types','coupons.coupon_type_id=coupon_types.id','left')	
                ->join('categories','coupons.category_id=categories.id','left')
                ->where(['coupons.user_id'=>$business['user_id']])
                ->where(['coupons.expire_date >=' => $date])
                ->order_by('coupons.create_at', 'DESC')
                ->get()
                ->result();
                foreach ($coupons as &$row)
                {
                    $row->curr_timestamp = strtotime($date);

                    if(array_search($row->id, $savecouponid) > -1)
                    {
                        $row->save = 1;
                    }else{
                        $row->save = 0;
                    }

                    if(array_search($row->id, $favcouponid) > -1)
                    {
                        $row->favourite = 1;
                    }else{
                        
                        $row->favourite = 0;
                    } 
                    if(empty($row->release_date)){
                      $row->release_date=0;
            }
                }

               return $coupons;
    }
    public function updateBusiness($data)
    {
        return $this->db
            ->where('user_id', $data['user_id'])
            ->update('business_profiles', $data);
    }
    public function getImageName($user_id)
    {
        return $this->db->select('banner_image')->from('business_profiles')->where('user_id',$user_id)->get()->first_row();
    }
    public function getBusinessDetailsNotification($data)
    {
        return $this->db->select('user_id, business_id, is_notification_enabled as is_enabled')
                    ->from('coupon_notifications')->where('business_id', $data)
                    ->get()->result();
    }
    public function getBusinessById($data)
    {
        return  $this->db->select('is_activated')
                    ->from('business_profiles')
                    ->where('user_id', $data)
                    ->get()->first_row();
    }
    
     public function getShopWindowUserId($business){
        // return print_r(date("yy-m-d"));
         
        $url = base_url();
        $user = $this->db->select("*,CONCAT('" . $url . "uploads/shopwindow/', image) as image")
        ->from('shop_window')
        ->where(['business_profile_id' => $business->id])
        ->where("expiration_date >=","".date("yy-m-d"))
        ->get();
        return $user->result_array();
    }
}
?>