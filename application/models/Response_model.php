<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Response_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }
    public function apiresponse($status = "500", $message = '',$data = NULL)
    {
        if(empty($data))
        {
            $data = (object)[];
        }
        header('Content-type: application/json; charset=UTF-8');
        die(json_encode(['status' => $status, 'message' => $message, 'data' => $data]));
    }
}

/* End of file Response.php */
