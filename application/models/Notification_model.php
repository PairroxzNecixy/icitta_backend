<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Notification_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('coupons_model');
        $this->load->model('business_model');
        //Do your magic here
    }
    public function getPlayerIdByUsersId($data)
    {
        $this->db->select('player_id')->from('authentication_keys');

        $this->db->where_in('user_id', $data);

        return $this->db->get()
            ->result();
    }
    public function getUsersForSendNotification($business_id)
    {
        return $this->db->select('user_id')->from('coupon_notifications')
            ->where(['business_id' => $business_id, 'is_notification_enabled'=>1])
            ->get()->result();
    }

    public function updateEnabledNotification($data, $is_enabled)
    {
        $this->db->set('is_notification_enabled', $is_enabled)
            ->where(['business_id' => $data['business_id'], 'user_id' => $data['user_id']])
            ->update('coupon_notifications');
        return $this->db->select('is_notification_enabled')
            ->from('coupon_notifications')
            ->where(['business_id' => $data['business_id'], 'user_id' => $data['user_id']])
            ->get()->first_row();
    }

    // public function getUserIdByBusinessId($business_id)
    // {
    //     return $this->db->select("user_profiles.name,coupon_notifications.is_notification_enabled, 
    //                     coupon_notifications.business_id, coupon_notifications.user_id
    //                     ")
    //             ->from('coupon_notifications') 
    //             ->join('user_profiles','user_profiles.user_id=coupon_notifications.user_id','left')
    //             ->where('coupon_notifications.business_id', $business_id)
    //             ->get()
    //             ->result();
    // }
    public function getBusinessIdByBusinessId($user_id)
    {
        return $this->db->select("business_profiles.name,coupon_notifications.is_notification_enabled, 
                                coupon_notifications.business_id, coupon_notifications.user_id 
                                ")
            ->from('coupon_notifications')
            ->join('business_profiles', 'business_profiles.user_id=coupon_notifications.business_id', 'left')
            ->where('coupon_notifications.user_id', $user_id)
            ->get()
            ->result();
    }


    public function sendAppprovedNotification($coupon_id)
    {
        $coupon_details =  $this->coupons_model->getCouponIdByCouponId($coupon_id);
        $users = $this->getUsersForSendNotification($coupon_details->user_id);

        $user_ids = array_column($users, 'user_id');

        if(empty($user_ids)){
            return false;
        }

        $player_ids = $this->getPlayerIdByUsersId($user_ids);
        if (empty($player_ids)) {
            return false;
        }

        $business = $this->business_model->getBusinessProfileByUserId(["user_id" => $coupon_details->user_id]);
        if (empty($business)) {
            return false;
        }

        $content = [
            "en" => $business->business_name . " has created a new coupon.",
            "it" => $business->business_name . " ha creato un nuovo coupon." 
        ];
        
        //data = ["coupon_id" => $coupon_id]; 
         
        $data = ["coupon_id" => $coupon_id,"business_id"=>$business->user_id]; 
        $player_ids = array_column($player_ids, 'player_id');

       // json_encode($player_ids);
      
        $player_ids = array_values((array)array_diff($player_ids, array('')));
        log_message("error", "player_ids: " . json_encode($player_ids));
         if (empty($player_ids)) {
            return false;
        }
        return $this->sendNotification($player_ids, $content, $data);
    }




    function sendNotification($player_ids, $content, $data = [])
    {
        // $this->form_validation->set_rules(
        //     'auth_key',
        //     'Auth key',
        //     'required',
        //     array(
        //         'required' => $this->lang->line('req_auth_key')
        //     )
        // );
        // if ($this->form_validation->run() == FALSE) {
        //     $error = $this->form_validation->error_array();
        //     $message = reset($error);
        //     return $this->response_model->apiresponse('500', $message);
        // }
        // if (empty($this->user)) {
        //     return $this->response_model->apiresponse('500', $this->lang->line('not_auth'));
        // }

        if (empty($player_ids)) {
            return false;
        }

        $fields = array(
            'app_id'                =>  'aad2420a-d7fa-4909-b857-b924391eda6f',
            'include_player_ids'    =>  $player_ids,
            'data'                  =>  $data,
            'contents'              =>  $content
        );

        $fields = json_encode($fields);
        // print("\nJSON sent:\n");
        // print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        log_message("error", "PUSH_NOTIFICATION_RESPONSE: " . json_encode($response));

        return true;
    }
}

/* End of file Response.php */
