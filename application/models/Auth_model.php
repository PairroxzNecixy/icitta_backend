<?php
class Auth_model extends CI_Model
{
    public function authenticateUser($auth_key)
    {
        $user = $this->db->select('user_id')->from('authentication_keys')
                        ->where('auth_key', $auth_key)->get()->row_array();
                       
            if(!empty($user))
            {
                return $this->db->from('users')->where('id', $user['user_id'])->get()->row_array();
            }else{
                return 0;
            }
    }

    public function selectLang($user_id)
    {
        $lang = $this->db->select('language')->from('users')->where('id', $user_id)->get();
        return $lang->row_array();
    }

    public function logout($auth_key)
    {
        return $this->db->where(['auth_key' => $auth_key])
                    ->delete('authentication_keys');
    }

    public function changePass($data)
    {
        $pass = password_hash($data['new_password'], PASSWORD_DEFAULT);
        return $this->db->set('password', $pass)
            ->where('id', $data['user_id'])
            ->update('users');
    }

    public function updateUserById($user, $data, $userdata)
    {
        $this->db
            ->where('id', $user['id'])
            ->update('users', $userdata);
        $mobile = $this->db->select('mobile')->from('users')
                ->where('id', $user['id'])->get()->first_row();
        $this->db
            ->set(['name'=> $data['name']])
            ->where('user_id', $user['id'])
            ->update('user_profiles');
        $name = $this->db->select('name')->from('user_profiles')
            ->where('user_id', $user['id'])->get()->first_row();
            

        return array('mobile'=>$mobile->mobile, 'name'=> $name->name);
    }
    public function updateBusinessById($user, $data, $userdata)
    {
        $this->db
            
            ->where('id', $user['id'])
            ->update('users', $userdata);
        $mobile = $this->db->select('mobile')->from('users')
                ->where('id', $user['id'])->get()->first_row();
        $this->db
            ->set(['name'=> $data['name']])
            ->where('user_id', $user['id'])
            ->update('business_profiles');
        $name = $this->db->select('name')->from('business_profiles')
            ->where('user_id', $user['id'])->get()->first_row();
            
        return array('mobile'=>$mobile->mobile, 'name'=> $name->name);
    }
    public function updateBusinessImage($data)
    {
        return $this->db->set('image', $data['image'])
            ->where('user_id', $data['user_id'])
            ->update('business_profiles');
    }
    public function updateuserImage($data)
    {
        return $this->db->set('image', $data['image'])
            ->where('user_id', $data['user_id'])
            ->update('user_profiles');
    }
    
    public function emailVerification($data)
    {
        $user = $this->db->from('users')
            ->where('email', $data['email'])
            ->get()
            ->row_array();
            return $user;
    }
    public function saveForgotPasswordRequest($data)
    {
        
        return $this->db->insert('forgot_password_requests', $data);
        
    }
    public function updateAddress($data)
    {
        
        switch ($data['user_type']) 
        {
            case 1:
                $this->db->set(['lat' => $data['lat'], 'lng' => $data['lng'], 'city' => $data['city'], 'address'=>$data['address']])
                    ->where('user_id', $data['user_id'])
                    ->update('business_profiles');
                return $this->db->select('lat,lng,address,city')->from('business_profiles')
                        ->where('user_id', $data['user_id'])->get()->first_row();
            case 2:
                $this->db->set(['lat' => $data['lat'], 'lng' => $data['lng'], 'city' => $data['city'], 'address'=>$data['address']])
                    ->where('user_id', $data['user_id'])
                    ->update('user_profiles');
                return $this->db->select('lat,lng,address,city')->from('user_profiles')
                    ->where('user_id', $data['user_id'])->get()->first_row();
            case 3:
                return $this->db->set(['lat' => $data['lat'], 'lng' => $data['lng'], 'city' => $data['city'], 'address'=>$data['address']])
                    ->where('user_id', $data['user_id'])
                    ->update('sponsor_profiles');
                return $this->db->select('lat,lng,address,city')->from('sponsor_profiles')
                    ->where('user_id', $data['user_id'])->get()->first_row();
            
        }
    }
    public function updatelaguage($data)
    {
        $this->db->set(['language' => $data['language']])
                ->where('id', $data['user_id'])
                ->update('users');
        return  $this->db->affected_rows();
    }

    public function updateplayerId($data)
    {
        $this->db->set(['player_id' => $data['player_id']])
                ->where('id', $data['user_id'])
                ->update('users');
        return  $this->db->affected_rows();
    }
    
}
