<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Oauth extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
        $ex = $this->db->where([
            'provider'  =>  $data['provider'],
            'id_token'  =>  $data['id_token']
        ])->get('oauth')->first_row();
        if ($ex != NULL) {
            return $this->db->where("id", $ex->id)->update('oauth', $data);
        } else {
            return $this->db->insert('oauth', $data);
        }
    }

    public function ex_oauth($data)
    {
        return $this->db->where([
            'provider'  =>  $data['provider'],
            'id_token'   =>  $data['id_token']
        ])->get('oauth')->first_row();
    }
}

/* End of file Oauth.php */
