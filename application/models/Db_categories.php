<?php

class Db_categories extends BASE_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCategories()
    {
        return  $this->db->get('categories')->result();
	}
	
    public function getCategoryById($category_id)
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('id', $category_id);
        $category = $this->db_categories->get_one();
        return $category;
	}
	
	public function add_new($data)
	{
		return $this->db->insert("categories", $data);
	}
}
