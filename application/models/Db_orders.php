<?php

class Db_orders extends BASE_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function createOrders($data)
	{
		return $this->db->insert('orders', $data);
    }
   public function  getAllOrders($user_id,$for_business){
    $user = $this->db->from('orders');
    if($for_business){
        $user = $user->where(['business_id' => $user_id]);
    }else{
        $user = $user->where(['user_id' => $user_id]);
    }
    // $user->get();
    return $user->get()->result_array();
    }
    public function getOrderById($id){
       
        $user = $this->db->select("*")
        ->from('orders')
        ->where(['id' => $id])
        ->get();
        return $user->row_array();
    }
    public function updateOrderById($order_id,$new_order_state){
        $this->db->set("state",$new_order_state);
        $this->db->where(['id' => $order_id]);
        return $this->db->update('orders');
    }
    public function getBusinessDetails($data)
    {
        $url = base_url();
        $business = $this->db->select("business_profiles.name as business_name,business_profiles.user_id as business_id, CONCAT('" . $url . "uploads/userProfile/', users.image) as business_image, business_profiles.city, users.mobile, business_profiles.address,business_profiles.lat,business_profiles.lng, business_profiles.is_activated")
            ->from('orders')
            ->join('business_profiles', 'orders.user_id=business_profiles.user_id', 'left')
            ->join('users', 'orders.user_id=users.id', 'left')
            ->where(['orders.id' => $data['id']])
            ->get()
            ->first_row();
        return $business;
    }
    
    public function getOrderById2($order_id){
        $window = $this->db->select('id, user_id')->from('orders')->where('id', $order_id)
        ->get()->first_row();
        // print_r($window);
        // die;
        return $window;   
    }

    public function deleteOrderById($data)
    {
        $this->db->where(['id' => $data['order_id'], 'user_id' => $data['user_id']])
            ->delete('orders');
        return $this->db->affected_rows();
    }
}