<?php
class Coupons_model extends CI_Model
{
    public function createCoupons($data)
    {
        $this->db->insert('coupons', $data);
        $coupon_id = $this->db->insert_id();
        if (!empty($coupon_id)) {
            $url = base_url();
            $Result = $this->db->select("coupons.name,coupons.id, CONCAT('" . $url . "uploads/coupons/',coupons.image) as image ,coupons.value,UNIX_TIMESTAMP(CONVERT_TZ(coupons.apply_date, '+00:00', @@session.time_zone)) as apply_date, UNIX_TIMESTAMP(CONVERT_TZ(coupons.expire_date, '+00:00', @@session.time_zone)) as expire_date,UNIX_TIMESTAMP(CONVERT_TZ(coupons.release_date, '+00:00', @@session.time_zone)) as release_date, coupons.description, coupons.is_approved, coupon_types.name as coupon_type, categories.name as category, users.name as username,coupons.PriceFrom as price_from,coupons.PriceTo as price_to,coupons.FixPrice as fix_price,coupons.shoudOrder as shoudOrder,coupons.package as package,coupons.tribute as tribute")
                ->from('coupons')
                ->join('coupon_types', 'coupons.coupon_type_id=coupon_types.id', 'left')
                ->join('categories', 'coupons.category_id=categories.id', 'left')
                ->join('users', 'coupons.user_id=users.id', 'left')
                ->where('coupons.id', $coupon_id)
                ->get()
                ->first_row();
            if (empty($Result->release_date)) {
                $Result->release_date = 0;
            }
            return $Result;
        } else {

            return false;
        }
    }
    public function updateCoupons($data)
    {
        return $this->db->set(['QR_image' => $data['QR_image'], 'QR_code' => $data['QR_code']])
            ->where('id', $data['id'])->update('coupons');
    }
    public function validForCreateCouponUser($user_id)
    {
        $date = date('Y-m-d H:i:s');
        $expirecoupon = $this->db
            ->from('coupons')
            ->where(['user_id' => $user_id, 'expire_date >=' => $date])
            ->count_all_results();

        if ($expirecoupon < 5) {
            return true;
        } else {

            return false;
        }
    }

    public function getcoupondefault($data)
    {
        $savedcoup = $this->db->select('coupon_id')
            ->from('save_coupons')
            ->where(['user_id' => $data['user_id']])
            ->get()
            ->result();
        $savecouponid = array_column($savedcoup, 'coupon_id');

        $favcoup = $this->db->select('coupon_id')
            ->from('favourite_coupons')
            ->where(['user_id' => $data['user_id']])
            ->get()
            ->result();
        $favcouponid = array_column($favcoup, 'coupon_id');

        $url = base_url();
        $coupons = $this->db->select("coupons.name,coupons.id, CONCAT('" . $url . "uploads/coupons/', coupons.image) as image ,coupons.value, UNIX_TIMESTAMP(coupons.apply_date) as apply_date, UNIX_TIMESTAMP(coupons.expire_date) as expire_date, coupons.description,
                        coupon_types.name as coupon_type_name, categories.name as category_name, users.name as username , CONCAT('0') as favourite, CONCAT('0') as save, coupons.is_approved,coupons.PriceFrom as price_from,coupons.PriceTo as price_to,coupons.FixPrice as fix_price,coupons.shoudOrder,,coupons.package,coupons.tribute")
            ->from('coupons')
            ->join('coupon_types', 'coupons.coupon_type_id=coupon_types.id', 'left')
            ->join('categories', 'coupons.category_id=categories.id', 'left')
            ->join('users', 'coupons.user_id=users.id', 'left')
            ->order_by('coupons.create_at', 'DESC')
            ->get()
            ->result();
        foreach ($coupons as &$row) {
            // print_r(array_search($row->id, $savecouponid));
            // exit;
            if (array_search($row->id, $savecouponid) > -1) {
                $row->save = 1;
            } else {
                $row->save = 0;
            }

            if (array_search($row->id, $favcouponid) > -1) {
                $row->favourite = 1;
            } else {

                $row->favourite = 0;
            }
        }

        return $coupons;
    }

    public function getCouponDetails($data)
    {
        $url = base_url();
        $coupons = $this->db->select("coupons.name, coupons.id, CONCAT('" . $url . "uploads/coupons/', coupons.image) as image ,coupons.value, UNIX_TIMESTAMP(CONVERT_TZ(coupons.apply_date, '+00:00', @@session.time_zone)) as apply_date, UNIX_TIMESTAMP(CONVERT_TZ(coupons.expire_date, '+00:00', @@session.time_zone)) as expire_date,UNIX_TIMESTAMP(CONVERT_TZ(coupons.release_date, '+00:00', @@session.time_zone)) as release_date, coupons.description, coupon_types.name as coupon_type_name,
                                        categories.name as category_name, CONCAT('" . $url . "uploads/QR_images/', coupons.QR_image) as QR_code, coupons.is_approved,coupons.PriceFrom as price_from,coupons.PriceTo as price_to,coupons.FixPrice as fix_price,coupons.shoudOrder as shoudOrder,coupons.package as package,coupons.tribute as tribute")
            ->from('coupons')
            ->join('coupon_types', 'coupons.coupon_type_id=coupon_types.id', 'left')
            ->join('categories', 'coupons.category_id=categories.id', 'left')
            ->where(['coupons.id' => $data['coupon_id']])
            ->get()
            ->first_row();

        date_default_timezone_set("UTC");
        $date = date('Y-m-d H:i:s');
        $coupons->curr_timestamp = strtotime($date);
        if (empty($coupons->release_date)) {
            $coupons->release_date = 0;
        }
        if (!empty($coupons)) {
            $savedcoup = $this->db->select('coupon_id')
                ->from('save_coupons')
                ->where(['user_id' => $data['user_id'], 'coupon_id' => $data['coupon_id']])
                ->get()
                ->first_row();

            if (!empty($savedcoup)) {
                $coupons->save = 1;
            } else {
                $coupons->save = 0;
            }
            $favcoup = $this->db->select('coupon_id')
                ->from('favourite_coupons')
                ->where(['user_id' => $data['user_id'], 'coupon_id' => $data['coupon_id']])
                ->get()
                ->result();

            if (!empty($favcoup)) {
                $coupons->favourite = 1;
            } else {

                $coupons->favourite = 0;
            }
        }
        return $coupons;
    }
    public function getBusinessDetails($data)
    {
        $url = base_url();
        $business = $this->db->select("business_profiles.name as business_name,business_profiles.user_id as business_id, CONCAT('" . $url . "uploads/userProfile/', users.image) as business_image, business_profiles.city, users.mobile, business_profiles.address,business_profiles.lat,business_profiles.lng, business_profiles.is_activated")
            ->from('coupons')
            ->join('business_profiles', 'coupons.user_id=business_profiles.user_id', 'left')
            ->join('users', 'coupons.user_id=users.id', 'left')
            ->where(['coupons.id' => $data['coupon_id']])
            ->get()
            ->first_row();
        return $business;
    }
    public function FavoriteCouponstatus($data)
    {
        return $this->db->from('favourite_coupons')
            ->where(['user_id' => $data['user_id'], 'coupon_id' => $data['coupon_id']])
            ->get()
            ->first_row();
    }
    public function makeFavoriteCoupon($data)
    {
        $coupon = $this->db->from('favourite_coupons')
            ->where(['user_id' => $data['user_id'], 'coupon_id' => $data['coupon_id']])
            ->get()
            ->first_row();
        if (empty($coupon)) {
            $this->db->insert('favourite_coupons', $data);
            return true;
        } else {

            $this->db->where(['user_id' => $data['user_id'], 'coupon_id' => $data['coupon_id']])
                ->delete('favourite_coupons');
            return false;
        }
    }

    public function makeSavecoupon($data)
    {
        $coupon = $this->db->from('save_coupons')
            ->where(['user_id' => $data['user_id'], 'coupon_id' => $data['coupon_id']])
            ->get()
            ->first_row();
        if (empty($coupon)) {
            $this->db->insert('save_coupons', $data);
            return true;
        } else {

            $this->db->where(['user_id' => $data['user_id'], 'coupon_id' => $data['coupon_id']])
                ->delete('save_coupons');
            return false;
        }
    }

    public function saveCouponstatus($data)
    {
        return $this->db->from('save_coupons')
            ->where(['user_id' => $data['user_id'], 'coupon_id' => $data['coupon_id']])
            ->get()
            ->first_row();
    }
    public function getFavoritecoupon($data)
    {
        $savedcoup = $this->db->select('coupon_id')
            ->from('save_coupons')
            ->where(['user_id' => $data['user_id']])
            ->get()
            ->result();
        $savecouponid = array_column($savedcoup, 'coupon_id');

        $url = base_url();
        date_default_timezone_set("UTC");
        $date = date('Y-m-d H:i:s');
        $favcoupons = $this->db->select("business_profiles.user_id as business_id,business_profiles.name as business_name,coupons.name, coupons.id, CONCAT('" . $url . "uploads/coupons/', coupons.image) as image ,coupons.value,UNIX_TIMESTAMP(CONVERT_TZ(coupons.apply_date, '+00:00', @@session.time_zone)) as apply_date, UNIX_TIMESTAMP(CONVERT_TZ(coupons.expire_date, '+00:00', @@session.time_zone)) as expire_date,UNIX_TIMESTAMP(CONVERT_TZ(coupons.release_date, '+00:00', @@session.time_zone)) as release_date, coupons.description, coupon_types.name as coupon_type_name,
                    categories.name as category_name, CONCAT('0') as is_notification_enabled, CONCAT('1') as favourite , CONCAT('0') as save,coupons.is_approved,coupons.shoudOrder,coupons.package as package,coupons.tribute as tribute")
            ->from('coupons')
            ->join('coupon_types', 'coupons.coupon_type_id=coupon_types.id', 'left')
            ->join('business_profiles', 'coupons.user_id=business_profiles.user_id', 'left')
            ->join('categories', 'coupons.category_id=categories.id', 'left')
            ->join('favourite_coupons', 'coupons.id=favourite_coupons.coupon_id', 'left')
            ->where(['favourite_coupons.user_id' => $data['user_id']])
            ->order_by('favourite_coupons.create_at', 'DESC')
            ->where('coupons.expire_date >=', $date)
            ->get()
            ->result();
        // print_r($favcoupons);
        // exit;
        foreach ($favcoupons as &$row) {
            $row->favourite = 1;
            $row->curr_timestamp = strtotime($date);
            if (array_search($row->id, $savecouponid) > -1) {
                $row->save = 1;
            } else {
                $row->save = 0;
            }

            $isNotificationEnabled_dt = $this->db->select('is_notification_enabled')
                ->from('coupon_notifications')
                ->where(['user_id' => $data['user_id']])
                ->where(['business_id' => $row->business_id])
                ->get()
                ->result();
            $isNotificationEnabled = array_column($isNotificationEnabled_dt, 'is_notification_enabled');
            if (!empty($isNotificationEnabled)) {
                $row->is_notification_enabled = $isNotificationEnabled[0];
            }

            if (empty($row->release_date)) {
                $row->release_date = 0;
            }
        }
        return $favcoupons;
    }

    public function getsavecoupondetails($data)
    {
        $favcoup = $this->db->select('coupon_id')
            ->from('favourite_coupons')
            ->where(['user_id' => $data['user_id']])
            ->get()
            ->result();
        $favcouponid = array_column($favcoup, 'coupon_id');
        // print_r($favcouponid);
        // exit();

        $url = base_url();
        $savecoupons = $this->db->select("coupons.name, coupons.id, CONCAT('" . $url . "uploads/coupons/', coupons.image) as image ,coupons.value, UNIX_TIMESTAMP(coupons.apply_date) as apply_date, UNIX_TIMESTAMP(coupons.expire_date) as expire_date, coupons.description, coupon_types.name as coupon_type_name,
                    categories.name as category_name, CONCAT(1) as save , CONCAT('0') as favourite, coupons.is_approved,coupons.shoudOrder,coupons.package as package,coupons.tribute as tribute")
            ->from('coupons')
            ->join('coupon_types', 'coupons.coupon_type_id=coupon_types.id', 'left')
            ->join('categories', 'coupons.category_id=categories.id', 'left')
            ->join('save_coupons', 'coupons.id=save_coupons.coupon_id', 'left')
            ->where(['save_coupons.user_id' => $data['user_id']])
            ->order_by('save_coupons.create_at', 'DESC')
            ->get()
            ->result();
        // print_r($savecoupons);
        // exit;
        foreach ($savecoupons as &$row) {
            $row->save = 1;
            if (array_search($row->id, $favcouponid) > -1) {
                $row->favourite = 1;
            } else {

                $row->favourite = 0;
            }
        }

        return $savecoupons;
    }

    public function getcouponsbyfilter($data, $category_id)
    {
        $savedcoup = $this->db->select('coupon_id')
            ->from('save_coupons')
            ->where(['user_id' => $data['user_id']])
            ->get()
            ->result();
        $savecouponid = array_column($savedcoup, 'coupon_id');

        $favcoup = $this->db->select('coupon_id')
            ->from('favourite_coupons')
            ->where(['user_id' => $data['user_id']])
            ->get()
            ->result();
        $favcouponid = array_column($favcoup, 'coupon_id');

        date_default_timezone_set("UTC");
        $date = date('Y-m-d H:i:s');

        $url = base_url();
        $this->db->select("coupons.name, coupons.id, CONCAT('" . $url . "uploads/coupons/', coupons.image) as image ,coupons.value, UNIX_TIMESTAMP(CONVERT_TZ(coupons.apply_date, '+00:00', @@session.time_zone)) as apply_date, UNIX_TIMESTAMP(CONVERT_TZ(coupons.expire_date, '+00:00', @@session.time_zone)) as expire_date,UNIX_TIMESTAMP(CONVERT_TZ(coupons.release_date, '+00:00', @@session.time_zone)) as release_date, coupons.description, coupon_types.name as coupon_type_name,
                categories.name as category_name, CONCAT('0') as is_notification_enabled, business_profiles.user_id as business_id, business_profiles.name as business_name, CONCAT('0') as favourite, CONCAT('0') as save, coupons.is_approved, CONCAT('" . $url . "uploads/QR_images/', coupons.QR_image) as QR_code,coupons.shoudOrder,coupons.package as package,coupons.tribute as tribute")
            ->from('coupons')
            ->join('coupon_types', 'coupons.coupon_type_id=coupon_types.id', 'left')
            ->join('categories', 'coupons.category_id=categories.id', 'left')
            ->join('business_profiles', 'coupons.user_id=business_profiles.user_id', 'left');

        $g_flag = false;

        if (!empty($category_id[0])) {

            $this->db->group_start();
            $g_flag = true;

            $this->db->where_in('coupons.category_id', $category_id);
        }
        if (!empty($data['coupon_type'][0])) {
            if (!$g_flag) {
                $this->db->group_start();
                $g_flag = true;
            }

            $this->db->where_in('coupons.coupon_type_id', $data['coupon_type']);
        }
        if (!empty($data['business_name'])) {

            // if (!$g_flag) {
            //     $this->db->group_start();
            //     $g_flag = true;
            // }
            $this->db->group_start();
            $this->db->like(['business_profiles.name' => $data['business_name']]);
            $this->db->or_like(['coupons.name' => $data['business_name']]);
            $this->db->group_end();
        }

        if ($g_flag) {
            $this->db->group_end();
        }

        $this->db->where(['coupons.expire_date >=' => $date, 'is_approved' => 1]);
        if($data['user_city']){
             $this->db->where(['coupons.city' => $data['user_city']]);
        }

        $coupons = $this->db->order_by('coupons.create_at', 'DESC')

            ->get()
            ->result();
        foreach ($coupons as &$row) {
            // print_r(array_search($row->id, $savecouponid));
            // exit;
            $row->curr_timestamp = strtotime($date);
            if (array_search($row->id, $savecouponid) > -1) {
                $row->save = 1;
            } else {
                $row->save = 0;
            }

            if (array_search($row->id, $favcouponid) > -1) {
                $row->favourite = 1;
            } else {

                $row->favourite = 0;
            }

            $isNotificationEnabled_dt = $this->db->select('is_notification_enabled')
                ->from('coupon_notifications')
                ->where(['user_id' => $data['user_id']])
                ->where(['business_id' => $row->business_id])
                ->get()
                ->result();
            $isNotificationEnabled = array_column($isNotificationEnabled_dt, 'is_notification_enabled');
            if (!empty($isNotificationEnabled)) {
                $row->is_notification_enabled = $isNotificationEnabled[0];
            }

            if (empty($row->release_date)) {
                $row->release_date = 0;
            }
        }

        return $coupons;
    }
    public function getbannerCoupons()
    {
        $url = base_url();
        return $this->db->select("id,business_name,link, CONCAT('" . $url . "uploads/bannerImage/', image) as image")
            ->where("delete_at", null)
            ->from('banner_coupons')
            ->order_by("priority", "ASC")
            ->get()->result();
    }

    public function getbusinessbyfilter($data, $category_id)
    {
        $url = base_url();
        $this->db->select("business_profiles.id, business_profiles.name, business_profiles.category_id, business_profiles.user_id, business_profiles.lat,business_profiles.lng, CONCAT('" . $url . "uploads/userProfile/', business_profiles.image) as image, CONCAT('" . $url . "uploads/userProfile/', business_profiles.banner_image)as banner_image,business_profiles.city,business_profiles.address,business_profiles.about_us,business_profiles.is_activated")
            ->from('business_profiles')
            ->join('coupons', 'business_profiles.user_id=coupons.user_id', 'left')
            ->where("business_profiles.is_activated", 1);

        if (!empty($category_id['0'])) {
            $this->db->where_in('business_profiles.category_id', $category_id);
        }
        if($data['city'] != ''){
            $this->db->where('business_profiles.city', $data['city']);
        }
        if (!empty($data['business_name'])) {
            $this->db->group_start();
            $this->db->like('business_profiles.name', $data['business_name'], '%');
            $this->db->or_like('coupons.name', $data['business_name'], '%');
            $this->db->group_end();
        }
        $business = $this->db->order_by('business_profiles.create_at', 'DESC')
            ->group_by("business_profiles.id")
            ->get()
            ->result();
        return $business;
    }

    public function getcategories()
    {
        return $this->db->count_all('categories');
    }
    public function getcoupontype()
    {
        return $this->db->count_all('coupon_types');
    }
    public function getcategoryIdByBusiness($user_id)
    {
        return $this->db->select('category_id')->from('business_profiles')->where('user_id', $user_id)
            ->get()->first_row();
    }
    public function getCouponIdByCouponId($coupon_id)
    {
        $coupon = $this->db->select('id, user_id')->from('coupons')->where('id', $coupon_id)
            ->get()->first_row();
        return $coupon;
    }
    public function deleteCouponByCouponId($data)
    {
        $this->db->where(['id' => $data['coupon_id'], 'user_id' => $data['user_id']])
            ->delete('coupons');
        return $this->db->affected_rows();
    }
    public function getDataByBusinessIdAndUserId($data)
    {
        return $this->db->from('coupon_notifications')
            ->where(['business_id' => $data['business_id'], 'user_id' => $data['user_id']])
            ->get()
            ->first_row();
    }

    public function insertbusinessIdAndUserId($data)
    {
        return $this->db->insert('coupon_notifications', $data);
        //return  $this->db->affected_rows();
    }
    public function getenabledStatus($data)
    {
        return $this->db->select('is_notification_enabled')
            ->from('coupon_notifications')
            ->where(['business_id' => $data['business_id'], 'user_id' => $data['user_id']])
            ->get()
            ->first_row();
    }
     public function getCouponByCouponId($coupon_id)
    {
        $url = base_url();   
        $coupon = $this->db->select('*,CONCAT("' . $url . 'uploads/coupons/", image) as image')->from('coupons')->where('id', $coupon_id)
            ->get()->first_row();
        return $coupon;
    }
}