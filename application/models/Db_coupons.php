<?php

class Db_coupons extends BASE_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function total($type = null)
	{
		if (!empty($type)) {
			$this->db->where("coupon_type_id", $type);
		}
		return $this->db->where("delete_at", NULL)
			->from("coupons")
			->count_all_results();
	}

	public function total_percentage_coupons()
	{
		return $this->db->where("delete_at", NULL)
			->from("coupons")
			->count_all_results();
	}

	public function getCoupons()
	{
		$this->db->select("*, coupons.image, coupons.id, coupons.name, coupon_types.name as coupon_type_name, business_profiles.name as business_name");
		$this->db->join("coupon_types", "coupons.category_id = coupon_types.id", "LEFT");
		$this->db->join("business_profiles", "coupons.user_id = business_profiles.user_id", "LEFT");
		$this->db->order_by("coupons.create_at", "DESC");
		return $this->db->get("coupons")->result_array();
	}

	public function getRecentCoupons()
	{
		$this->db->limit(10)
			->order_by("coupons.create_at", "DESC")
			->select("*, coupons.image, coupons.id, coupons.name, coupon_types.name as coupon_type_name, business_profiles.name as business_name")
			->join("coupon_types", "coupons.category_id = coupon_types.id", "LEFT")
			->join("business_profiles", "coupons.user_id = business_profiles.user_id", "LEFT");
		return $this->db->get("coupons")->result_array();
	}

	public function getSponsorCoupons()
	{
		return $this->db->where("delete_at", null)
			->order_by("banner_coupons.priority", "ASC")
			->get('banner_coupons')->result();
	}

	public function getCouponById($coupon_id)
	{
		return $this->db->where('coupons.id', $coupon_id)
			->select("*, coupons.image, coupons.id, coupons.name, coupon_types.name as coupon_type_name, business_profiles.name as business_name")
			->join("coupon_types", "coupons.category_id = coupon_types.id", "LEFT")
			->join("business_profiles", "coupons.user_id = business_profiles.user_id", "LEFT")
			->get('coupons')->first_row();
	}

	public function getCouponsByUserId($user_id)
	{
		return $this->db->order_by("coupons.create_at")
			->select("*, coupons.image, coupons.id, coupons.name, coupon_types.name as coupon_type_name, business_profiles.name as business_name")
			->where("coupons.user_id", $user_id)
			->join("coupon_types", "coupons.category_id = coupon_types.id", "LEFT")
			->join("business_profiles", "coupons.user_id = business_profiles.user_id", "LEFT")
			->get('coupons')->result();
	}

	public function getSponsorCouponById($coupon_id)
	{
		return $this->db->where("id", $coupon_id)
			->get('banner_coupons')->first_row();
	}

	public function add_new_banner_coupon($data)
	{
		return $this->db->insert('banner_coupons', $data);
	}

	public function update_new_banner_coupon($couponId, $data)
	{
		return $this->db->where("id", $couponId)->update('banner_coupons', $data);
	}

	public function approveCoupon($couponId)
	{
		return $this->db->where("id", $couponId)
			->update("coupons", ["is_approved" => 1]);
	}
	public function unApproveCoupon($couponId)
	{
		return $this->db->where("id", $couponId)
			->update("coupons", ["is_approved" => 0]);
	}


	public function deleteSponsoredCoupon($couponId)
	{
		return $this->db->where("id", $couponId)
			->update("banner_coupons", ["delete_at" => date("Y-m-d H:i:s")]);
	}
	public function update_coupon($id, $data)
	{
		return $this->db->where("id", $id)
			->update("coupons", $data);
	}


	public function deleteCoupon($coupon_id)
	{

		$this->db->trans_strict(true);
		$this->db->trans_begin();

		$this->db->where('coupons.id', $coupon_id);
		$this->db->delete('coupons');

		$this->db->where('favourite_coupons.coupon_id', $coupon_id);
		$this->db->delete('favourite_coupons');

		$this->db->where('save_coupons.coupon_id', $coupon_id);
		$this->db->delete('save_coupons');

		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			return false;
		}

		$this->db->trans_commit();
		return true;
	}
	public function totalVerifiedCoupons()
	{
		return $this->db->where("verified_count>", 0)
			->from("coupons")
			->count_all_results();
	}
	public function totalNoOfTimesVerifiedCoupons()
	{
		return $this->db->select("sum(verified_count) as total")
			->from("coupons")
			->where("verified_count>", 0)
			->get()
			->first_row()->total;
			
	}
	
	public function getCouponTypes(){
		return $this->db
			->select("*")
			->get('coupon_types')->result_array();
	}
}
