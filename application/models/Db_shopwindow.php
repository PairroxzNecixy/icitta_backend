<?php

class Db_shopwindow extends BASE_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function createShopWindow($data)
	{
		return $this->db->insert('shop_window', $data);
    }
   public function  getAllShopWindow($city){
       $url = base_url();
        $user = $this->db->select("shop_window.*,CONCAT('" . $url . "uploads/shopwindow/', shop_window.image) as image,business_profiles.name")
        ->from('shop_window')
        ->where(['shop_window.city' => $city])->
        join("business_profiles","shop_window.business_profile_id = business_profiles.id")
        ->get();
    return $user->result_array();
    }
    public function getBusinessById($Business_id){
         $user = $this->db->select("*")
        ->from('business_profiles')
        ->where(['id' => $Business_id])
        ->get();

        return $user->row();
    }
    public function getShopWindowById($id){
       
        $user = $this->db->select("shop_window.*,CONCAT('".base_url('uploads/shopwindow/')."',shop_window.image) as image,business_profiles.name")
        ->from('shop_window')
        ->where(['shop_window.id' => $id])->
        join("business_profiles","shop_window.business_profile_id = business_profiles.id")
        ->get();
        return $user->row_array();
    }
    public function getBusinessDetails($data)
    {
        $url = base_url();
        $business = $this->db->select("business_profiles.user_id as business_user_id,business_profiles.name as business_name,business_profiles.id as business_id, CONCAT('" . $url . "uploads/userProfile/', business_profiles.image) as business_image, business_profiles.city, users.mobile, business_profiles.address,business_profiles.lat,business_profiles.lng, business_profiles.is_activated")
            ->from('shop_window')
            ->join('business_profiles', 'shop_window.business_profile_id=business_profiles.id', 'left')
            ->join('users', 'business_profiles.user_id=users.id', 'left')
            ->where(['shop_window.id' => $data['id']])
            ->get()
            ->first_row();
        return $business;
    }
    
     public function getWindowById($window_id){
        $window = $this->db->select('id, business_profile_id')->from('shop_window')->where('id', $window_id)
        ->get()->first_row();
        return $window;   
    }

    public function deleteWindowById($data,$window)
    {
        $res = $this->db->select("id")->from("business_profiles")->where(["id" => $window->business_profile_id,"user_id" => $data['user_id']])->get()->row_array();
        if($res){
            
            $this->db->where(['id' => $data['window_id']])
                ->delete('shop_window');
            return $this->db->affected_rows();
        }else{
            return 0; 
        }  
    }
    
    public function getWindowByFilter($data)
    {
        date_default_timezone_set("UTC");
        $date = date('Y-m-d H:i:s');
        $url = base_url();
        $this->db->select("shop_window.*, CONCAT('" . $url . "uploads/shopwindow/', shop_window.image) as image ,UNIX_TIMESTAMP(CONVERT_TZ(shop_window.start_date, '+00:00', @@session.time_zone)) as start_date, UNIX_TIMESTAMP(CONVERT_TZ(shop_window.expiration_date, '+00:00', @@session.time_zone)) as expiration_date,
                business_profiles.id as business_profile_id, business_profiles.name as business_name")
            ->from('shop_window')
            ->join('business_profiles', 'shop_window.business_profile_id=business_profiles.id', 'full');
        if (!empty($data['business_name'])) {
            $this->db->group_start();
            $this->db->like(['business_profiles.name' => $data['business_name']]);
            $this->db->or_like(['shop_window.title' => $data['business_name']]);
            $this->db->group_end();
        }
        $this->db->where(['shop_window.expiration_date >=' => $date]);
        if($data['city_id']){
            $this->db->where('shop_window.city',$data['city_id']);
        }
        $coupons = $this->db->order_by('shop_window.id', 'DESC')
            ->get()
            ->result();
        return $coupons;
    }
    
     public function getBusinessProfileById($business)
    {
        $url = base_url();
        $businessprofile = $this->db->select("business_profiles.id,business_profiles.user_id,business_profiles.name as business_name, CONCAT('" . $url . "uploads/userProfile/', business_profiles.image) as business_image, CONCAT('" . $url . "uploads/userProfile/', business_profiles.banner_image) as banner_image,
                business_profiles.city, business_profiles.address,business_profiles.lat,business_profiles.lng , categories.name as category_name, business_profiles.about_us, business_profiles.is_activated,business_profiles.facebook_link,business_profiles.insta_link,business_profiles.website_link,
                users.mobile,users.email")
            ->from('business_profiles')
            ->join('categories', 'categories.id=business_profiles.category_id', 'left')
            ->join('users', 'users.id=business_profiles.user_id', 'left')
            // ->join("cities","business_profiles.city = cities.id")
            ->where('business_profiles.id', $business)
            ->get()
            ->first_row();
        return $businessprofile;
    }
}