<?php

class Db_users extends BASE_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function total($user_type = null)
	{
		if(!empty($user_type)){
			$this->db->where("user_type_id", $user_type);
		}
		return $this->db->where("delete_at", NULL)
						->from("users")
						->count_all_results();
	}

	public function getUsers()
	{
		$this->db->select('*, users.id');
		$this->db->from('users');
		$this->db->join("user_types", "users.user_type_id=user_types.id", "LEFT");
		$this->db->order_by("users.create_at", "DESC");
		$users = $this->db->get()->result_array();
		return $users;
	}

	public function getGeneralUsers()
	{
		$this->db->select('*, users.id');
		$this->db->from('users');
		$this->db->where('user_types.slag', 'user');
		$this->db->join("user_types", "users.user_type_id=user_types.id", "LEFT");
		$this->db->order_by("users.create_at", "DESC");
		$users = $this->db->get()->result_array();
		return $users;		
	}

	public function getSponsorUsers()
	{
		$this->db->select('*, users.id, sponsor_profiles.name,  sponsor_profiles.city');
		$this->db->from('users');
		$this->db->where('user_types.slag', 'sponsor');
		$this->db->join("user_types", "users.user_type_id=user_types.id", "LEFT");
		$this->db->join("sponsor_profiles", "users.id=sponsor_profiles.user_id", "LEFT");
		$this->db->order_by("users.create_at", "DESC");
		$users = $this->db->get()->result_array();
		return $users;
	}

	public function getBusinessUsers()
	{
		$this->db->select('*, users.id');
		$this->db->from('users');
		$this->db->where('user_types.slag', 'business');
		$this->db->join("user_types", "users.user_type_id=user_types.id", "LEFT");
		$this->db->order_by("users.create_at", "DESC");
		$users = $this->db->get()->result_array();
		return $users;		
	}

	public function getUserById($user_id)
	{
		$this->db->select('*, users.id, user_types.slag as user_type_slug');
		$this->db->from('users');
		$this->db->where('users.id', $user_id);
		$this->db->join("user_types", "users.user_type_id=user_types.id", "LEFT");
		$user = $this->db_users->get_one();
		return $user;
	}

	public function getBusinessByUserId($user_id)
	{
		return $this->db->where('user_id', $user_id)
					->get("business_profiles")->row_array();
	}

	public function getSponsorByUserId($user_id){
		return $this->db->where('user_id', $user_id)
					->get("sponsor_profiles")->row_array();
	}

	public function getUserByEmail($email)
	{
		return $this->db->where('email', $email)
			->get('users')->first_row();
	}

	public function getAdminByEmail($email)
	{
		return $this->db->where('email', $email)
			->get('admin_users')->first_row();
	}

	public function approveUser($user_id)
	{
		 $this->db->where("id", $user_id)
			->update("users", ["is_actived" => 1]);
		return 	$this->db->where("user_id", $user_id)
			->update("business_profiles", ["is_activated" => 1]);
	}
	public function unApproveUser($user_id)
	{
		$this->db->where("id", $user_id)
			->update("users", ["is_actived" => 0]);
		return $this->db->where("user_id", $user_id)
			->update("business_profiles", ["is_activated" => 0]);
	}
	 public function deleteUser($user_id, $user_type)
    {
        log_message("error","user delete test: ".$user_id);
        
        $this->db->trans_strict(true);
        $this->db->trans_begin();

        $this->db->where('authentication_keys.user_id', $user_id);
        $this->db->delete('authentication_keys');

        $this->db->where('coupons.user_id', $user_id);
        $this->db->delete('coupons');

        $this->db->where('favourite_coupons.user_id', $user_id);
        $this->db->delete('favourite_coupons');

        $this->db->where('forgot_password_requests.user_id', $user_id);
        $this->db->delete('forgot_password_requests');

        $this->db->where('save_coupons.user_id', $user_id);
        $this->db->delete('save_coupons');

        $this->db->where('oauth.user_id', $user_id);
        $this->db->delete('oauth');
        
        $this->db->where('social_logins.user_id', $user_id);
        $this->db->delete('social_logins');

        $this->db->where('coupon_notifications.user_id', $user_id);
        $this->db->delete('coupon_notifications');

        if ($user_type == 1) {
            $this->db->where('business_profiles.user_id', $user_id);
            $this->db->delete('business_profiles');
        } else if ($user_type == 2) {
            $this->db->where('user_profiles.user_id', $user_id);
            $this->db->delete('user_profiles');
        }

        $this->db->where('users.id', $user_id);
        $this->db->delete('users');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
        return true;
    }


}
