<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class BASE_Controller extends CI_Controller
{

	public $data;
	public function __construct($page = false)
	{
		parent::__construct();
		$this->data['page'] = $page;
		$this->data['Message'] = "";
	}


	function redirect_back()
	{
		if (isset($_SERVER['HTTP_REFERER'])) {
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		} else {
			header('Location: '.$_SERVER['REQUEST_SCHEME'].'://' . $_SERVER['SERVER_NAME']);
		}
		exit;
	}
}
