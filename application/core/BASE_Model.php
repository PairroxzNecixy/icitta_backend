<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class BASE_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert($field, $tableName) {
        if (!isset($tableName))
            die($tableName . " is not a valid table.");
        $this->db->insert($tableName, $field);
        return ($this->db->affected_rows() > 0) ? true : false;
    }

    public function get() {
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : null;
    }

    public function select($tableName, $conditions = null, $select = "*") {
        $this->db->select($select);
        $this->db->from($tableName);
        (is_null($conditions)) ? '' : $this->db->where($conditions);
        return $this->get();
    }

    public function update($field, $tableName, $conditions) {
        if (!isset($tableName))
            die($tableName . " is not a valid table.");
        return $this->db->where($conditions)->update($tableName, $field);
    }

    public function delete($tableName, $conditions) {
        if (!isset($tableName))
            die($tableName . " is not a valid table.");
        return $this->db->where($conditions)->delete($tableName);
    }

    public function get_one() {
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : null;
    }

}
