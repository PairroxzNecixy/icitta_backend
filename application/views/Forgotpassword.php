<html>
<head>
<title></title>
</head>
   <body>
    <div class="wrapper" style="width:100%;height:100%;display:flex;flex-direction:column;align-items:center;justify-content:center;">
        <div style="margin: 0 auto;border-radius:10px;text-align:center;width:100%;max-width: 350px;padding:25px;box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(235, 33, 46, 0.4);" class=""> 
            <h2 align="center" style="margin-top:0;">
                <img style="max-width: 50px;margin:0 auto;" src="<?php echo base_url('assets/images/ic_logo.png'); ?>" />
            </h2>
            <h2 style="margin:0 auto;color:#e21e25;margin-bottom:20px;"> 
                Recupero Password 
            </h2>
            <form method="POST" action= "<?php echo base_url().'users/updatepassword';?>">
                <h5 style="color:#e21e25;"><?php echo $this->session->flashdata('message');?> </h5>
                <table align="center" style="width:100%; text-align: center;">
                    <tr>
                         <td style="text-align:left; font-size:16px;text-transform: capitalize;">nuova password:</td>
                    </tr>

                    <tr>
                        <td><input style="margin: 8px 0 12px;width:100%;height:37px;border:1px #ccc solid; text-align: center;" type='password' name='password', placeholder='nuova password'></td> 
                    </tr>

                    <tr>
                         <td style="text-align:left;text-transform: capitalize;">conferma password:</td> 
                    </tr>

                    <tr>
                        <td><input style="margin: 8px 0 12px;width:100%;height:37px;border:1px #ccc solid; text-align: center;" type='password' name='confirm_password', placeholder='conferma password'></td> 
                    </tr>

                    <tr>
                        
                        <td><input type='hidden' name='code' value='<?php echo $code; ?>'>
                         <input type='submit' name='submit' value='Aggiorna' style=" background: #e21e25; border: 0;color: #fff; 
                            text-transform: uppercase;margin: 15px 0 0;padding: 12px 0px;width: 130px;display: inline-block;
                            font-size: 16px;font-weight: 600; cursor: pointer;"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
   </body>
</html>