<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | Dashboard</title>
	<?php include __DIR__ . '/layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<div class="wrapper">
		<?php include __DIR__ . '/layout/sidemenu.php'; ?>

		<div class="app-content content">
			<div class="content-wrapper">
				<div class="content-header row">
				</div>
				<div class="content-body">
					<div class="row">
						<div class="col-lg-3 col-sm-6 d-flex">
							<div class="card usser_box w-100">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2 media-body">
											<div class="icon"><i class="fa fa-tags" aria-hidden="true"></i></div>
											<h5>All Coupons
												<span><?php echo $total_coupons ?></span>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-sm-6 d-flex">
							<div class="card usser_box second_box w-100">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2  media-body">
											<div class="icon"><i class="fa fa-percent" aria-hidden="true"></i></div>
											<h5>Coupons (%)
												<span> <?php echo $total_percentage_coupons ?></span>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-sm-6 d-flex">
							<div class="card usser_box new_bg w-100">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2  media-body">
											<div class="icon"><i class="fa fa-cube" aria-hidden="true"></i></div>
											<h5>Coupons <br>(Box)
												<span> <?php echo $total_box_coupons ?></span>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-sm-6 d-flex">
							<div class="card usser_box promotion_bg w-100">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2  media-body">
											<div class="icon"><i class="fa fa-handshake-o" aria-hidden="true"></i></div>
											<h5>Coupons<br> (Promotions)
												<span><?php echo $total_promotion_coupons ?></span>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>

					<!-- <div class="col-lg-3 col-sm-6 d-flex">
							<div class="card usser_box third_box w-100">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2 media-body">
											<div class="icon"><i class="fa fa-university" aria-hidden="true"></i></div>
											<h5> Coupons <br>(Sponsored)
												<span><?php echo $total_sponsor_coupons ?></span>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> -->
					<div class="col-lg-3 col-sm-6 d-flex">
							<div class="card usser_box third_box w-100">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2  media-body">
											<div class="icon"><i class="fa fa-cube" aria-hidden="true"></i></div>
											<h5> Coupons <br>(Scanned)
											<span><?php echo $totalNoOfTimesVerifiedCoupons ?></span>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- <div class="col-lg-3 col-sm-6 d-flex">
							<div class="card usser_box third-box w-100">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2  media-body">
											<div class="icon"><i class="fa fa-handshake-o" aria-hidden="true"></i></div>
											<h5> Coupons <br>(Scanned)
											<span> <?php echo $total_verified_coupons ?></span>
											</h5>
										</div>
									</div>
								</div>
							</div>
						</div> -->

					<div class="row match-height">
						<div class="col-lg-12 col-md-12">
							<div class="card pb-2">
								<div class="card-header">
									<h4>Recent Coupons</h4>

								</div>
								<div class="card-content">
									<!-- <div class="card-body">
                    <p>Total paid invoices 240, unpaid 150. <span class="float-right"><a href="project-summary.html" target="_blank">Invoice Summary <i class="ft-arrow-right"></i></a></span></p>
                </div> -->
									<div class="table-responsive">
										<table id="recent-orders" class="table table-hover mb-0 ps-container ps-theme-default">
											<thead>
												<tr>
													<th role="columnheader">View</th>
													<th role="columnheader">Images</th>
													<th role="columnheader">Coupon Name</th>
													<th role="columnheader">Coupon Type</th>
													<th role="columnheader">Value</th>
													<th role="columnheader">Status</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($coupons as $coupon) { ?>
													<tr class="newpage">
														<td role="cell"><button type="button" onclick="window.location='<?php echo base_url('admin/coupons/showCouponDetails/'.$coupon['id'])?>';" class="btn view_btn">View</button></td>
														<td role="cell" class="text-truncate">
															<img class="img-fluid table_img" src="<?php echo base_url('uploads/coupons/'.$coupon['image']) ?>">
														</td>
														<td role="cell" class="text-truncate"><?php echo $coupon['name']; ?></td>
														<td role="cell" class="text-truncate"><?php echo $coupon['coupon_type_name']; ?></td>
														<td role="cell" class="text-truncate"><?php echo $coupon['value']; ?></td>
														<!-- <td role="cell" class="text-truncate"><span class="badge badge-success">Paid</span> -->
														<td class="text-truncate action">
															<?php if ($coupon['is_approved']) { ?>
																<button type="button" class="btn un-approve_btn" onclick="confirmUnApprove(<?php echo $coupon['id'] ?>)" data-toggle="modal" data-target="#exampleModalCenter">Approved</button>
															<?php } else { ?>
																<button type="button" class="btn approve_btn" onclick="confirmApprove(<?php echo $coupon['id'] ?>)" data-toggle="modal" data-target="#exampleModalCenter">Un-Approved</button>
															<?php } ?>
														</td>
															<!-- Button trigger modal -->
															<!-- <button type="button" class="btn approve_btn" data-toggle="modal" data-target="#exampleModalCenter">Approved</button> -->
															<!-- <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Approval</label>
                                    </div> -->
														</td>
													</tr>
												<?php } ?>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12">
							<div class="card">
								<div class="card-header">
									<h4>Users</h4>
									<!-- <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    </ul>
                </div> -->
								</div>
								<div class="card-content">
									<div id="recent-buyers" class="media-list height-300 position-relative">
										<a href="#" class="media border-0">
											<div class="media-left">
												<i class="fa fa-user-circle-o" aria-hidden="true"></i>
												<span class="avatar avatar-md avatar-online">
													<!-- <img class="media-object rounded-circle" src="../assets/images/avatar-s-7.png" alt="Generic placeholder image"> -->
												</span>
											</div>
											<div class="media-body w-100 pr-2">
												<h6 class="list-group-item-heading">all users <span class="float-right"><?php echo $total_users ?></span></h6>
												<!-- <p class="list-group-item-text mb-0"><span class="badge badge-warning">Decor</span> <span class="badge badge-danger ml-1">Appliances</span></p> -->
											</div>
										</a>
										<a href="#" class="media border-0">
											<div class="media-left">
												<i class="fa fa-user-circle-o" aria-hidden="true"></i>
												<span class="avatar avatar-md avatar-online">
													<!-- <img class="media-object rounded-circle" src="../assets/images/avatar-s-7.png" alt="Generic placeholder image"> -->

												</span>
											</div>
											<div class="media-body w-100 pr-2">
												<h6 class="list-group-item-heading">general users <span class="float-right"><?php echo $total_general_users ?></span></h6>
												<!-- <p class="list-group-item-text mb-0"><span class="badge badge-primary">Electronics</span><span class="badge badge-warning ml-1">Decor</span></p> -->
											</div>
										</a>
										<a href="#" class="media border-0">
											<div class="media-left">
												<i class="fa fa-user-circle-o" aria-hidden="true"></i>
												<span class="avatar avatar-md avatar-away">
													<!-- <img class="media-object rounded-circle" src="../assets/images/avatar-s-7.png" alt="Generic placeholder image"> -->

												</span>
											</div>
											<div class="media-body w-100 pr-2">
												<h6 class="list-group-item-heading">business users <span class="float-right"><?php echo $total_business_users ?></span></h6>
												<!-- <p class="list-group-item-text mb-0"><span class="badge badge-danger">Appliances</span></p> -->
											</div>
										</a>
										<a href="#" class="media border-0">
											<div class="media-left">
												<i class="fa fa-user-circle-o" aria-hidden="true"></i>
												<span class="avatar avatar-md avatar-busy">
													<!-- <img class="media-object rounded-circle" src="../assets/images/avatar-s-7.png" alt="Generic placeholder image"> -->

												</span>
											</div>
											<div class="media-body w-100 pr-2">
												<h6 class="list-group-item-heading">sponsor users<span class="float-right"><?php echo $total_sponsor_users ?> </span></h6>
												<!-- <p class="list-group-item-text mb-0"><span class="badge badge-primary">Electronics</span> <span class="badge badge-success ml-1">Office</span></p> -->
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/ Product sale & buyers -->
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/layout/linkfooter.php'; ?>
	</div>

	<script>
	
	

function confirmUnApprove(couponId) {
			$.confirm({
				title: 'Are you sure?',
				content: 'If you continue, This coupon will be Un-Approved.',
				buttons: {
					confirm: function() {
						unApproveCoupon(couponId);
					},
					cancel: function() {
						return true;
					},
				}
			});
		}


		function confirmApprove(couponId) {
			$.confirm({
				title: 'Are you sure?',
				content: 'If you continue, This coupon will be Approved.',
				buttons: {
					confirm: function() {
						approveCoupon(couponId);
					},
					cancel: function() {
						return true;
					},
				}
			});
		}

		
		function unApproveCoupon(couponId){
			$.ajax({
				url:base_url+"admin/coupons/unApproveCoupon",
				method: 'POST',
				dataType: 'json',
				data:{id:couponId},
				success:function(res){
					if(res.error){
						show_flash_message("error", res.msg);	
					}else{
						show_flash_message("success", res.msg);
						window.location.reload();
					}
				},
				error: function(res){

				}
			})
		}

		function approveCoupon(couponId){
			$.ajax({
				url:base_url+"admin/coupons/approveCoupon",
				method: 'POST',
				dataType: 'json',
				data:{id:couponId},
				success:function(res){
					if(res.error){
						show_flash_message("error", res.msg);	
					}else{
						show_flash_message("success", res.msg);
						window.location.reload();
					}
				},
				error: function(res){
				}
			})
		}
		</script>

</body>

</html>
