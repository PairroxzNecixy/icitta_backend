<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | ADD NEW CATEGORIES</title>
	<?php include __DIR__ . '/layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

	<?php include __DIR__ . "/layout/alerts.php" ?>

	<div class="wrapper">
		<?php include __DIR__ . '/layout/sidemenu.php'; ?>
		<div class="app-content content">
			<div class="content-wrapper my-5 my-md-0">
				<div class="row match-height">
					<div class="col-lg-5 col-md-5 m-auto">
						<div class="create-wrapper p-2">
							<form action="<?php echo base_url("admin/categories/create") ?>" method="POST" enctype="multipart/form-data">
								<h4 class="text-center mb-2">add new category</h4>
								<fieldset class="form-group mb-2">
									<div class="input-group">
										<!-- <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                    </div> -->
										<div class="custom-file">
											<input name="image" type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" required>
											<label class="custom-file-label" for="inputGroupFile01">Choose file</label>
										</div>
									</div>
									<!-- <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01">
                                    <label class="custom-file-label" for="inputGroupFile01">Category Image</label>
                                </div> -->
								</fieldset>
								<fieldset class="form-group mb-2">
									<input name="name"type="text" id="roundText" class="form-control round" placeholder="Category Name" required>
								</fieldset>

								<fieldset class="form-group mb-2">
									<input name="name_italian"type="text" id="roundText" class="form-control round" placeholder="Nome della categoria" required>
								</fieldset>
								<!-- <fieldset class="form-group mb-2">
                                <textarea class="form-control" id="descTextarea" rows="4"
                                    placeholder="Description"></textarea>
                            </fieldset>
                            <fieldset class="form-group mb-2">
                                <input type="text" id="roundText" class="form-control round"
                                    placeholder="Value">
                                    <span class="percentage"><i class="fa fa-percent" aria-hidden="true"></i></span>
                            </fieldset> -->
								<fieldset class="text-center">
									<button type="submit" class="btn submit_btn">add</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/layout/linkfooter.php'; ?>
	</div>
</body>

</html>
