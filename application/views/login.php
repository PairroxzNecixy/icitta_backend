<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | LOGIN</title>
	<?php include __DIR__ . '/layout/linkheader.php'; ?>
</head>

<body style="background: #F5F7FA;" class="vertical-layout vertical-menu 1-column  bg-full-screen-image blank-page blank-page" data-open="click" data-menu="vertical-menu" data-col="1-column">

	<?php include __DIR__ . "/layout/alerts.php" ?>

	<!-- BEGIN: Content-->
	<div class="app-content content">
		<div class="content-wrapper">
			<div class="content-header row">
			</div>
			<div class="content-body">
				<section class="flexbox-container">
					<div class="col-12 d-flex align-items-center justify-content-center">
						<div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
							<div class="card border-grey border-lighten-3 px-1 py-1 m-0">
								<div class="card-header border-0 pb-0">
									<div class="card-title text-center">
										<a href="#">
											<img src="<?= base_url('assets/images/logo.png') ?>" alt="branding logo">
										</a>
									</div>
									<h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Login With Icitta</span></h6>
								</div>
								<div class="card-content">
									<!-- <div class="text-center">
                                    <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span class="fa fa-facebook"></span></a>
                                    <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"><span class="fa fa-twitter"></span></a>
                                    <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span class="fa fa-linkedin font-medium-4"></span></a>
                                    <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-github"><span class="fa fa-github font-medium-4"></span></a>
                                </div> -->
									<!-- <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>OR Using Account Details</span></p> -->
									<div class="card-body">
										<form class="form-horizontal" action="<?php echo base_url("admin/login/login") ?>" method="POST">
											<fieldset class="form-group position-relative has-icon-left">
												<input type="text" name="username" class="form-control" id="user-name" placeholder="Your Username" required>
												<div class="form-control-position">
													<i class="fa fa-user-o" aria-hidden="true"></i>
												</div>
											</fieldset>
											<fieldset class="form-group position-relative has-icon-left">
												<input type="password" name="password" class="form-control" maxlength="8" id="user-password" placeholder="Enter Password" required>
												<div class="form-control-position">
													<i class="fa fa-key"></i>
												</div>
											</fieldset>
											<button type="submit" onclick="window.location='Dashboard';" class="btn recover_password btn-block"> Login</button>
											<div class="form-group row mb-0 pt-1">
												<!-- <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                                <fieldset class="mb-1 mb-sm-0">
                                                    <div class="custom-control custom-checkbox mr-sm-2">
                                                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                                        <label class="custom-control-label" for="customControlAutosizing">Remember me</label>
                                                    </div>
                                                </fieldset>
                                            </div> -->
												<div class="col-sm-12 col-12 float-sm-left text-center"><a href="<?php echo base_url('forgetPassword') ?>" class="card-link">Forgot Password?</a></div>
											</div>
										</form>
									</div>
									<!-- <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>New to Stack ?</span></p>
                                <div class="card-body">
                                    <a href="register-with-bg-image.html" class="btn btn-outline-danger btn-block"><i class="ft-user"></i> Register</a>
                                </div> -->
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	<!-- END: Content-->
	</div>

	<?php include __DIR__ . '/layout/linkfooter.php'; ?>

</body>

</html>
