<div class="flash-alert">
	<?php if ($this->session->flashdata('error') != null) { ?>
		<div class="alert alert-danger show">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> <?php echo $this->session->flashdata('error') ?>
		</div>
	<?php } ?>

	<?php if (!empty(validation_errors())) { ?>
		<div class="alert alert-danger show">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Error!</strong> <?php echo validation_errors(); ?>
		</div>
	<?php } ?>

	<?php if ($this->session->flashdata('success') != null) { ?>
		<div class="alert alert-success show">
			<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Success!</strong> <?php echo $this->session->flashdata('success') ?>
		</div>
	<?php } ?>


</div>
