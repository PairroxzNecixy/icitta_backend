<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ICITTA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/vendors.min.css'); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url('assets/css/vertical-menu.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/bootstrap-extended.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/components.min.css?v=1.0.1.3'); ?>" rel="stylesheet" type="text/css" />

    <!-- <link href="<?php echo base_url('assets/css/unslider.css'); ?>" rel="stylesheet" type="text/css"/> -->
    <link href="<?php echo base_url('assets/css/morris.css'); ?>" rel="stylesheet" type="text/css" />

    <!-- <link href="<?php echo base_url('assets/css/jquery-jvectormap-2.0.3.css?v=1.0.1.2'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/css/climacons.min.css?v=1.0.1.2'); ?>" rel="stylesheet" type="text/css"/> -->

<link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap4.min.css'); ?>">

    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/style.css?v=1.0.6.26'); ?>" rel="stylesheet" type="text/css" />


    <script>
    var base_url = '<?php echo base_url(); ?>';
    </script>

</head>


<body>

</body>

</html>