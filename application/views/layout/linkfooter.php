<footer class="footer footer-static footer-dark navbar-border">
	<p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
		<span class="float-md-left d-block d-md-inline-block">
			Copyright &copy; 2019 <a class="text-bold-800 grey darken-2" href="#" target="_blank">
			</a></span>
		<!-- <span class="float-md-right d-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span> -->
	</p>
</footer>


<!-- <script src="<?php echo base_url('assets/js/jquery.min3.2.0.js') ?>" type="text/javascript"></script> -->
<script src="<?php echo base_url('assets/js/vendors.min.js') ?>" type="text/javascript"></script>
<!-- <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script> -->

<!-- <script src="<?php echo base_url('assets/js/raphael-min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/morris.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/horizontal-timeline.js') ?>" type="text/javascript"></script> -->


<script src="<?php echo base_url('assets/js/app-menu.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/app.min.js') ?>" type="text/javascript"></script>
<!-- <script src="<?php echo base_url('assets/js/customizer.min.js') ?>" type="text/javascript"></script> -->



<!-- <script src="<?php echo base_url('assets/js/dashboard-ecommerce.min.js') ?>" type="text/javascript"></script> -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>



<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


<!-- <script>
$('.navigation li').click(function(e) {
    console.log('click');
    $('.navigation li.active').removeClass('active');
    var $this = $(this);
    if (!$this.hasClass('active')) {
        $this.addClass('active');
    }
    e.preventDefault();
});
</script> -->

<script>
	setTimeout(function() {
		$(".flash-alert .alert").hide();
	}, 6000);

	function show_flash_message(type, message) {

		var class_ = '';
		var alerttext = '';
		if (type == 'success') {
			class_ = "success";
			alerttext = 'Success! ';
		} else {
			class_ = "danger";
			alerttext = 'Error! ';
		}

		elm = '<div class="alert alert-' + class_ + ' show">' +
			'<a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
			'<strong>' + alerttext + '</strong> ' + message +
			'</div>';

		$(".flash-alert").append(elm);

		setTimeout(function() {
			$(".flash-alert .alert").hide();
		}, 6000);
	}
</script>