<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | ALL COUPONS</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>
<style>
	@media only screen and (max-width: 760px),
	(min-device-width: 768px) and (max-device-width: 1024px) {

		/* Force table to not be like tables anymore */
		table,
		thead,
		tbody,
		th,
		td,
		tr {
			display: block;
		}

		/* Hide table headers (but not display: none;, for accessibility) */
		thead tr {
			position: absolute;
			top: -9999px;
			left: -9999px;
		}

		tr {
			margin: 0 0 1rem 0;
		}

		tr:nth-child(odd) {
			background: #ccc;
			margin-bottom: 0;
		}

		td {
			/* Behave  like a "row" */
			border: none;
			border-bottom: 1px solid #eee;
			position: relative;
			padding-left: 50%;
		}

		td:before {
			/* Now like a table header */
			position: absolute;
			/* Top/left values mimic padding */
			/* top: 0; */
			left: 15px;
			width: 45%;
			padding-right: 10px;
			text-align: left;
			/* white-space: nowrap; */
		}

		.table td {
			text-align: right;
		}

		/*
    Label the data
You could also use a data-* attribute and content for this. That way "bloats" the HTML, this way means you need to keep HTML and CSS in sync. Lea Verou has a clever way to handle with text-shadow.
    */
		td:nth-of-type(1):before {
			content: "Edit";
		}

		td:nth-of-type(2):before {
			content: "Image";
		}

		td:nth-of-type(3):before {
			content: "Coupon Type";
		}

		td:nth-of-type(4):before {
			content: "Coupon Name";
		}

		td:nth-of-type(5):before {
			content: "Value";
		}

		td:nth-of-type(6):before {
			content: "Status";
		}

		/*td:nth-of-type(7):before { content: "Date of Birth"; }
    td:nth-of-type(8):before { content: "Dream Vacation City"; }
    td:nth-of-type(9):before { content: "GPA"; }
    td:nth-of-type(10):before { content: "Arbitrary Data"; } */
	}
</style>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>

	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>
		<div class="app-content content">
			<div class="content-wrapper">
				<!-- <div class="row match-height">
            <div class="col-md-12 text-right mb-2">
                <button type="submit" onClick="window.location='createcoupon';" class="btn submit_btn btn-primary create">Create</button>
            </div>
        </div> -->
				<div class="row match-height">
					<div class="col-xl-12 col-lg-12">
						<div class="card">
							<div class="card-header p-2">
								<h4>All Coupons</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<!-- <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="fa fa-refresh" aria-hidden="true"></i></a></li>
                                </ul>
                            </div> -->
							</div>
							<div class="card-content">
								<!-- <div class="card-body">
                                <p>Total paid invoices 240, unpaid 150. <span class="float-right"><a href="project-summary.html" target="_blank">Invoice Summary <i class="ft-arrow-right"></i></a></span></p>
                            </div> -->
								<div class="table-responsive">
									<table id="coupons_table" class="table table-hover mb-0 ps-container ps-theme-default">
										<thead>
											<tr>
												<th>Action</th>
												<th>Images</th>
												<th>Business name</th>
												<th>Coupon Type</th>
												<th>Coupon Name</th>
												<th>Coupon Used</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($coupons as $coupon) { ?>
												<tr class="newpage">
													<td>
														<button type="button" onclick="confirmDeleteCoupon(<?php echo $coupon['id'] ?>, this)" class="btn view_btn" style="margin-right:5px;margin-bottom:5px">Delete</button>
														<button type="button" onclick="window.location=base_url+'admin/coupons/showCouponDetails/<?php echo $coupon['id'] ?>';" class="btn view_btn">View</button>
													</td>
													<td class="text-truncate">
														<img class="img-fluid table_img" src="<?php echo base_url('uploads/coupons/' . $coupon['image']) ?>"></td>
													<td><?php echo $coupon['business_name']; ?></td>
													<td class="text-truncate"><?php echo $coupon['coupon_type_id'] == 1 ? "Percent wise discount" :  ($coupon['coupon_type_id'] == "2" ? 'Box' : 'Promotion	') ; ?></td>
													<!--<td class="text-truncate"><?php echo $coupon['coupon_type_id']; ?></td>-->
													<td class="text-truncate"><?php echo $coupon['name']; ?></td>
													<td class="text-truncate"><?php echo $coupon['verified_count']; ?></td>
													<!-- <td class="text-truncate">65</td> -->
													<td class="text-truncate action">
														<?php if ($coupon['is_approved']) { ?>
															<button type="button" class="btn un-approve_btn" onclick="confirmUnApprove(<?php echo $coupon['id'] ?>)" data-toggle="modal" data-target="#exampleModalCenter">Approved</button>
														<?php } else { ?>
															<button type="button" class="btn approve_btn" onclick="confirmApprove(<?php echo $coupon['id'] ?>)" data-toggle="modal" data-target="#exampleModalCenter">Un-Approved</button>
														<?php } ?>
													</td>
													<!-- Button trigger modal -->
													<!-- <button type="button" class="btn approve_btn" data-toggle="modal" data-target="#exampleModalCenter2">Approved</button> -->
													<!-- <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">Approval</label>
                                                </div> -->
													</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>

	<script>
		$(document).ready(function() {
			$('#coupons_table').DataTable({
				order: [],
				stateSave: true
			});
		});

		function confirmUnApprove(couponId) {
			$.confirm({
				title: 'Are you sure?',
				content: 'If you continue, This coupon will be Un-Approved.',
				buttons: {
					confirm: function() {
						unApproveCoupon(couponId);
					},
					cancel: function() {
						return true;
					},
				}
			});
		}


		function confirmApprove(couponId) {
			$.confirm({
				title: 'Are you sure?',
				content: 'If you continue, This coupon will be Approved.',
				buttons: {
					confirm: function() {
						approveCoupon(couponId);
					},
					cancel: function() {
						return true;
					},
				}
			});
		}


		function unApproveCoupon(couponId) {
			$.ajax({
				url: base_url + "admin/coupons/unApproveCoupon",
				method: 'POST',
				dataType: 'json',
				data: {
					id: couponId
				},
				success: function(res) {
					if (res.error) {
						show_flash_message("error", res.msg);
					} else {
						show_flash_message("success", res.msg);
						window.location.reload();
					}
				},
				error: function(res) {

				}
			})
		}

		function approveCoupon(couponId) {
			$.ajax({
				url: base_url + "admin/coupons/approveCoupon",
				method: 'POST',
				dataType: 'json',
				data: {
					id: couponId
				},
				success: function(res) {
					if (res.error) {
						show_flash_message("error", res.msg);
					} else {
						show_flash_message("success", res.msg);
						window.location.reload();
					}
				},
				error: function(res) {

				}
			})
		}

		function confirmDeleteCoupon(coupon_id, elm) {
			console.log("cofirm delete user");
			$.confirm({
				title: 'Are you sure?',
				content: 'If you continue, This coupon will be deleted permanently.',
				buttons: {
					confirm: function() {
						deleteCoupon(coupon_id);
					},
					cancel: function() {
						return true;
					},
				}
			});
		}


		function deleteCoupon(coupon_id) {
			$.ajax({
				url: base_url + "admin/coupons/deleteCoupon",
				method: 'POST',
				dataType: 'json',
				data: {
					id: coupon_id,
				},
				success: function(res) {
					if (res.error) {
						show_flash_message("error", res.msg);
					} else {
						show_flash_message("success", res.msg);
						window.location.reload();
					}
				},
				error: function(res) {

				}
			});
		}
	</script>
</body>

</html>