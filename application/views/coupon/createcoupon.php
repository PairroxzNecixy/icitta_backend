<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | CREATECOUPON</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>

	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>
		<div class="app-content content">
			<div class="content-wrapper my-5 my-md-0">
				<div class="row match-height">
					<div class="col-lg-5 col-md-5 m-auto">
						<div class="create-wrapper p-2">
							<form action="<?php echo base_url('admin/coupons/saveCoupon') ?>" method="post" enctype="multipart/form-data">
								<h4 class="text-center mb-2">create a coupon</h4>
								<fieldset class="form-group mb-2">
									<div class="custom-file">
										<input type="file" name="image" class="custom-file-input" id="inputGroupFile01">
										<label class="custom-file-label" for="inputGroupFile01">Coupon Image</label>
									</div>
								</fieldset>
								<fieldset class="form-group mb-2">
									<input type="text" name="business_name" id="roundText" class="form-control round" placeholder="Busines Name">
								</fieldset>
								<fieldset class="form-group mb-2">
									<input type="text" name="link" id="roundText" class="form-control round" placeholder="Link">
								</fieldset>

								<fieldset class="form-group mb-2">
									<input type="text" name="priority" id="roundText" class="form-control round" placeholder="Priority (1 to 10) ">
								</fieldset>

								<!-- <fieldset class="form-group mb-2">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01">
                                    <label class="custom-file-label" for="inputGroupFile01">Sponsor Image</label>
                                </div>
                            </fieldset>
                            <fieldset class="form-group mb-2">
                                <textarea class="form-control" id="descTextarea" rows="4"
                                    placeholder="Description"></textarea>
                            </fieldset>
                            <fieldset class="form-group mb-2">
                                <input type="text" id="roundText" class="form-control round"
                                    placeholder="Value">
                                    <span class="percentage"><i class="fa fa-percent" aria-hidden="true"></i></span>
                            </fieldset>
                            <fieldset class="form-group mb-2">
                                    <input type="date" id="issueinput3" class="form-control round" name="date"
                                      data-trigger="hover" data-placement="top" data-title="Date" placeholder="05/08/2019">
                            </fieldset> -->
								<fieldset class="text-center">
									<button type="submit" class="btn submit_btn">create</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>
</body>

</html>
