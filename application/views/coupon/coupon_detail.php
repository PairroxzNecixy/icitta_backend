<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | CATEGORY</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>

	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>
		<div class="app-content content">
			<div class="content-wrapper">
				<div class="row">
					<div class="col-md-3 d-flex align-items-center">
						<figure class="mb-0">
							<img class="img-fluid" src="<?php echo base_url('uploads/coupons/' . $coupon->image) ?>">
						</figure>
					</div>
					<div class="col-md-9 pt-md-4 pt-2">
						<div class="name_type">
						    <div>
							   <a class="btn btn-primary" href="<?php echo base_url("admin/coupons/edit_coupon/".$coupon->id) ?>">Edit</a>
						    </div>
							<h4 class="text-capitalize m-0 pt-2"><?php echo $coupon->name ?></h4>
							<p class="text-capitalize m-0 pt-1"><?php echo $coupon->coupon_type_name ?></p>
							<p class="text-capitalize m-0 pt-1">Business Name: <?php echo $coupon->business_name ?></p>
							<p class="text-capitalize m-0 pt-1">Coupon Name: <?php echo $coupon->name ?></p>
								<p class="text-capitalize m-0 pt-1">Coupon Type: <?php echo $coupon->coupon_type_id == 1 ? "Percent wise discount" :  ($coupon->coupon_type_id == 2? 'Box' : 'Promotion	') ; ?></p>
							<p class="text-capitalize m-0 pt-1">Coupon Used: <?php echo $coupon->verified_count ?></p>
							<p class="m-0 pt-1"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;Expiration Date: &nbsp;<?php echo date("d F Y", strtotime($coupon->expire_date)) ?>
								<span class="pl-2"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;Expiration Time: <?php echo date("g:i A", strtotime($coupon->expire_date)) ?></span>
							</p>
							<div class="coupon_discription py-3">
								<p><?php echo $coupon->description ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>
</body>

</html>
