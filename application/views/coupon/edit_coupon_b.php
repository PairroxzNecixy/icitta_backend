<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | EDIT COUPON</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>

	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>
		<div class="app-content content">
			<div class="content-wrapper my-5 my-md-0">
				<div class="row match-height">
					<div class="col-lg-5 col-md-5 m-auto">
						<div class="create-wrapper p-2">
                            <form action="<?php echo base_url('admin/coupons/update_coupon') ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $coupon->id ?>">
								<h4 class="text-center mb-2">Edit coupon</h4>
								<fieldset class="form-group mb-2">
									<div class="custom-file">
										<input type="file" name="image" class="custom-file-input" id="inputGroupFile01">
										<label class="custom-file-label" for="inputGroupFile01">Change Image Or Leave blank</label>
									</div>
								</fieldset>
								<fieldset class="form-group mb-2">
									<input type="text" name="name" id="roundText" value="<?php echo $coupon->name ?>" class="form-control round" placeholder="Coupon name">
                                </fieldset>

							    <fieldset class="form-group mb-2">
									<textarea name="description" id="roundText" class="form-control round" placeholder="Coupon Description" rows="6"><?php echo $coupon->description ?></textarea>
									<!-- <input type="text" name="description" id="roundText" value="" class="form-control round" placeholder="Coupon Description"> -->
                                </fieldset>
                                
                                 <fieldset class="form-group mb-2">
									<select name="CuponType" id="selectText" class="form-control round" placeholder="Coupon type" rows="6">
									<?php for($i=0;$i<count($cuponTypeArray);$i++){ if($cuponTypeArray[$i]['id'] == $coupon->coupon_type_id) { ?>
									<option value="<?php echo $i+1 ?>" selected="selected"><?php echo $cuponTypeArray[$i]['name']?></option>
									<?php }else{ ?>
										<option value="<?php echo $i+1 ?>"><?php echo $cuponTypeArray[$i]['name']?></option>
									<?php } 
    								}
    								?>
								</select>
									
                                </fieldset>

								<fieldset class="form-group mb-2">
									<input type="text" name="expire_date" id="roundText" value="<?php echo $coupon->expire_date ?>" class="form-control round" placeholder="Coupon expire date">
                                </fieldset>

                              

                                    <fieldset class="form-group mb-2" id="valueText">
                                        <input type="text" name="value" id="roundText" value="<?php echo $coupon->value ?>" class="form-control round" placeholder="Coupon value">
                                    </fieldset>

                              
                                
								<!-- <fieldset class="form-group mb-2">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01">
                                    <label class="custom-file-label" for="inputGroupFile01">Sponsor Image</label>
                                </div>
                            </fieldset>
                            <fieldset class="form-group mb-2">
                                <textarea class="form-control" id="descTextarea" rows="4"
                                    placeholder="Description"></textarea>
                            </fieldset>
                            <fieldset class="form-group mb-2">
                                <input type="text" id="roundText" class="form-control round"
                                    placeholder="Value">
                                    <span class="percentage"><i class="fa fa-percent" aria-hidden="true"></i></span>
                            </fieldset>
                            <fieldset class="form-group mb-2">
                                    <input type="date" id="issueinput3" class="form-control round" name="date"
                                      data-trigger="hover" data-placement="top" data-title="Date" placeholder="05/08/2019">
                            </fieldset> -->
								<fieldset class="text-center">
									<button type="submit" class="btn submit_btn">Update</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>
</body>

<script>
$(document).ready(function(){
	if($("#selectText").val() == 1){
     $("#valueText").css("display","block")
  }else{
	$("#valueText").css("display","none") 
  }
  $("#selectText").change(function() {	
      console.log($("#selectText").val());
  if($("#selectText").val() == 1){
     $("#valueText").css("display","block")
  }else{
	$("#valueText").css("display","none") 
  }
  });
})
</script>

</html>
