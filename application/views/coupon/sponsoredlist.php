<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | Sponsor Coupon</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>
<style>
	@media only screen and (max-width: 760px),
	(min-device-width: 768px) and (max-device-width: 1024px) {

		/* Force table to not be like tables anymore */
		table,
		thead,
		tbody,
		th,
		td,
		tr {
			display: block;
		}

		/* Hide table headers (but not display: none;, for accessibility) */
		thead tr {
			position: absolute;
			top: -9999px;
			left: -9999px;
		}

		tr {
			margin: 0 0 1rem 0;
		}

		tr:nth-child(odd) {
			background: #ccc;
			margin-bottom: 0;
		}

		td {
			/* Behave  like a "row" */
			border: none;
			border-bottom: 1px solid #eee;
			position: relative;
			padding-left: 50%;
		}

		td:before {
			/* Now like a table header */
			position: absolute;
			/* Top/left values mimic padding */
			/* top: 0; */
			left: 15px;
			width: 45%;
			padding-right: 10px;
			text-align: left;
			/* white-space: nowrap; */
		}

		.table td {
			text-align: right;
		}

		/*
		Label the data
    You could also use a data-* attribute and content for this. That way "bloats" the HTML, this way means you need to keep HTML and CSS in sync. Lea Verou has a clever way to handle with text-shadow.
		*/
		td:nth-of-type(1):before {
			content: "Edit";
		}

		td:nth-of-type(2):before {
			content: "Image";
		}

		td:nth-of-type(3):before {
			content: "Coupon Type";
		}

		td:nth-of-type(4):before {
			content: "Coupon Name";
		}

		td:nth-of-type(5):before {
			content: "Value";
		}

		td:nth-of-type(6):before {
			content: "Status";
		}

		/*td:nth-of-type(7):before { content: "Date of Birth"; }
		td:nth-of-type(8):before { content: "Dream Vacation City"; }
		td:nth-of-type(9):before { content: "GPA"; }
		td:nth-of-type(10):before { content: "Arbitrary Data"; } */
	}
</style>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>

	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>

		<div class="app-content content">
			<div class="content-wrapper">
				<div class="row match-height">
					<div class="col-md-12 text-right mb-2">
						<button type="submit" onClick="window.location=base_url+'admin/coupons/createCoupon';" class="btn submit_btn create">Create</button>
					</div>
				</div>
				<div class="row match-height">
					<div class="col-xl-12 col-lg-12">
						<div class="card">
							<div class="card-header p-2">
								<h4>Sponsored Coupons</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<!-- <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="fa fa-refresh" aria-hidden="true"></i></a></li>
                            </ul>
                        </div> -->
							</div>
							<div class="card-content">
								<!-- <div class="card-body">
                            <p>Total paid invoices 240, unpaid 150. <span class="float-right"><a href="project-summary.html" target="_blank">Invoice Summary <i class="ft-arrow-right"></i></a></span></p>
                        </div> -->
								<div class="table-responsive">
									<table id="coupons_table" class="table table-hover mb-0 ps-container ps-theme-default">
										<thead>
											<tr>
												<th>Images</th>
												<th>Priority</th>
												<th>Business name</th>
												<th>Link</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($sponsoredList as $coupon) { ?>
												<tr class="newpage">
													<!-- <td><button type="button" onclick="window.location=base_url+'coupons/showCouponDetails/<?php echo $coupon->id ?>';" class="btn view_btn">View</button></td> -->
													<td class="text-truncate">
														<img class="img-fluid table_img" src="<?php echo base_url('uploads/bannerImage/' . $coupon->image) ?>">
													</td>
													<td><?php echo $coupon->priority; ?></td>
													<td><?php echo $coupon->business_name; ?></td>
													<td class="text-truncate"><a target="_blank" href="<?php echo $coupon->link; ?>">Link</a></td>
													<td class="text-truncate">
														<a href="<?php echo base_url("admin/sponsoredList/edit_coupon/".$coupon->id) ?>">Edit</a>
														&nbsp; &nbsp; &nbsp; &nbsp;
														<a href="javascript:void(0)" onclick="confirmDeleteSponsoredCoupon(<?php echo $coupon->id ?>, this)">Delete</a>
													</td>
													<!-- <td class="text-truncate">65</td> -->
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5> -->
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">
					Are you sure to approve this coupon ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Approved</button>
					<button type="button" class="btn btn-primary">Unapproved</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$('#coupons_table').DataTable({
				order: []
			});
		});




		function confirmDeleteSponsoredCoupon(couponId, elm) {

			$.confirm({
				title: 'Are you sure?',
				content: 'If you continue, This coupon will be deleted.',
				buttons: {
					confirm: function() {
						deleteCoupon(couponId);
					},
					cancel: function() {
						return true;
					},
				}
			});
		}


		function deleteCoupon(couponId) {
			$.ajax({
				url: base_url + "admin/sponsoredList/deleteSponsoredCoupon",
				method: 'POST',
				dataType: 'json',
				data: {
					id: couponId
				},
				success: function(res) {
					if (res.error) {
						show_flash_message("error", res.msg);
					} else {
						show_flash_message("success", res.msg);
						window.location.reload();
					}
				},
				error: function(res) {

				}
			})
		}
	</script>
</body>

</html>