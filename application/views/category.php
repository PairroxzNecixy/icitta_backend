<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | CATEGORY</title>
	<?php include __DIR__ . '/layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . "/layout/alerts.php" ?>

	<div class="wrapper">
		<?php include __DIR__ . '/layout/sidemenu.php'; ?>
		<div class="app-content content">
			<div class="content-wrapper">
				<div class="row match-height">
					<div class="col-md-12 text-right pb-2">
						<button type="submit" onclick="window.location=base_url+'admin/addCategory';" class="btn submit_btn create">add category</button>
					</div>
				</div>
				<div class="row match-height">

					<?php foreach ($categories as $cat) : ?>
						<div class="col-lg-3 col-sm-4 text-center">
							<div class="coupon-grid">
								<img src="<?php echo base_url('uploads/categories/' . $cat->image) ?>" class="img-fluid">
								<div class="coupon_name text-center">
									<p><?php echo $cat->name_italian ?></p>
								</div>
							</div>
							<h4 class="mb-0 py-2 text-capitalize"><?php echo $cat->name ?></h4>
						</div>

					<?php endforeach; ?>

				</div>
			</div>
		</div>
		<?php include __DIR__ . '/layout/linkfooter.php'; ?>
	</div>
</body>

</html>