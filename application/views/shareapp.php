<script>
(function () {
    var app = {
        launchApp: function () {
            <?php if($type==0) { ?>
                window.location.replace("<?php echo $link; ?>");
            <?php } ?>
            this.timer = setTimeout(this.openWebApp,0);
        },

        openWebApp: function () {
            window.location.replace("<?php echo $redirect_to; ?>");
        }
    };

    app.launchApp();
})();
</script>