<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | Users</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>
	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>

		<div class="app-content content mt-2">
			<div class="content-wrapper">
				<div class="row match-height">
					<div class="col-xl-12 col-lg-12">
						<div class="card">
							<div class="card-header p-2">
								<h4>Sponsor Users</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<!-- <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="fa fa-refresh" aria-hidden="true"></i></a></li>
                                </ul>
                            </div> -->
							</div>
							<div class="card-content">
								<!-- <div class="card-body">
                                <p>Total paid invoices 240, unpaid 150. <span class="float-right"><a href="project-summary.html" target="_blank">Invoice Summary <i class="ft-arrow-right"></i></a></span></p>
                            </div> -->
								<div class="table-responsive">
									<table id="all_users" class="table table-hover mb-0 ps-container ps-theme-default">
										<thead>
											<tr>
												<th>View</th>
												<th>User Type</th>
												<th>Name</th>
												<th>Email ID</th>
												<th>City</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($users as $User) { ?>
												<tr class="newpage">
													<td><button type="button" onClick="window.location='<?php echo base_url('admin/users/userDetail/' . $User['id']) ?>';" class="btn view_btn">View</button></td>
													<td class="text-truncate">
														<?php echo $User['user_type']; ?>
														<!-- <img class="img-fluid table_img" src="<?php echo base_url('admin/users/userDetail/' . $user['id']) ?>"> -->
													</td>
													<td class="text-truncate"><?php echo $User['name']; ?></td>
													<td class="text-truncate"><?php echo $User['email']; ?></td>
													<td class="text-truncate"><?php echo $User['city']; ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>

	<script>
		$(document).ready(function() {
			$('#all_users').DataTable({
				order: []
			});
		});
	</script>
</body>

</html>