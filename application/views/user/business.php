<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | Business</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>
	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>
		<div class="app-content content mt-2">
			<div class="content-wrapper">
				<div class="row match-height">
					<div class="col-xl-12 col-lg-12">
						<div class="card">
							<div class="card-header p-2">
								<h4>Business Users</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<!-- <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="fa fa-refresh" aria-hidden="true"></i></a></li>
                            </ul>
                        </div> -->
							</div>
							<div class="card-content">
								<!-- <div class="card-body">
                            <p>Total paid invoices 240, unpaid 150. <span class="float-right"><a href="project-summary.html" target="_blank">Invoice Summary <i class="ft-arrow-right"></i></a></span></p>
                        </div> -->
								<div class="table-responsive">
									<table id="recent-orders" class="table table-hover mb-0 ps-container ps-theme-default">
										<thead>
											<tr>
												<th>View</th>
												<th>User Type</th>
												<th>Name</th>
												<th>Email ID</th>
												<th>City</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($BusinessUsers as $User) { ?>
											<tr class="newpage">
												<td><button type="button" onClick="window.location='DetailPage';" class="btn view_btn">View</button></td>
												<td class="text-truncate">
													<?php echo $User['user_type_id']; ?>
													<!-- <img class="img-fluid table_img" src="<?php echo base_url('assets/images/avatar-s-1.png') ?>"> -->
												</td>
												<td class="text-truncate"><?php echo $User['name']; ?></td>
												<td class="text-truncate"><?php echo $User['email']; ?></td>
												<td class="text-truncate"><?php echo $User['city']; ?></td>
												<td class="text-truncate">
													<!-- <span class="badge badge-success">Paid</span> -->
													<button type="button" class="btn approve_btn" data-toggle="modal" data-target="#exampleModalCenter8">Approved</button>
												</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5> -->
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">
					Are you sure you want to approve this user ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Yes</button>
					<button type="button" class="btn btn-primary">No</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter9" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5> -->
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">
					Are you sure you want to approve this user ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Yes</button>
					<button type="button" class="btn btn-primary">No</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5> -->
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">
					Are you sure you want to approve this user ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Yes</button>
					<button type="button" class="btn btn-primary">No</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter11" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5> -->
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">
					Are you sure you want to approve this user ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Yes</button>
					<button type="button" class="btn btn-primary">No</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter12" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5> -->
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">
					Are you sure you want to approve this user ?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Yes</button>
					<button type="button" class="btn btn-primary">No</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
</body>

</html>
