<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | Business</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>
	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>

		<div class="app-content content mt-2">
			<div class="content-wrapper">
				<div class="row match-height">
					<div class="col-xl-12 col-lg-12">
						<div class="card">
							<div class="card-header p-2">
								<h4>Business Users</h4>
								<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
								<!-- <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="fa fa-refresh" aria-hidden="true"></i></a></li>
                                </ul>
                            </div> -->
							</div>
							<div class="card-content">
								<!-- <div class="card-body">
                                <p>Total paid invoices 240, unpaid 150. <span class="float-right"><a href="project-summary.html" target="_blank">Invoice Summary <i class="ft-arrow-right"></i></a></span></p>
                            </div> -->
								<div class="table-responsive">
									<table id="all_users" class="table table-hover mb-0 ps-container ps-theme-default">
										<thead>
											<tr>
												<th>Delete</th>
                                                <th>View</th>
												<th>User Type</th>
												<th>Name</th>
												<th>Email ID</th>
												<th>City</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($users as $user) { ?>
												<tr class="newpage">
												     <td><button type="button"
                                                        onclick="confirmDeleteUser(<?php echo $user['id'] ?>,'<?php echo $user['user_type'] ?>', this)"
                                                        class="btn view_btn">Delete</button></td>
													<td><button type="button" onClick="window.location='<?php echo base_url('admin/users/userDetail/' . $user['id']) ?>';" class="btn view_btn">View</button></td>
													<td class="text-truncate">
														<?php echo $user['user_type']; ?>
														<!-- <img class="img-fluid table_img" src="<?php echo base_url('admin/users/userDetail/' . $user['id']) ?>"> -->
													</td>
													<td class="text-truncate"><?php echo $user['name']; ?></td>
													<td class="text-truncate"><?php echo $user['email']; ?></td>
													<td class="text-truncate"><?php echo $user['city']; ?></td>
													<td class="text-truncate action">
														<?php if ($user['is_actived']) { ?>
															<button type="button" class="btn un-approve_btn" onclick="confirmUnApprove(<?php echo $user['id'] ?>)" data-toggle="modal" data-target="#exampleModalCenter">Approved</button>
														<?php } else { ?>
															<button type="button" class="btn approve_btn" onclick="confirmApprove(<?php echo $user['id'] ?>)" data-toggle="modal" data-target="#exampleModalCenter">Un-Approved</button>
														<?php } ?>
													</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>



	<script>
		$(document).ready(function() {
			$('#all_users').DataTable({
				order: []
			});
		});

		function confirmUnApprove(couponId) {
			$.confirm({
				title: 'Are you sure?',
				content: 'If you continue, This User will be Un-Approved.',
				buttons: {
					confirm: function() {
						unApproveUser(couponId);
					},
					cancel: function() {
						return true;
					},
				}
			});
		}


		function confirmApprove(couponId) {
			$.confirm({
				title: 'Are you sure?',
				content: 'If you continue, This User will be Approved.',
				buttons: {
					confirm: function() {
						approveUser(couponId);
					},
					cancel: function() {
						return true;
					},
				}
			});
		}


		function unApproveUser(couponId) {
			$.ajax({
				url: base_url + "admin/users/unApproveUser",
				method: 'POST',
				dataType: 'json',
				data: {
					id: couponId
				},
				success: function(res) {
					if (res.error) {
						show_flash_message("error", res.msg);
					} else {
						show_flash_message("success", res.msg);
						window.location.reload();
					}
				},
				error: function(res) {

				}
			})
		}

		function approveUser(couponId) {
			$.ajax({
				url: base_url + "admin/users/approveUser",
				method: 'POST',
				dataType: 'json',
				data: {
					id: couponId
				},
				success: function(res) {
					if (res.error) {
						show_flash_message("error", res.msg);
					} else {
						show_flash_message("success", res.msg);
						window.location.reload();
					}
				},
				error: function(res) {

				}
			})
		}
		function confirmDeleteUser(user_id,user_type, elm) {
		if(user_type=="User"){
			user_type=2;
		}else if(user_type=="Business"){
			user_type=1;
		}
		console.log("cofirm delete user");
$.confirm({
	title: 'Are you sure?',
	content: 'If you continue, This user will be deleted and all the data related to this user such as coupons, favorite, profile etc. will be deleted.',
	buttons: {
		confirm: function() {
			deleteUser(user_id,user_type);
		},
		cancel: function() {
			return true;
		},
	}
});
}


function deleteUser(user_id,user_type) {
	$.ajax({
		url: base_url + "admin/users/deleteUser",
		method: 'POST',
		dataType: 'json',
		data: {
			id: user_id,
			user_type:user_type
		},
		success: function(res) {
			if (res.error) {
				show_flash_message("error", res.msg);
			} else {
				show_flash_message("success", res.msg);
				window.location.reload();
			}
		},
		error: function(res) {

		}
	});
}
	</script>
</body>

</html>