<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>ICITTA | CATEGORY</title>
	<?php include __DIR__ . '/../layout/linkheader.php'; ?>
</head>

<body class="vertical-layout vertical-menu 2-columns white fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
	<?php include __DIR__ . '/../layout/alerts.php'; ?>
	<div class="wrapper">
		<?php include __DIR__ . '/../layout/sidemenu.php'; ?>
		<div class="app-content content">
			<div class="content-wrapper">
				<!-- <div class="card profile-with-cover">
                <div class="card-img-top img-fluid bg-cover"></div>
                <div class="media profil-cover-details w-100">
                    <div class="media-left pl-2 pt-2">
                        <a href="#" class="profile-image">
                            <img src="<?php echo base_url('assets/images/avatar-s-1.png') ?>" class="rounded-circle img-border height-100" alt="Card image">
                        </a>
                    </div>
                    <div class="media-body pt-3 px-2">
                        <div class="row">
                            <div class="col">
                              <h3 class="card-title mb-0">Jhon Doe</h3>
                            </div>
                            <div class="col text-right">
                                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Follow</button>
                                <div class="btn-group d-none d-md-block float-right ml-2" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-success"><i class="fa fa-dashcube"></i> Message</button>
                                    <button type="button" class="btn btn-success"><i class="fa fa-cog"></i></button>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
                <nav class="navbar navbar-light navbar-profile align-self-end">
                    <button class="navbar-toggler d-sm-none" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation"></button>
                    <nav class="navbar navbar-expand-lg">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#"><i class="fa fa-line-chart"></i> Timeline <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="ft-user"></i> Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fa fa-briefcase"></i> Projects</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fa fa-heart-o"></i> Favourites</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fa fa-bell-o"></i> Notifications</a>
                                </li>
                            </ul>
                        </div>                    
                    </nav>
                </nav>            
            </div> -->

				<div class="col-md-12">
					<div class="category_name">
						<h3 class="text-capitalize mb-0"><?php echo $business['name'] ?></h3>
						<p class="text-left mb-1"><?php echo $user['user_type'] ?>
						</p>
						<p class="text-left mb-1">User Type: <?php echo $user['user_type'] ?></p>
						<p class="text-left mb-1">Email Id: <?php echo $user['email'] ?></p>
						<p class="text-left mb-1">City: <?php echo $business['city'] ?></p>
						<div class="discripton_content pb-2">
							<p class="mb-0"><?php echo $business['about_us'] ?></p>
						</div>
					</div>
					<div class="created_coupon">
						<div class="row match-height">
							<div class="col-md-12">
								<h3>coupons</h3>
							</div>
						</div>
						<div class="row match-height">
							<?php $i=0; foreach($coupons as $c): ?>
							<div class="col-lg-3 col-sm-4 text-center" onclick="window.location='<?php echo base_url('admin/coupons/showCouponDetails/'.$c->id) ?>'">
								<div class="coupon-grid coupon_box">
									<img src="<?php echo base_url("uploads/coupons/".$c->image) ?>" class="img-fluid">
									<div class="coupon_name text-center">
										<p><?php $c->description; ?></p>
									</div>
								</div>
								<h4 class="mb-0 py-2 text-capitalize"><?php echo $c->name ?></h4>
							</div>
							<?php $i++; endforeach;
							
							if($i==0){
								echo "<p>No Coupons</p>";
							}
							?>
<!-- 							
							<div class="col-lg-3 col-sm-4 text-center">
								<div class="coupon-grid coupon_box">
									<img src="assets/images/06.jpg" class="img-fluid">
									<div class="coupon_name text-center">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
									</div>
								</div>
								<h4 class="mb-0 py-2 text-capitalize">coupon name</h4>
							</div>
							<div class="col-lg-3 col-sm-4 text-center">
								<div class="coupon-grid coupon_box">
									<img src="assets/images/06.jpg" class="img-fluid">
									<div class="coupon_name text-center">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
									</div>
								</div>
								<h4 class="mb-0 py-2 text-capitalize">coupon name</h4>
							</div>
							<div class="col-lg-3 col-sm-4 text-center">
								<div class="coupon-grid coupon_box">
									<img src="assets/images/06.jpg" class="img-fluid">
									<div class="coupon_name text-center">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
									</div>
								</div>
								<h4 class="mb-0 py-2 text-capitalize">coupon name</h4>
							</div> -->

						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include __DIR__ . '/../layout/linkfooter.php'; ?>
	</div>
</body>

</html>
