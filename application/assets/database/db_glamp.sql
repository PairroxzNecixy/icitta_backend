-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 11, 2019 at 09:02 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_glamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `authentication_keys`
--

CREATE TABLE `authentication_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `divice_id` varchar(512) NOT NULL,
  `divice_type` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authentication_keys`
--

INSERT INTO `authentication_keys` (`id`, `user_id`, `auth_key`, `divice_id`, `divice_type`, `create_at`, `modify_at`, `delete_at`) VALUES
(5, 3, '517695d26d3b23560e', 'divice1234', 'android', '2019-07-11 11:44:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(50) NOT NULL,
  `id_token` varchar(255) NOT NULL,
  `token_key` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_logins`
--

INSERT INTO `social_logins` (`id`, `user_id`, `id_token`, `token_key`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 3, 'token123', 'token234key890', '2019-07-11 12:25:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `image` varchar(512) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `mobile`, `password`, `gender`, `image`, `role_id`, `is_active`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 'amit', 'mandad', 'amitmandad92@gmail.com', '2356897412', '$2y$10$uoMCF0wFvQEF8l8QQfR5c.TQ/O771Hy1krT0fHdYU9J7Bt5DqOCHy', 'male', '', 0, 0, '2019-07-10 13:15:04', NULL, NULL),
(2, 'kamlesh', 'mandad', 'kamlesh92@gmail.com', '2356897413', '$2y$10$CvOounQaXg7.3vWL8I7Uve5Ni4TVP5/.TxEo5BE2igFRekIdyfieG', 'male', '', 0, 0, '2019-07-10 14:46:12', NULL, NULL),
(3, 'kamlesh', 'mandad', 'kamlesh912@gmail.com', '2356897414', '$2y$10$ubJXrXpXTkCSX6JrQ3UpyujgRBHACLGg5rtEOzl/f6iWscA97lJh6', 'male', '', 0, 0, '2019-07-10 14:53:03', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authentication_keys`
--
ALTER TABLE `authentication_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authentication_keys`
--
ALTER TABLE `authentication_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
