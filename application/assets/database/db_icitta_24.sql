-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 24, 2019 at 03:39 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_icitta`
--

-- --------------------------------------------------------

--
-- Table structure for table `authentication_keys`
--

CREATE TABLE `authentication_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `device_id` varchar(512) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authentication_keys`
--

INSERT INTO `authentication_keys` (`id`, `user_id`, `auth_key`, `device_id`, `device_type`, `create_at`, `modify_at`, `delete_at`) VALUES
(30, 5, '948305d319fd0daa77', 'device123', 'ios', '2019-07-19 10:14:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 7, '795795d3563a4a10fd', 'device12345', 'iosa', '2019-07-22 06:02:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 8, '863085d369b48bb779', 'device321', 'iosa', '2019-07-23 05:29:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 8, '991735d369bb844c45', 'device12345', 'iosa', '2019-07-23 05:31:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 9, '71445d381a295d13c', 'de321', 'iosa', '2019-07-24 08:32:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 10, '309415d381811abdca', 'de321', 'iosa', '2019-07-24 08:34:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 9, '238445d381a9dbe17e', 'de3212', 'iosa', '2019-07-24 08:43:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 9, '62575d381aae6902b', 'de32123', 'iosa', '2019-07-24 08:45:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `business_profiles`
--

CREATE TABLE `business_profiles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lat` varchar(512) NOT NULL,
  `lng` varchar(512) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_profiles`
--

INSERT INTO `business_profiles` (`id`, `name`, `category_id`, `user_id`, `lat`, `lng`, `city`, `address`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 'pairroxz', 1, 7, '', '', '', '', '2019-07-24 09:53:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `image` varchar(512) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `image`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 'medical', 'default.jpg', '2019-07-13 05:48:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'sanitary', 'default.jpg', '2019-07-13 05:48:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `apply_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `expire_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` varchar(6000) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `user_id`, `coupon_type_id`, `name`, `image`, `value`, `category_id`, `apply_date`, `expire_date`, `description`, `create_at`, `modify_at`, `delete_at`) VALUES
(2, 1, 3, 'discount offer', '', '50', 1, '2019-07-11 18:30:00', '2019-07-12 18:30:00', 'testing', '2019-07-12 07:21:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 7, 1, 'coupon', '3e13c150525410f1559ea78021f6dd32.jpg', '50', 1, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'demo', '2019-07-22 09:46:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 7, 2, 'coupon2', '0b9426b1f257f2a3f310366f4065fb16.jpg', '60', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-22 11:02:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 7, 2, 'coupon3', 'a81d07ff4a13c7c006b5663e0b06577f.jpg', '65', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-22 11:21:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 7, 2, 'coupon4', 'be4f8a6c63f51e432911288b9f861ca9.jpg', '65', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 08:11:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, 2, 'coupon5', '875830633f630a0541f642dbe0f8ac9e.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 08:49:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 7, 2, 'coupon6', 'fa6726d88a780acf431ea94c4fbfbbea.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 09:16:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 7, 2, 'coupon7', '526908c47c26664198988701972bdde9.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 09:19:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 7, 2, 'coupon7', '8f03f201f00e060710313ad92dd4cd6f.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 09:30:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 7, 2, 'coupon9', 'b7df435dbea4509382bc8b7413eee7fa.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 09:32:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 7, 2, 'coupon10', '15f79ddc0205ee4d4007d6f07a6eed82.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 09:35:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 7, 2, 'coupon11', 'c0e193ffdd545762a8b35e244a046d0d.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 09:37:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 7, 2, 'coupon12', 'aae89d88b56c1f28bb16b0b19e40ef62.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 09:46:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 7, 2, 'coupon12', '117c693d27650291f7a959e054fcc735.png', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 10:13:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 7, 2, 'errrr', '436708fd224f29fcb69dc52c7e950dc4.png', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 10:15:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 7, 2, 'coupon114', '5333b1718cceb0d6591ad2804ba6ba9f.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 10:17:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 7, 2, 'coupon115', '4ca08bd9a37e443aaed5e86e7ad51394.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 10:19:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 7, 2, 'coupon116', '1659005e6c0384c1643d52449cf42bad.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 10:20:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 7, 2, 'coupon117', 'd0822730f8fa10cd1126cd5e32e1f6a6.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 10:22:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 7, 2, 'coupon118', '158cf2409eeb2157734b350f1e5dd229.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 10:24:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 7, 2, 'coupon119', '59ab3a07b59b72cbadb0dd729a141ca5.jpg', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 10:26:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 7, 1, 'coupon120', '5799bf799f233b27deb7c4c1a79a9acd.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 10:29:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 7, 1, 'coupon121', 'dd800cc42cdd78fac3102b7439748661.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 10:47:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 7, 1, 'coupon121', '12c07b21b35d7017cd058b3e9f884a1a.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 10:49:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 7, 1, 'coupon121', '2bbeba74d90042ab38d3a180d5c1ae24.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 10:51:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 7, 1, 'coupon121', '010f506f904cb90d1d925b94e8dad5e6.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 10:52:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 7, 1, 'coupon121', '4c50c86d5884c2cfecd21832f1b05f10.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 10:53:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 7, 1, 'coupon121', '8c7c6f878c6cdad6744b374344fca68f.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 10:54:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 7, 2, 'errrr', '2768fcc3a2ab01e3068643848ed2ad25.png', '55', 2, '2019-07-21 18:30:00', '2019-07-24 18:30:00', 'tested tested tested', '2019-07-23 11:19:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 7, 1, 'coupon11', 'b68c3aa29d3b1d1eeae6b60e425da8f6.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 11:52:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 7, 1, 'coupon19', '4213221e93bbae51557352cc91a3e689.jpg', '42', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-23 12:00:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 7, 1, 'coupon130', 'dcfb125a247e657878e451e2ba65a9ca.jpg', '41', 1, '2019-07-22 18:30:00', '2019-07-25 18:30:00', 'testing', '2019-07-24 05:40:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 7, 1, 'coupon131', 'ef6cefa7832206d7a2457adcf736e00d.jpg', '41', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'testing', '2019-07-24 05:41:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 7, 1, 'coupon131', '83afee41a38da7b6f36ad1e9317401f6.jpg', '41', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'testing', '2019-07-24 05:50:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 7, 1, 'coupon131', '6b42bb8bb263283958075062b57d416f.jpg', '41', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'testing', '2019-07-24 06:36:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 7, 1, 'coupon132', 'c081929f09f9c6f83446a3994cefe537.jpg', '41', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'testing', '2019-07-24 06:37:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 7, 1, 'coupon133', '3ea22deab0c37498fca96e54ad1cf257.jpg', '41', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'testing', '2019-07-24 06:39:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 7, 1, 'coupon134', 'd5d8a50c1d10be195b636d4807dd5cc4.jpg', '41', 1, '2019-07-25 15:00:00', '2019-07-25 15:00:00', 'testing', '2019-07-24 06:45:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 7, 2, 'coupon136', 'a2b35965b9d5cf43b4f52fde76965fa0.jpg', '73', 2, '2019-07-25 15:00:00', '2019-07-25 15:00:00', 'testing', '2019-07-24 06:55:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_types`
--

CREATE TABLE `coupon_types` (
  `id` int(11) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_types`
--

INSERT INTO `coupon_types` (`id`, `coupon_type`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 'Percent wise discount', '2019-07-19 13:37:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Promotion', '2019-07-19 13:37:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Box', '2019-07-19 13:37:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(50) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `token_key` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_logins`
--

INSERT INTO `social_logins` (`id`, `user_id`, `access_token`, `token_key`, `create_at`, `modify_at`, `delete_at`) VALUES
(5, 3, 'token123', 'token23456', '2019-07-16 11:36:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `image` varchar(512) NOT NULL,
  `language` varchar(255) NOT NULL DEFAULT 'en',
  `user_type_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `country_code`, `mobile`, `city`, `address`, `password`, `gender`, `image`, `language`, `user_type_id`, `is_active`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 'amit', 'amitmandad92@gmail.com', '', '2356897412', '', '', '$2y$10$uoMCF0wFvQEF8l8QQfR5c.TQ/O771Hy1krT0fHdYU9J7Bt5DqOCHy', 'male', '', 'en', 1, 0, '2019-07-10 07:45:04', NULL, NULL),
(2, 'Harshit', 'test@gmail.com', '', '4567891230', 'jaipur rajasthan', 'malviyanagar', '$2y$10$tKM2drK/rrZpadxWpOGUeeKHEht1SLHL9QN6cOmyXOUXxa9Xe0eCG', '', 'default.jpg', 'hi', 1, 0, '2019-07-16 07:00:12', NULL, NULL),
(3, 'test', 'test2@gmail.com', '', '1234567891', 'jaipur', 'nandpuri', '$2y$10$qf2iFeAKhOuXoe6tcypvWuQJ4NnvTS9pvuHymzbLaoYEcO0U6HGCG', '', 'e55707a992524c7e6bc374f0f1d652ad.jpeg', 'en', 1, 0, '2019-07-16 07:04:15', NULL, NULL),
(4, 'test3', 'test3@gmail.com', '', '1234567891', 'jaipur', 'nandpuri', '$2y$10$kqbiyI7yL341LUvMsxxXW.vKNSjPSsGER8uDpXPlS.JQmGcyRsWiq', '', 'default.jpg', 'en', 1, 0, '2019-07-16 11:59:21', NULL, NULL),
(5, 'final test', 'final@gmail.com', '', '9632587410', 'jaipur', 'nandpuri', '$2y$10$g6YleW3fT.b7faHxyjlKsu0L/e61ws7Adu9qLcTd26JtDzoqC5Pa6', '', 'default.jpg', 'en', 2, 0, '2019-07-19 10:14:55', NULL, NULL),
(6, 'final test', 'final2@gmail.com', '', '9632587410', 'jaipur', 'nandpuri', '$2y$10$alLUdzbh5ad.5P9KnT15E.IsGRVD8tx17zyt63MnbXxCqHD0gZUwu', '', 'default.jpg', 'en', 2, 0, '2019-07-19 10:30:06', NULL, NULL),
(7, 'ADM FULL', 'ADM@gmail.com', '', '96948560048', 'alwar', 'kala kuwa', '$2y$10$8a/wy6VmqBZ.XdUr0uBTK.FkWfo.3XQf9k35.Qu/FK76cz1C.ZOwC', '', '529b6444263ebabea00745309d6cc8f6.jpg', 'en', 2, 0, '2019-07-22 06:02:12', NULL, NULL),
(8, 'kamlesh', 'kamlesh@gmail.com', '', '3265987410', 'jaipur', 'triveni', '$2y$10$Bv2vy2QPUfP4NuZB6hoI/umJcb4t8E1fJc/PPFjuxI0DqqjAgsiee', '', 'default.jpg', 'en', 1, 0, '2019-07-23 05:29:44', NULL, NULL),
(9, 'new user', 'new@gmail.com', '', '3265987410', 'jaipur', 'triveni', '$2y$10$vJI9G0Qc//8nSK5GKD9yZ.a4bom1zM/0WDeIETgDbdLUNJY.8TAtO', '', 'default.jpg', 'en', 1, 0, '2019-07-24 08:32:42', NULL, NULL),
(10, 'new user 2', 'new2@gmail.com', '', '3265987410', 'jaipur', 'triveni', '$2y$10$OVKj.Jo5ryxZBXiabf5C2.FfJsgXvi18EozEys4X/I/JpqQQNvtaS', '', 'default.jpg', 'en', 1, 0, '2019-07-24 08:34:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `slag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `user_type`, `slag`) VALUES
(1, 'Business ', 'business '),
(2, 'User', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authentication_keys`
--
ALTER TABLE `authentication_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_profiles`
--
ALTER TABLE `business_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `coupon_type_id` (`coupon_type_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `coupon_types`
--
ALTER TABLE `coupon_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_type_id` (`user_type_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authentication_keys`
--
ALTER TABLE `authentication_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `business_profiles`
--
ALTER TABLE `business_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `coupon_types`
--
ALTER TABLE `coupon_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `coupons`
--
ALTER TABLE `coupons`
  ADD CONSTRAINT `coupons_ibfk_1` FOREIGN KEY (`coupon_type_id`) REFERENCES `coupon_types` (`id`),
  ADD CONSTRAINT `users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_type` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
