-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 22, 2019 at 03:24 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_icitta`
--

-- --------------------------------------------------------

--
-- Table structure for table `authentication_keys`
--

CREATE TABLE `authentication_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `device_id` varchar(512) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authentication_keys`
--

INSERT INTO `authentication_keys` (`id`, `user_id`, `auth_key`, `device_id`, `device_type`, `create_at`, `modify_at`, `delete_at`) VALUES
(30, 5, '948305d319fd0daa77', 'device123', 'ios', '2019-07-19 15:44:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 7, '795795d3563a4a10fd', 'device12345', 'iosa', '2019-07-22 11:32:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `business_profile`
--

CREATE TABLE `business_profile` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `lat` varchar(512) NOT NULL,
  `log` varchar(512) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 'medical', '2019-07-13 11:18:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'sanitary', '2019-07-13 11:18:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `apply_date` datetime NOT NULL,
  `expire_date` datetime NOT NULL,
  `description` varchar(6000) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `user_id`, `coupon_type_id`, `name`, `image`, `value`, `category_id`, `apply_date`, `expire_date`, `description`, `create_at`, `modify_at`, `delete_at`) VALUES
(2, 1, 3, 'discount offer', '', '50', 1, '2019-07-12 00:00:00', '2019-07-13 00:00:00', 'testing', '2019-07-12 12:51:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 7, 1, 'coupon', '3e13c150525410f1559ea78021f6dd32.jpg', '50%', 1, '2019-07-22 00:00:00', '2019-07-25 00:00:00', 'demo', '2019-07-22 15:16:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 7, 2, 'coupon2', '0b9426b1f257f2a3f310366f4065fb16.jpg', '60%', 2, '2019-07-22 00:00:00', '2019-07-25 00:00:00', 'tested tested tested', '2019-07-22 16:32:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 7, 2, 'coupon3', 'a81d07ff4a13c7c006b5663e0b06577f.jpg', '65%', 2, '2019-07-22 00:00:00', '2019-07-25 00:00:00', 'tested tested tested', '2019-07-22 16:51:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_types`
--

CREATE TABLE `coupon_types` (
  `id` int(11) NOT NULL,
  `coupe_type` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_types`
--

INSERT INTO `coupon_types` (`id`, `coupe_type`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 'Percent wise discount', '2019-07-19 19:07:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Promotion', '2019-07-19 19:07:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Box', '2019-07-19 19:07:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(50) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `token_key` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime NOT NULL,
  `delete_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_logins`
--

INSERT INTO `social_logins` (`id`, `user_id`, `access_token`, `token_key`, `create_at`, `modify_at`, `delete_at`) VALUES
(5, 3, 'token123', 'token23456', '2019-07-16 17:06:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `image` varchar(512) NOT NULL,
  `language` varchar(255) NOT NULL DEFAULT 'en',
  `user_type_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` datetime DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `country_code`, `mobile`, `city`, `address`, `password`, `gender`, `image`, `language`, `user_type_id`, `is_active`, `create_at`, `modify_at`, `delete_at`) VALUES
(1, 'amit', 'amitmandad92@gmail.com', '', '2356897412', '', '', '$2y$10$uoMCF0wFvQEF8l8QQfR5c.TQ/O771Hy1krT0fHdYU9J7Bt5DqOCHy', 'male', '', 'en', 1, 0, '2019-07-10 13:15:04', NULL, NULL),
(2, 'Harshit', 'test@gmail.com', '', '4567891230', 'jaipur rajasthan', 'malviyanagar', '$2y$10$tKM2drK/rrZpadxWpOGUeeKHEht1SLHL9QN6cOmyXOUXxa9Xe0eCG', '', 'default.jpg', 'hi', 1, 0, '2019-07-16 12:30:12', NULL, NULL),
(3, 'test', 'test2@gmail.com', '', '1234567891', 'jaipur', 'nandpuri', '$2y$10$qf2iFeAKhOuXoe6tcypvWuQJ4NnvTS9pvuHymzbLaoYEcO0U6HGCG', '', 'e55707a992524c7e6bc374f0f1d652ad.jpeg', 'en', 1, 0, '2019-07-16 12:34:15', NULL, NULL),
(4, 'test3', 'test3@gmail.com', '', '1234567891', 'jaipur', 'nandpuri', '$2y$10$kqbiyI7yL341LUvMsxxXW.vKNSjPSsGER8uDpXPlS.JQmGcyRsWiq', '', 'default.jpg', 'en', 1, 0, '2019-07-16 17:29:21', NULL, NULL),
(5, 'final test', 'final@gmail.com', '', '9632587410', 'jaipur', 'nandpuri', '$2y$10$g6YleW3fT.b7faHxyjlKsu0L/e61ws7Adu9qLcTd26JtDzoqC5Pa6', '', 'default.jpg', 'en', 2, 0, '2019-07-19 15:44:55', NULL, NULL),
(6, 'final test', 'final2@gmail.com', '', '9632587410', 'jaipur', 'nandpuri', '$2y$10$alLUdzbh5ad.5P9KnT15E.IsGRVD8tx17zyt63MnbXxCqHD0gZUwu', '', 'default.jpg', 'en', 2, 0, '2019-07-19 16:00:06', NULL, NULL),
(7, 'ADM FULL', 'ADM@gmail.com', '', '96948560048', 'alwar', 'kala kuwa', '$2y$10$8a/wy6VmqBZ.XdUr0uBTK.FkWfo.3XQf9k35.Qu/FK76cz1C.ZOwC', '', '529b6444263ebabea00745309d6cc8f6.jpg', 'en', 2, 0, '2019-07-22 11:32:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `slag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `user_type`, `slag`) VALUES
(1, 'Business ', 'business '),
(2, 'User', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authentication_keys`
--
ALTER TABLE `authentication_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_profile`
--
ALTER TABLE `business_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `coupon_type_id` (`coupon_type_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `coupon_types`
--
ALTER TABLE `coupon_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_type_id` (`user_type_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authentication_keys`
--
ALTER TABLE `authentication_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `business_profile`
--
ALTER TABLE `business_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `coupon_types`
--
ALTER TABLE `coupon_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `coupons`
--
ALTER TABLE `coupons`
  ADD CONSTRAINT `coupons_ibfk_1` FOREIGN KEY (`coupon_type_id`) REFERENCES `coupon_types` (`id`),
  ADD CONSTRAINT `users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_type` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
